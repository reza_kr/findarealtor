<?php

    require_once('functions.php');

    $html = load_html();

    if($_GET['e'] == 'missing_fields') {

    	$message = '<div class="alert alert-danger" role="alert">Please complete all the required fields.</div>';

    } else if($_GET['e'] == 'invalid_email') {

    	$message = '<div class="alert alert-danger" role="alert">Please provide a valid email.</div>';

    } else if($_GET['e'] == 'password_too_short') {

    	$message = '<div class="alert alert-danger" role="alert">Password must be at least 8 characters long.</div>';
    
    } else if($_GET['e'] == 'email_in_use') {

    	$message = '<div class="alert alert-danger" role="alert">The email address you provided is already in use. If you already have an account, please <a href="login.php">log in here</a>. Otherwise, try a differnt email address.</div>';
    }
    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>
    </head>

    <body class="do-contact-us-page">

        <?php require_once('inc/header.php'); ?>

        <section class="content">
            <div class="container">
                
                <div class="row">
					<div class="col-xs-12 col-md-4 col-md-offset-4">

						<?php echo $message; ?>

						<p><a href="<?php echo $fb_login_url; ?>" class="fb_login"><img src="img/fb_login.png" /></a></p>
		                
		                <span class="or">OR</span>

						<form class="form" method="POST" action="?action=user_registration">
							<div class="form-group">
								 <label class="sr-only" for="first_name">First Name</label>
								 <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
							</div>
							<div class="form-group">
								 <label class="sr-only" for="last_name">Last Name</label>
								 <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
							</div>
							<div class="form-group">
								 <label class="sr-only" for="email">Email Address</label>
								 <input type="email" class="form-control" id="email" name="email" placeholder="Email Address">
							</div>
							<div class="form-group">
								 <label class="sr-only" for="password">Password</label>
								 <input type="password" class="form-control" id="password" name="password" placeholder="Password">
		                         <div class="help-block text-right"><a href="">Forget password?</a></div>
							</div>
							<div class="form-group">
								 <button type="submit" class="btn btn-primary btn-block">Sign up</button>
							</div>
						</form>

						<br>
						Already have an account? <a href="login.php"><b>Log in</b></a>
					</div>
				</div>

            </div>
        </section>

        <?php require_once('inc/footer.php'); ?>
                                    
    </body>
</html>
