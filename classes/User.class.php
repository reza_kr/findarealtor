<?php

	session_start();
	
	Class User { 

		public function __construct () {

			require("inc/connect_dbo.php");
			$this->dbo = $dbo;

			require_once('PseudoCrypt.class.php');
			$this->pseudocrypt = new PseudoCrypt();

		}


		public function handleFacebookLogin($oauth_id, $name, $profile_picture) {

			$found = false;

			$query = $this->dbo->prepare("SELECT id, first_name, last_name, profile_picture FROM user_accounts WHERE oauth_id = ?");
			$query->execute(array($oauth_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$_SESSION['user_id'] = $row[0];
				$_SESSION['first_name'] = $row[1];
				$_SESSION['last_name'] = $row[2];
				$_SESSION['profile_picture'] = $row[3];
				$_SESSION['logged_in'] = true;
				$_SESSION['user_type'] = 'user';
				$_SESSION['oauth_id'] = $oauth_id;

				$found = true;
			
			}

			if($found == false) {

				//user does not exist yet. add to databse now...
				$name_parts = explode(' ', $name, 2);

				$datetime = date('Y-m-d H:i:s');

				$query = $this->dbo->prepare("INSERT INTO user_accounts SET first_name = ?, last_name =?, date_joined = ?, oauth_id = ?, verified = ?");
				$query->execute(array($name_parts[0], $name_parts[1], $datetime, $oauth_id, 1));
				https://scontent-yyz1-1.xx.fbcdn.net/hphotos-xap1/v/t1.0-9/12289656_1505946099733629_2161263185198280104_n.jpg?oh=610733c837174be58789358dc04f9ffc&oe=56DF0C6F
				$_SESSION['user_id'] = $this->dbo->lastInsertId();
				$_SESSION['user_id_hashed'] = $this->pseudocrypt->hash($this->dbo->lastInsertId(), 6);
				$_SESSION['first_name'] = $name_parts[0];
				$_SESSION['last_name'] = $name_parts[1];
				//$_SESSION['profile_picture'] = $profile_picture;
				$_SESSION['logged_in'] = true;
				$_SESSION['user_type'] = 'user';
				$_SESSION['oauth_id'] = $oauth_id;

				$fb_login = true; //used within ajax_file_upload.php
				include('ajax_file_upload.php');
				//file_get_contents('ajax_file_upload.php?action=fb_profile_picture&uid=' . $_SESSION['user_id_hashed'] . '&file=' . $profile_picture);
			}

			$datetime = date('Y-m-d H:i:s');

			$query = $this->dbo->prepare("UPDATE user_accounts SET last_login = ? WHERE id = ?");
			$query->execute(array($datetime, $_SESSION['user_id']));

			// Keep Logged In logic - To be added later!
			// $token = hash('SHA256', time());
			// $datetime = date('Y-m-d H:i:s');

			// $query = $this->dbo->prepare("INSERT INTO login_tokens SET user_id = ?, token = ?, datetime = ?");
			// $query->execute(array($_SESSION['user_id'], $token, $datetime));

			// $cookie = $_SESSION['user_id'] . ':' . $token;

			// $cookie_hashed = hash('SHA256', $cookie);

			// $cookie .= ':' . $cookie_hashed;
			// setcookie('keep_logged_in', $cookie);

		}


		public function register() {

			$first_name = htmlspecialchars($_POST['first_name']);
			$last_name = htmlspecialchars($_POST['last_name']);
			$email = htmlspecialchars($_POST['email']);
			$password = htmlspecialchars($_POST['password']);

			if($email == '' || $password == '' || $first_name == '' || $last_name == '') {

				header('Location: registration.php?e=missing_fields');
				exit;
			
			} else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {

				header('Location: registration.php?e=invalid_email');
				exit;

			} else if(strlen($password) < 8) {

				header('Location: registration.php?e=password_too_short');
				exit;
			}

			$password_hashed = hash('SHA1', $password);

			$token = hash('SHA256', time());

			$datetime = date('Y-m-d H:i:s');

			$query = $this->dbo->prepare("SELECT COUNT(*) FROM user_accounts WHERE email = ?");
			$query->execute(array($email));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$num_rows = $row[0];
			}

			if($num_rows == 0) {

				$query = $this->dbo->prepare("INSERT INTO user_accounts SET first_name = ?, last_name = ?, email = ?, password = ?, date_joined = ?, token = ?, last_login = ?");
				$query->execute(array($first_name, $last_name, $email, $password_hashed, $datetime, $token, $datetime));

				$user_id = $this->dbo->lastInsertId();


				require_once('Email.class.php');
				$mail = new Email();

				$mail->signUpConfirmation($first_name, $last_name, $email, 'user', $token);

				header('Location: signup_confirmation.php?ref=confirm_email_address');				

			} else {

				//Email in use...
				header('Location: registration.php?e=email_in_use');
				exit;

			}

		}


		public function login() {

			echo file_get_contents('test.php?id=1204321'); exit;

			$email = htmlspecialchars($_POST['email']);
			$password = htmlspecialchars($_POST['password']);

			if($email == '' || $password == '') {

				header('Location: login.php?e=missing_fields');
				exit;
			
			} else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {

				header('Location: login.php?e=invalid_email');
				exit;

			} else if(strlen($password) < 8) {

				header('Location: login.php?e=password_too_short');
				exit;
			}

			$password_hashed = hash('SHA1', $password);

			$query = $this->dbo->prepare("SELECT id, first_name, last_name, profile_picture FROM user_accounts WHERE email = ? AND password = ?");
			$query->execute(array($email, $password_hashed));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$datetime = date('Y-m-d H:i:s');

				$query2 = $this->dbo->prepare("UPDATE user_accounts SET last_login = ? WHERE id = ?");
				$query2->execute(array($datetime, $row[0]));

				$_SESSION['user_id'] = $row[0];
				$_SESSION['user_id_hashed'] = $this->pseudocrypt->hash($row[0], 6);
				$_SESSION['first_name'] = $row[1];
				$_SESSION['last_name'] = $row[2];
				$_SESSION['profile_picture'] = $row[3];
				$_SESSION['email'] = $email;
				$_SESSION['logged_in'] = true;
				$_SESSION['user_type'] = 'user';

				// Keep Logged In logic - To be added later!
				// $token = hash('SHA256', time());
				// $datetime = date('Y-m-d H:i:s');

				// $query = $this->dbo->prepare("INSERT INTO login_tokens SET user_id = ?, token = ?, datetime = ?");
				// $query->execute(array($_SESSION['user_id'], $token, $datetime));

				// $cookie = $_SESSION['user_id'] . ':' . $token;

				// $cookie_hashed = hash('SHA256', $cookie);

				// $cookie .= ':' . $cookie_hashed;
				// setcookie('keep_logged_in', $cookie);

				if(isset($_SESSION['last_page'])) {

					if(isset($_GET['id'])) {
						header('Location: ' . $_SESSION['last_page'] . '?id=' . $_GET['id']);
						exit;
					} else {
						header('Location: ' . $_SESSION['last_page']);
						exit;
					}

				} else {

					header('Location: /');
					exit;
				}
			}

			header('Location: login.php?e=incorrect_login');
			exit;
		}


		public function resetPassword() {
	    	
	    	$email = htmlspecialchars($_POST['email']);

	    	if(filter_var($email, FILTER_VALIDATE_EMAIL)) {

				$query = $this->dbo->prepare("SELECT id, first_name, last_name FROM user_accounts WHERE email = ?");
				$query->execute(array($email));

				$result = $query->fetchAll();

				foreach($result as $row) {
				    
				    $user_id = $row[0];
				    $first_name = $row[1];
				    $last_name = $row[2];

				    $token = hash('SHA256', time());

				    $datetime = date('Y-m-d H:i:s', time() + 86400); //NOW PLUS 1 DAY for EXPIRY

				    $query = $this->dbo->prepare("INSERT INTO password_reset_token SET user_id = ?, email = ?, token = ?, expiry = ?");
					$query->execute(array($user_id, $email, $token, $datetime));

					require_once("classes/Email.class.php");
					$mail = new Email();

					$mail->resetPassword($first_name, $last_name, $email, 'user', $token);

					header('Location: password_reset.php?ref=request_sent');
					exit;
				}

				header('Location: password_reset.php?e=account_not_found');
				exit;

			} else {

				header('Location: password_reset.php?e=invalid_email');
				exit;
			}

	    }


	    public function checkPasswordResetToken() {

			$token = htmlspecialchars($_GET['token']);
			$email = html_entity_decode(htmlspecialchars($_GET['email']));

			$datetime = date('Y-m-d H:i:s');

			$query = $this->dbo->prepare("SELECT email FROM password_reset_token WHERE email = ? AND token = ? AND expiry >= ?");
			$query->execute(array($email, $token, $datetime));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$found_email = $row[0];

			}

			if($found_email != "") {

				return $found_email;

			} else {

				header('Location: password_reset.php?e=invalid_reset_token');
			}

		}



		public function resetPasswordChange($email) {

			$new_password = htmlspecialchars($_POST['new_password']);
			$new_password2 = htmlspecialchars($_POST['new_password2']);

			$token = htmlspecialchars($_GET['token']);


			if($new_password == $new_password2) {

				//if (preg_match('/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/', $new_password)) {
				if(strlen($new_password) >= 8) {

					$new_password = hash('SHA1', $new_password);

					$query = $this->dbo->prepare("UPDATE user_accounts SET password = ? WHERE email = ?");
					$query->execute(array($new_password, $email));

			    	$query = $this->dbo->prepare("DELETE FROM password_reset_token WHERE email = ?");
					$query->execute(array($email));

			    	header('Location: login.php?ref=password_change_successful');

				} else {

					header('Location: password_change.php?token=' . $token . '&email=' . $email . '&e=password_too_short');
				}

			} else {

				header('Location: password_change.php?token=' . $token . '&email=' . $email . '&e=password_mismatch');
			}
		}


		public function postReview() {

			$realtor_id = $this->pseudocrypt->unhash(htmlspecialchars($_POST['rid']));
			$user_id = $_SESSION['user_id'];

			$rating = htmlspecialchars($_POST['satisfaction']);
			$review_details = trim(htmlspecialchars($_POST['details']));

			if(!isset($rating)) {

				header('Location: write_a_review.php?id=' . htmlspecialchars($_POST['rid']) . '&e=rating_needed');
				exit;
			
			} else if(str_word_count($review_details) < 30) {

				header('Location: write_a_review.php?id=' . htmlspecialchars($_POST['rid']) . '&e=review_too_short');
				exit;
			}

			$anonymous = (htmlspecialchars($_POST['anonymous']) == 'on') ? 1 : 0;

			$datetime = date('Y-m-d H:i:s');

			$query = $this->dbo->prepare("INSERT INTO reviews SET realtor_id = ?, user_id = ?, review_text = ?, datetime = ?, rating = ?, anonymous = ?, status = ?");
			$query->execute(array($realtor_id, $user_id, $review_details, $datetime, $rating, $anonymous, 'pending'));

			header('Location: realtor.php?id=' . htmlspecialchars($_POST['rid']) . '&ref=review_received');
			exit;
		}


		public function updateReview() {

			$user_id = $_SESSION['user_id'];

			$review_id = htmlspecialchars($_POST['review_id']);
			$rating = htmlspecialchars($_POST['satisfaction']);
			$review_details = htmlspecialchars($_POST['details']);
			$anonymous = (htmlspecialchars($_POST['anonymous']) == 'on') ? 1 : 0;

			$datetime = date('Y-m-d H:i:s');

			$query = $this->dbo->prepare("UPDATE reviews SET review_text = ?, datetime = ?, rating = ?, anonymous = ?, status = ? WHERE id = ? AND user_id = ?");
			$query->execute(array($review_details, $datetime, $rating, $anonymous, 'pending', $review_id, $user_id));

			header('Location: my_reviews.php?ref=review_updated');
			exit;
		}


		public function deleteReview() {

			$user_id = $_SESSION['user_id'];

			$review_id = htmlspecialchars($_POST['review_id']);

			$query = $this->dbo->prepare("DELETE FROM reviews WHERE id = ? AND user_id = ?");
			$query->execute(array($review_id, $user_id));

			header('Location: my_reviews.php?ref=review_deleted');
			exit;
		}


		public function loadMyReviews() {

			$data = '';

			$user_id = $_SESSION['user_id'];

			$query = $this->dbo->prepare("SELECT * FROM reviews WHERE user_id = ? ORDER BY datetime DESC");
			$query->execute(array($user_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$review_id = $row[0];
				$realtor_id = $row[1];
				$review_text = $row[3];
				$datetime = date('Y/m/d', strtotime($row[4]));
				$rating = $row[5];
				$anonymous = ($row[6] == 1) ? 'Yes' : 'No';
				$status = ucwords($row[7]);

				$query2 = $this->dbo->prepare("SELECT first_name, last_name FROM realtor_accounts WHERE id = ?");
				$query2->execute(array($realtor_id));

				$result2 = $query2->fetchAll();

				foreach($result2 as $row2) {

					$first_name = $row2[0];
					$last_name = $row2[1];
				}

				$data .= '<tr>
							<td>' . $first_name . ' ' . $last_name . '</td>
							<td class="review_text">' . $review_text . '</td>
							<td class="rating">' . $rating . '</td>
							<td class="anonymous">' . $anonymous . '</td>
							<td>' . $datetime . '</td>
							<td>' . $status . '</td>
							<td>
								<div class="btn-group" role="group" aria-label="...">
									<a href="#" id="' . $review_id . '" class="btn btn-default edit_review" data-toggle="modal" data-target="#edit_review"><i class="fa fa-pencil"></i></a>
									<a href="#" id="' . $review_id . '" class="btn btn-default delete_review" data-toggle="modal" data-target="#delete_review"><i class="fa fa-trash-o"></i></a>
								</div>
							</td>
						</tr>';
			}

			if($data == '') {

				$data = '<tr>
							<td colspan="7" class="text-center">No previous reviews to show. Why not write one today?</td>
						</tr>';
			}

			return $data;
		}


		public function updateUserProfile() {

			$get_args = '';

			$user_id = $_SESSION['user_id'];

			$first_name = htmlspecialchars($_POST['first_name']);
			$last_name = htmlspecialchars($_POST['last_name']);
			$email = htmlspecialchars($_POST['email']);

			if($first_name == '' || $last_name == '' || $email == '') {

				$get_args .= 'e=missing_fields&';
			
			}

			if(!isset($_SESSION['oauth_id'])) { //if oauth_id is not set, check for valid email...

				if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {

					$get_args .= 'e=invalid_email&';
				
				}
			
			}

			$query = $this->dbo->prepare("UPDATE user_accounts SET first_name = ?, last_name = ?, email = ? WHERE id = ?");
			$query->execute(array($first_name, $last_name, $email, $user_id));


			//PASSWORD CHANGE
			$new_password = htmlspecialchars($_POST['new_password']);
			$confirm_password = htmlspecialchars($_POST['confirm_password']);

			if($new_password != '') {

				if($new_password == $confirm_password) {

					//if (preg_match('/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/', $new_password)) {
					if(strlen($new_password) >= 8) {

						$new_password = hash('SHA1', $new_password);

						$query = $this->dbo->prepare("UPDATE user_accounts SET password = ? WHERE id = ?");
						$query->execute(array($new_password, $user_id));

				    	$get_args .= 'ref_pass=password_change_successful&';

					} else {

						$get_args .= 'e=password_too_short&';
					}

				} else {

					$get_args .= 'e=password_mismatch&';
				}
			}


			$get_args .= 'ref_profile=profile_updated';

			header('Location: account_settings.php?' . $get_args);

		}



		public function loadUserAccountOverview() {

			$user_id = $_SESSION['user_id'];

			$data = array();
			$service_area_array = array();

			$query = $this->dbo->prepare("SELECT first_name, last_name, email, profile_picture FROM user_accounts WHERE id = ?");
			$query->execute(array($user_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$data['first_name'] = $row[0];
				$data['last_name'] = $row[1];
				$data['email'] = $row[2];
				$data['profile_picture'] = $row[3];
			}

			if($data['profile_picture'] != '') {
				$data['profile_picture'] = '<img src="http://cdn.findarealtor.net/upp/' . $data['profile_picture'] . '" alt="' . $data['first_name'] . ' ' . $data['last_name'] . '" /><a href="#" id="remove_picture" title="Remove Profile Picture"><i class="fa fa-close"></i></a>';
			} else {
				$data['profile_picture'] = '<img src="img/anonymous.png" alt="Anonymous" />';
			}


			return $data;
		}


		public function completeProfilePictureUpload($filename, $user_id) {

			$user_id = $this->pseudocrypt->unhash($user_id);

			$query = $this->dbo->prepare("UPDATE user_accounts SET profile_picture = ? WHERE id = ?");
			$query->execute(array($filename, $user_id));

			$_SESSION['profile_picture'] = $filename;
			
		}


		public function getProfilePicture() {

			$user_id = $this->pseudocrypt->unhash($_POST['user_id']);

			$query = $this->dbo->prepare("SELECT profile_picture FROM user_accounts WHERE id = ?");
			$query->execute(array($user_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				return $row[0];
			}
		}


		public function removeProfilePicture() {

			$user_id = $_SESSION['user_id'];

			$query = $this->dbo->prepare("SELECT profile_picture FROM user_accounts WHERE id = ?");
			$query->execute(array($user_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$profile_picture = $row[0];
			}

			$query = $this->dbo->prepare("UPDATE user_accounts SET profile_picture = ? WHERE id = ?");
			$query->execute(array('', $user_id));

			$_SESSION['profile_picture'] = '';

			require_once('classes/aws.phar');
	            
	        $s3client = Aws\S3\S3Client::factory(array(
	            'key'    => 'AKIAJKBMGCMLYBHN77PQ',
	            'secret' => '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' //reza_karami@outlook.com account
	        ));


	        $key = 'upp/' . $profile_picture; //upp = user profile pictures

	        $result = $s3client->deleteObject(array(
			    'Bucket' => 'far-resources',
			    'Key'    => $key
			)); 

		}

	}

?>