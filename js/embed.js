function mycallback(data) {

	var rating = document.getElementById('far_rating');

	rating.innerHTML = data.rating + '<span id="far_out_of"><i>/</i>10</span>';

	rating.style.fontSize = '19px';
	rating.style.fontWeight = 'bold';
	rating.style.color = '#555';
	rating.style.display = 'inline-block';
	rating.style.marginTop = '8px';
	rating.style.verticalAlign = 'top';

	var out_of = document.getElementById('far_out_of');

	out_of.style.fontWeight = '300';
	out_of.style.display = 'inline-block';
	out_of.style.margin = '3px 0 0 1px';
	out_of.style.verticalAlign = 'top';
	out_of.style.fontSize = '13px';
}

(function(){

	var embed = document.getElementById('findarealtor_rating');
	var logo = document.getElementById('far_logo');
	var star = document.getElementById('far_star');
	var pipe = document.getElementById('pipe');

	logo.style.height = '35px';

	star.style.height = '25px';
	star.style.display = 'inline-block';
	star.style.verticalAlign = 'top';
	star.style.marginTop = '5px';

	embed.style.fontFamily = 'sans-serif';
	embed.style.display = 'inline-block';
	embed.style.border = '1px solid #DDD';
	embed.style.borderRadius = '2px';
	embed.style.padding = '5px 10px 5px 10px';
	embed.style.background = '#FFF';

	pipe.style.display = 'inline-block';
	pipe.style.verticalAlign = 'top';
	pipe.style.height = '35px';
	pipe.style.width = '1px';
	pipe.style.background = '#DDD';
	pipe.style.margin = '0 5px';

}) ();