    <!-- PRELOADER -->
    <div class="preloader">
        <div class="do-loader">&nbsp;</div>
    </div>

    <header>
        <!-- <div class="do-color-parallax-overlay-2"></div> -->
        <!-- Navigation Menu start-->
        <nav class="navbar do-main-menu" role="navigation">
            <div class="container">

                <!-- Navbar Toggle -->
                <div class="navbar-header" <?php echo (isset($_SESSION['logged_in']) ? 'style="float: left;"' : ''); ?> >
                    <?php

                    if(!isset($_SESSION['logged_in'])) {

                        echo '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>';
                    }

                    ?>

                    <!-- Logo -->
                    <a class="navbar-brand" href="/"><img class="logo" src="img/logo.png" alt="FindARealtor"></a>
                    <?php if($_SESSION['country'] == 'CA') echo '<img src="img/ca.png" alt="FindARealtor Canada" class="ca_flag" />'; ?>
                </div>
                <!-- Navbar Toggle End -->

                <!-- navbar-collapse start-->
                
                <?php echo $main_menu; ?>
                    
                <!-- navbar-collapse end-->

            </div>
        </nav>
        <!-- Navigation Menu end-->

        <?php

        $page = basename($_SERVER['PHP_SELF']);

        if($page == '' || $page == 'index.php') {

        ?>
            <section class="homepage_search">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-8 col-md-offset-2 search_wrapper">
                            <h1>Find Realtors. Read Reviews.</h1>

                            <form action="search.php" method="GET" id="search">
                                <div class="form-group">
                                    <input type="text" id="search_location" class="form-control" name="city" placeholder="Find Realtors in (e.g. New York)" value="<?php echo $_SESSION['search_location']; ?>"/>
                                    <input type="hidden" id="search_location_latlng" name="latlng" />
                                    <button id="submit_form" class="btn btn-primary btn-lg"><i class="fa fa-search"></i> Search</button>
                                    <span class="error" style="display: none;"></span>
                                </div>
                                
                            </form>

                            <!-- <form action="search.php?action=search" method="POST">
                                <div class="form-group">
                                    <input type="text" id="search_location" class="form-control" name="city" placeholder="Find Realtors in (e.g. New York)" value="<?php echo $_SESSION['search_location']; ?>"/>
                                    <input type="hidden" id="search_location_id" name="city_id" value="<?php echo $_SESSION['search_location_id']; ?>" />
                                    <button type="submit" name="submit" id="submit" class="btn btn-primary btn-lg"><i class="fa fa-search"></i> Search</button>
                                </div>
                                
                            </form>

                            <ul id="location_autocomplete_list">
                                
                            </ul> -->
                        </div>
                    </div>
                </div>
            </section>

        <?php

        }

        ?>

    </header>

    <?php

    $page = basename($_SERVER['PHP_SELF']);

    if($page != '' && $page != 'index.php' && $page != 'realtor.php') {

    ?>

        <section class="do-normal-page-title-section">
            <div class="container">
                <div class="row">
                    <!-- Page Title -->
                    <div class="do-page-title-wrapper">
                        <div class="do-default-page-title col-xs-12">
                            <h2><?php echo $html['page_title']; ?></h2>
                        </div>
                    </div>
                    <!-- Page Title End -->
                </div>
            </div>
        </section>

    <?php

    }

    ?>