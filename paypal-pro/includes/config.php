<?php

	session_start();
	error_reporting(E_ALL ^ E_NOTICE);

	require(__DIR__ . "/functions.php");

	/*******************************************************************************************************
	    GENERAL SCRIPT CONFIGURATION VARIABLES
	********************************************************************************************************/
	//THIS IS TITLE ON PAGES
	//$title = "Payment Processing"; //site title

	//THIS IS ADMIN EMAIL FOR NEW PAYMENT NOTIFICATIONS.
	$admin_email = "reza_karami@outlook.com"; //this email is for notifications about new payments

	//CHANGE "USD" TO REQUIRED CURRENCY, SUPPORTED BY PROVIDER.USD, CAD, EUR
	define("PTP_CURRENCY_CODE","USD"); 

	//IF YOU NEED TO ADD MORE SERVICES JUST ADD THEM THE SAME WAY THEY APPEAR BELOW.
	// $services = array(
	// 		array("Service 1", "49.99")
	// 		);

	//NOW, IF YOU WANT TO ACTIVATE THE DROPDOWN WITH SERVICES ON THE TERMINAL
	//ITSELF, CHANGE BELOW VARIABLE TO TRUE;			
	$show_services = false;

	// set  to   RECUR  - for recurring payments, ONETIME - for 
	$payment_mode = "RECUR";


	//service name   |   price  to charge   | Billing period  "Day", "Week", "SemiMonth", "Month", "Year"   |  how many periods of previous field per billing period
	// ************* Not being used! ******************* //
	// $recur_services = array(
	// 				 array("Standard - Monthly", "39.00", "Month", "1"),
	// 				 array("Standard - Yearly", "390.00", "Year", "1"),
	// 				 array("Premium - Monthly", "79.00", "Month", "1"),
	// 				 array("Premium - Yearly", "790.00", "Year", "1"),
	// 				 array("Platinum - Monthly", "219.00", "Month", "1"),
	// 				 array("Platinum - Yearly", "2190.00", "Year", "1"),
	// 				); 

	//IF YOU'RE GOING LIVE FOLLOWING VARIABLE SHOULD BE SWITCH TO true
	// IT WILL AUTOMATICALLY REDIRECT ALL NON-HTTTPS REQUESTS TO HTTPS.
	// MAKE SURE SSL IS INSTALLED ALREADY.
	$redirect_non_https = false;
	$liveMode = false;

	/****************************************************
	//TEST CREDIT CARD CREDENTIALS for SANDBOX TESTING
	Card Type: Visa
	Account Number: 4683075410516684
	Expiration Date: Any in future
	Security Code: 123
	****************************************************/

	if(!$liveMode) {

		//TEST MODE
		define('API_USERNAME', 'reza_karami90-facilitator_api1.hotmail.com');
		define('API_PASSWORD', '1364843938');
		define('API_SIGNATURE', 'AKglUKfKZu3oUyP7czUpTgakdsimAY6Goka69iUKHrriSPOXqTH0jiZY');
		define('API_ENDPOINT', 'https://api-3t.sandbox.paypal.com/nvp');
		define('PAYPAL_URL', 'https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=');
	
	} else {

		//LIVE MODE
		define('API_USERNAME', 'your_LIVE_api_username');
		define('API_PASSWORD', 'your_LIVE_api_password');
		define('API_SIGNATURE', 'your_LIVE_api_signature_very_long_string');

		//DONT EDIT BELOW 2 LINES IF UNSURE.
		define('API_ENDPOINT', 'https://api-3t.paypal.com/nvp');
		define('PAYPAL_URL', 'https://www.paypal.com/webscr&cmd=_express-checkout&token=');
	}

	/*******************************************************************************************************
	    PAYPAL EXPRESS CHECKOUT CONFIGURATION VARIABLES
	********************************************************************************************************/
	$enable_paypal = true; //shows/hides paypal payment option from payment form.
	$paypal_merchant_email = "reza_karami@outlook.com";
	$paypal_success_url = "http://findarealtor.net/payment_processing.php?r=success";
	$paypal_cancel_url = "http://findarealtor.net/payment_processing.php?r=canceled";
	//$paypal_ipn_listener_url = "http://www.domain.com/path/to/paypal-pro-terminal/paypal_listener.php";
	//$paypal_custom_variable = "some_var";
	$paypal_currency = "USD";
	$sandbox = false; //if you want to test payments with your sandbox account change to true (you must have account at https://developer.paypal.com/ and YOU MUST BE LOGGED IN WHILE TESTING!)
	
	if($liveMode) { 
	
		$sandbox = false; 

	} else { 

		$sandbox = true; 
	}


	//DO NOT CHANGE ANYTHING BELOW THIS LINE, UNLESS SURE OF COURSE
	define("PAYMENT_MODE", $payment_mode);

	if(!$sandbox) {

	    define("PAYPAL_URL_STD","https://www.paypal.com/cgi-bin/webscr");
	
	} else {
	   
	    define("PAYPAL_URL_STD","https://www.sandbox.paypal.com/cgi-bin/webscr");
	}

	define('USE_PROXY', FALSE);
	define('PROXY_HOST', '127.0.0.1');
	define('PROXY_PORT', '808');
	define('VERSION', '2.3');
	define('ACK_SUCCESS', 'SUCCESS');
	define('ACK_SUCCESS_WITH_WARNING', 'SUCCESSWITHWARNING');

	if($redirect_non_https) {

		if ($_SERVER['SERVER_PORT']!=443) {

			$sslport = 443; //whatever your ssl port is
			$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
			
			header("Location: $url");
			
			exit();
		}
	}

?>