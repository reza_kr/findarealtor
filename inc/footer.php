	<div class="footer_wrapper">
	    <section class="do-social-link-section">
	        <div class="container">
	            <div class="row">
	            	<a href="#">
	                    Connect With Us
	                </a>
	                <a href="https://www.facebook.com/Find-A-Realtor-926833874051894/" class="do-facebook" target="_blank">
	                    <span>
	                        <i class="fa fa-facebook"></i>
	                    </span>
	                    Facebook
	                </a>
	            
	                <a href="https://twitter.com/findarealtornet" class="do-twitter" target="_blank">
	                    <span>
	                        <i class="fa fa-twitter"></i>
	                    </span>
	                    Twitter
	                </a>
	            
	                <a href="https://instagram.com/findarealtor/" class="do-instagram" target="_blank">
	                    <span>
	                        <i class="fa fa-instagram"></i>
	                    </span>
	                    Instagram
	                </a>
	            
	                <a href="https://plus.google.com/u/5/b/109938112528749079051/109938112528749079051/posts/p/pub?pageId=109938112528749079051&hl=en" class="do-googleplus" target="_blank">
	                    <span>
	                        <i class="fa fa-google-plus"></i>
	                    </span>
	                    Google+
	                </a>
	            </div>
	        </div>
	    </section>

	    <footer class="do-footer">
	        <div class="container">
	            <div class="row">

	                <div class="do-footer-top">

	                    <div class="do-footer-widget do-footer-about-widget col-md-3 col-sm-6 col-xs-12">
	                        <div class="do-footer-logo-wrapper">
	                            <img src="img/logo_wh.png" alt="FindARealtor.net">
	                        </div>

	                        <p>Let us know what you think of our service. We'd love to hear your thoughts!</p>

	                        <p>Email: <a href="mailto:feedback@findarealtor.net">feedback@findarealtor.net</a></p>
	                    </div>

	                    <div class="do-footer-widget do-footer-link-widget col-md-3 col-sm-6 col-xs-12">
	                        <h3 class="do-footer-widget-header">More Links</h3>
	                        <ul>
	                            <li><a href="about.php">About</a></li>
	                            <li><a href="faq.php">FAQ</a></li>
	                            <li><a href="review_guidelines.php">Review Guidelines</a></li>
	                            <li><a href="contact.php">Contact</a></li>
	                        </ul>
	                    </div>

	                    <div class="do-footer-widget do-about-link-widget col-md-3 col-sm-6 col-xs-12">
	                        <h3 class="do-footer-widget-header">For Realtors</h3>
	                        <ul>
	                            <li><a href="realtor_registration.php">Register as a Realtor</a></li>
	                            <li><a href="pricing.php">Plans &amp; Pricing</a></li>
	                            <li><a href="realtor_faq.php">Realtor FAQ</a></li>
	                            <li><a href="realtor_support.php">Support</a></li>
	                        </ul>
	                    </div>

	                    <div class="do-footer-widget col-md-3 col-sm-6 col-xs-12">
	                        <h3 class="do-footer-widget-header">Newsletter</h3>
	                        <div class="newsletter_signup_wrapper">
		                        <p>Sign up for our newsletter to get the latest updates right to your inbox!</p>
		                       	<input type="email" id="newsletter_email" placeholder="Email Address" />
		                       	<a href="#" class="newsletter_signup"><i class="fa fa-arrow-right"></i></a>
		                    </div>
	                    </div>

	                </div>

	                <div class="do-footer-bottom">
	                    &copy; <?php echo date('Y'); ?> FindARealtor.net. <a href="terms_of_service.php">Terms of Service</a> &middot; <a href="privacy_policy.php">Privacy Policy</a> 
	                </div>
	            </div>
	        </div>
	    </footer>
	</div>

	<?php require_once('footer_scripts.php'); ?>