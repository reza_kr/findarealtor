<?php

    require_once('functions.php');

    $html = load_html();

    if($_GET['e'] == 'password_mismatch') {

    	$message = '<div class="alert alert-danger" role="alert">Passwords don\'t match.</div>';
    
    } else if($_GET['ref'] == 'password_too_short') {

    	$message .= '<div class="alert alert-danger" role="alert">Password must be at least 8 characters long.</div>';
    
    }
    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>
    </head>

    <body class="do-contact-us-page">

        <?php require_once('inc/header.php'); ?>

        <section class="content">
            <div class="container">
                
                <div class="row">
					<div class="col-xs-12 col-md-4 col-md-offset-4">

						<form class="form" method="POST" action="?action=process&token=<?php echo htmlspecialchars($_GET['token']); ?>&email=<?php echo htmlspecialchars($_GET['email']);?>">
							<div class="form-group">
								 <label for="new_password">New Password</label>
								 <input type="password" class="form-control" id="new_password" name="new_password">
							</div>

							<div class="form-group">
								 <label for="new_password2">Confirm Password</label>
								 <input type="password" class="form-control" id="new_password2" name="new_password2">
							</div>

							<div class="form-group">
								 <button type="submit" class="btn btn-primary btn-block">Set Password</button>
							</div>
						</form>
					</div>
				</div>

            </div>
        </section>

        <?php require_once('inc/footer.php'); ?>
                                    
    </body>
</html>
