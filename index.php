<?php

	require_once('functions.php');

	$html = load_html();

?>
<!DOCTYPE html>
<html lang="en">

	<head>
	    <?php require_once('inc/head.php'); ?>
	</head>

	<body class="do-homepage-2nd home">
	    <!-- PRELOADER -->
	    <div class="preloader">
	        <div class="do-loader">&nbsp;</div>
	    </div>

	    <?php require_once('inc/header.php'); ?>

	    <section class="find_out_more">
	        <div class="container">
	            <div class="row">
                    <div class="col-xs-12 col-md-10">
                        <h3>We help to connect present and future homeowners with trustworthy Realtors.</h3>
                    </div>
                    <div class="col-xs-12 col-md-2">
                    <a href="about.php" class="btn btn-primary pull-right m-t-10 m-b-10">Find out More</a>
                    </div>
	            </div>
	        </div>
	    </section>

	    <section class="top_rated_realtors">
	    	<div class="container">
	    		<div class="row">
	    			<?php echo $html['top_rated_realtors']; ?>
	    		</div>  
	    	</div>
	    </section>

	    <section class="find_out_more">
	        <div class="container">
	            <div class="row">
                    <div class="col-xs-12 col-md-9">
                        <h3>Are you a Realtor interested in being listed on FindARealtor?</h3>
                    </div>
                    <div class="col-xs-12 col-md-3">
                    <a href="realtor_registration.php" class="btn btn-primary pull-right m-t-10 m-b-10">Create a Realtor Profile</a>
                    </div>
	            </div>
	        </div>
	    </section>

	    <?php require_once('inc/footer.php'); ?>
	              					
	</body>

</html>
