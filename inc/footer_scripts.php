	<input type="hidden" id="ssl" value="<?php echo $_SESSION['search_location']; ?>" />
	<input type="hidden" id="asl" value="<?php echo $_SESSION['autocorrect_search_location']; ?>" />

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

	<script src="js/plugins.js"></script>
	<script src="js/jquery.feedback_me.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9mg6BXYWtJ55STBikGytsqBYOurD9Zvc&libraries=places"></script>
	<script type="text/javascript" src="js/jquery.fileupload.js"></script>
	<script type="text/javascript" src="js/jquery.csv-0.71.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script>

	<script src="js/horizBarChart.js"></script>

	<script src="js/main.js"></script>
	<script type="text/javascript" src="js/custom.js?v=<?php echo time(); ?>"></script>

	<script type="text/javascript">
		<?php

			if(!isset($_SESSION['logged_in'])) {

				echo "$(document).ready(function() {
						$(document).on('click','.navbar-collapse.in',function(e) {
						    if( $(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle' ) {
						        $(this).collapse('hide');
						    }
						});
					});";
			}
		?>
	</script>
