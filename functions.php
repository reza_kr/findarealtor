<?php
	
	session_start();

	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

	$page = basename($_SERVER['PHP_SELF']);
	$full_url = basename($_SERVER['REQUEST_URI']);

	if($page != 'country_select.php' && $page != 'login.php' && $page != 'realtor_login.php' && $page != 'signup_confirmation.php' && $page != 'realtor_password_reset.php' && $page != 'password_reset.php' && $page != 'realtor_password_change.php' && $page != 'password_change.php') {

		$_SESSION['last_page'] = $full_url;
	}


	if(!isset($_SESSION['search_location'])) {
	
		//$ip_info = json_decode(file_get_contents('http://ipinfo.io/' . $_SERVER['REMOTE_ADDR'] . '/json'), true);
		$ip_info = json_decode(file_get_contents('http://ipinfo.io/192.197.54.136/json'), true);

		//if($ip_info['country'] == 'CA' || $ip_info['country'] == 'US') {
		if($ip_info['country'] == 'US') {

			$_SESSION['country'] = 'US';
			$_SESSION['search_location'] = $ip_info['city'] . ', ' . $ip_info['region'] . ', ' . $ip_info['country'];
			$_SESSION['autocorrect_search_location'] = true;

			// if(!isset($_SESSION['country'])) {

			// 	if($ip_info['country'] == 'CA') {

			// 		$_SESSION['country'] = 'CA';

			// 	} else {

			// 		$_SESSION['country'] = 'US';
			// 	}
			// }

		} else {

			//default city
			$_SESSION['country'] = 'US';
			$_SESSION['search_location'] = 'San Francisco, CA, USA';
			$_SESSION['autocorrect_search_location'] = false;
		}

	} else {

		$_SESSION['autocorrect_search_location'] = false;
	}


	require_once('lib/facebook-php-sdk/autoload.php');

	use Facebook\FacebookSession;
	use Facebook\FacebookRedirectLoginHelper;
	use Facebook\FacebookRequest;
	use Facebook\FacebookResponse;
	use Facebook\FacebookSDKException;
	use Facebook\FacebookRequestException;
	use Facebook\FacebookAuthorizationException;
	use Facebook\GraphObject;
	use Facebook\GraphUser;
	use Facebook\GraphSessionInfo;
	use Facebook\FacebookHttpable;
	use Facebook\FacebookCurlHttpClient;
	use Facebook\FacebookCurl;

	$app_id = '1664420870442959';
	$app_secret = '6b95c8912f57fb0b48f8c358ece58393';
	$redirect_url = 'http://localhost/realtor/login.php';

	FacebookSession::setDefaultApplication($app_id, $app_secret);
	$helper = new FacebookRedirectLoginHelper($redirect_url);
	$session = $helper->getSessionFromRedirect();

	if(isset($_SESSION['fb_token'])) {

		$session = new FacebookSession($_SESSION['fb_token']);
	}


	if(isset($session)) {

		$_SESSION['fb_token'] = $session->getToken();

		$request = new FacebookRequest($session, 'GET', '/me');

		$response = $request->execute();
		$graph = $response->getGraphObject(GraphUser::classname());

		$name = $graph->getName();
		$id = $graph->getId();

		$profile_picture = 'https://graph.facebook.com/' . $id . '/picture?width=200';
		$email = $graph->getProperty('email'); //NOT WORKING :/

		require_once("classes/User.class.php");
		$user = new User();

		$user->handleFacebookLogin($id, $name, $profile_picture);

		// echo 'Welcome back, ' . $_SESSION['first_name'] . ', ' . $_SESSION['last_name'] . '<br>';
		// echo 'ID: ' . $_SESSION['user_id'] . '<br>';
		// echo 'Email: N/A<br>';
		// echo 'Profile Picture:<br>' . '<img src="' . $_SESSION['profile_picture'] . '" /><br>';
		// echo '<a href="logout.php">Logout</a>';

	} else {

		$fb_login_url = $helper->getLoginUrl(array('email'));
	}


	// Keep Logged In logic - To be added later!
	// $cookie = isset($_COOKIE['keep_logged_in']) ? $_COOKIE['keep_logged_in'] : '';

	// if($cookie) {
 //        list($user_id, $token, $cookie_hashed) = explode(':', $cookie);

 //        if (hash_equals(hash($user_id . ':' . $token, $cookie_hashed)) {

 //            return false;
 //        }
 //        $usertoken = fetchTokenByUserName($user);
 //        if (timingSafeCompare($usertoken, $token)) {
 //            logUserIn($user);
 //        }
 //    }


	if($_SESSION['logged_in'] == true) {

		if($_SESSION['user_type'] == 'user') {

			if($_SESSION['profile_picture'] != '') {

				$profile_picture = '<img src="http://cdn.findarealtor.net/upp/' . $_SESSION['profile_picture'] . '" alt="' . $_SESSION['first_name'] . ' ' . $_SESSION['last_name'] . '" />';
			
			} else {

				$profile_picture = '<img src="img/anonymous.png" />';
			}

			$main_menu = '<div id="nav-menu" class="do-menu-wrapper pull-right" role="navigation">
                    <ul class="nav navbar-nav do-menus">';

			$main_menu .= '<li class="boxed hidden-xs">
                        <a href="write_a_review.php"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Write a Review</a>
                    </li>';

            $main_menu .= '<li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">' . $profile_picture . ' ' . $_SESSION['first_name'] . ' ' . $_SESSION['last_name'] . '<span class="caret"></span></a>
			          
			          <ul class="dropdown-menu">
			          	<li class="visible-xs"><a href="write_a_review.php"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Write a Review</a></li>
			          	<li role="separator" class="divider visible-xs"></li>
			            <li><a href="my_reviews.php"><i class="fa fa-book"></i>&nbsp;&nbsp;My Reviews</a></li>
			            <li><a href="account_settings.php"><i class="fa fa-cog"></i>&nbsp;&nbsp;Account Settings</a></li>
			            <li role="separator" class="divider"></li>
			            <li><a href="logout.php"><i class="fa fa-lock"></i>&nbsp;&nbsp;Log Out</a></li>
			          </ul>
			        </li>';

			$main_menu .= '</ul>
                	</div>';

			$restricted_pages = array('realtor_support.php', 'realtor_account_settings.php', 'realtor_login.php', 'realtor_password_change.php', 'realtor_password_reset.php', 'realtor_registration.php', 'request_a_review.php', 'claim_realtor_profile.php');

			if(in_array($page, $restricted_pages)) {

				header('Location: /');

			} else if($page == 'login.php' || $page == 'registration.php') {

				header('Location: /');
			}

		} else if($_SESSION['user_type'] == 'realtor') {

			if($_SESSION['profile_picture'] != '') {

				$profile_picture = '<img src="http://cdn.findarealtor.net/rpp/' . $_SESSION['profile_picture'] . '" alt="' . $_SESSION['first_name'] . ' ' . $_SESSION['last_name'] . '" />';
			
			} else {

				$profile_picture = '<img src="img/anonymous.png" />';
			}

			$main_menu = '<div id="nav-menu" class="do-menu-wrapper pull-right" role="navigation">
                    <ul class="nav navbar-nav do-menus">';

            $main_menu .= '<li class="boxed hidden-xs">
                        <a href="request_a_review.php"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Request a Review</a>
                    </li>';

            $main_menu .= '<li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">' . $profile_picture . ' ' . $_SESSION['first_name'] . ' ' . $_SESSION['last_name'] . '<span class="caret"></span></a>
			          
			          <ul class="dropdown-menu">
			          	<li class="visible-xs"><a href="request_a_review.php"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Request a Review</a></li>
			          	<li role="separator" class="divider visible-xs"></li>
			            <li><a href="realtor.php?id=' . $_SESSION['realtor_id_hashed'] . '"><i class="fa fa-user"></i>&nbsp;&nbsp;My Profile</a></li>
			            <li><a href="realtor_account_settings.php"><i class="fa fa-cog"></i>&nbsp;&nbsp;Account Settings</a></li>
			            <li role="separator" class="divider"></li>
			            <li><a href="logout.php"><i class="fa fa-lock"></i>&nbsp;&nbsp;Log Out</a></li>
			          </ul>
			        </li>';

			$main_menu .= '</ul>
                	</div>';

			$restricted_pages = array('account_settings.php', 'login.php', 'password_change.php', 'password_reset.php', 'registration.php', 'write_a_review.php', 'claim_realtor_profile.php', 'my_reviews.php');

			if(in_array($page, $restricted_pages)) {

				header('Location: /');
			
			} else if($page == 'realtor_login.php') {

				header('Location: realtor.php?id=' . $_SESSION['realtor_id_hashed']);
			
			} else if($page == 'realtor_registration.php') {

				//Handle pricing page sign up clicks
				if($_GET['ref'] == 'standard_plan') {

					header('Location: process_payment.php?ref=standard_plan');
				
				} else if($_GET['ref'] == 'pro_plan') {

					header('Location: process_payment.php?ref=pro_plan');
				
				} else if($_GET['ref'] == 'platinum_plan') {

					header('Location: process_payment.php?ref=platinum_plan');
				
				} else {

					header('Location: realtor.php?id=' . $_SESSION['realtor_id_hashed']);
				}

			} else if($page == 'write_a_review.php' || $page == 'choose_realtor.php') {

				header('Location: /'); ///////////////
			}

		}

	
	} else {

		$main_menu = '<div id="nav-menu" class="do-menu-wrapper pull-right navbar-collapse collapse" role="navigation">
                    <ul class="nav navbar-nav do-menus">';

		$main_menu .= '<li>
							<a href="login.php">Log In</a>
						</li>
						<li>
							<a href="registration.php">Sign Up</a>
						</li>
						<li class="boxed">
				            <a href="write_a_review.php">Write a Review</a>
				        </li>
				        <li>
				        	<a href="realtor_login.php">Realtor Portal</a>
				        </li>';

		$main_menu .= '</ul>
                	</div>';

		//access control - guest
		$user_pages_array = array('add_a_realtor.php', 'choose_realtor.php', 'account_settings.php', 'write_a_review.php', 'my_reviews.php');
		$realtor_pages_array = array('realtor_account_settings.php', 'upgrade.php', 'request_a_review.php', 'realtor_support.php', 'process_payment.php');

		if(in_array($page, $user_pages_array)) {

			if($page == 'write_a_review.php' && isset($_GET['id'])) {

				header('Location: login.php?id=' . $_GET['id']);
				exit;
			}

			header('Location: login.php');
			exit;
		
		} else if(in_array($page, $realtor_pages_array)) {

			if($page == 'process_payment.php' && isset($_GET['ref'])) {

				header('Location: realtor_login.php?ref=' . $_GET['ref']);
				exit;
			}

			header('Location: realtor_login.php');
			exit;
		
		}

	}


	// $main_menu .= '<li class="dropdown country_select">
	// 		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">' . $_SESSION['country'] . '<span class="caret"></span></a>
	// 		          <input type="hidden" id="c" value="' . strtolower($_SESSION['country']) . '" />
	// 		          <ul class="dropdown-menu">
	// 		            <li><a href="country_select.php?c=US">US</a></li>
	// 		            <li><a href="country_select.php?c=CA">CA</a></li>
	// 		          </ul>
	// 		        </li>';



	function load_html() {

		$data = array();

		$page = basename($_SERVER['PHP_SELF']);

		require_once("classes/Main.class.php");
		$main = new Main();

		require_once("classes/User.class.php");
		$user = new User();

		require_once("classes/Realtor.class.php");
		$realtor = new Realtor();

		require_once('classes/PseudoCrypt.class.php');
		$pseudocrypt = new PseudoCrypt();


		$pages_array = array();
		$pages_array['template_page.php'] = "Template Page";
		$pages_array['add_a_realtor.php'] = "Add a Realtor";
		$pages_array['claim_realtor_profile.php'] = "Claim Realtor Profile";
		$pages_array['realtor.php'] = "Realtor Profile";
		$pages_array['realtor_account_settings.php'] = "Account Settings";
		$pages_array['realtor_login.php'] = "Realtor Login";
		$pages_array['realtor_password_change.php'] = "Realtor Password Change";
		$pages_array['realtor_password_reset.php'] = "Realtor Password Reset";
		$pages_array['realtor_registration.php'] = "Register as a Realtor";
		$pages_array['request_a_review.php'] = "Request a Review";
		$pages_array['search.php'] = "Realtor Search";
		$pages_array['signup_confirmation.php'] = "Sign-Up Confirmation";
		$pages_array['upgrade.php'] = "Upgrade";
		$pages_array['pricing.php'] = "Plans &amp; Pricing";
		$pages_array['account_settings.php'] = "Account Settings";
		$pages_array['login.php'] = "Log In";
		$pages_array['password_change.php'] = "Password Change";
		$pages_array['password_reset.php'] = "Password Reset";
		$pages_array['registration.php'] = "Sign Up";
		$pages_array['choose_realtor.php'] = "Choose a Realtor";
		$pages_array['write_a_review.php'] = "Write a Review";
		$pages_array['my_reviews.php'] = "My Reviews";
		$pages_array['realtor_faq.php'] = "Realtor FAQ";
		$pages_array['faq.php'] = "Frequently Asked Questions";
		$pages_array['about.php'] = "About";
		$pages_array['contact.php'] = "Contact";
		$pages_array['review_guidelines.php'] = "Review Guidelines";
		$pages_array['terms_of_service.php'] = "Terms of Service";
		$pages_array['privacy_policy.php'] = "Privay Policy";
		$pages_array['process_payment.php'] = "Process Payment";
		$pages_array['realtor_support.php'] = "Realtor Support";

		if($page != 'index.php') {

			$data['page_title_meta'] = $pages_array[$page] . " – FindARealtor.net | Find the best realtors in your area. Read first-hand reviews or write one of your own!";
		
		} else {

			$data['page_title_meta'] = "FindARealtor.net | Find the best realtors in your area. Read first-hand reviews or write one of your own!";
		}


		switch($page) {

			case "":
			case "index.php":

				$data['top_rated_realtors'] = $main->topRatedRealtors();

				break;

			case "country_select.php":

				if($_GET['c'] == 'US') {

					$_SESSION['country'] = 'US';
					unset($_SESSION['search_location']);

				} else if($_GET['c'] == 'CA') {

					$_SESSION['country'] = 'CA';
					unset($_SESSION['search_location']);
				}

				if(!isset($_SESSION['last_page'])) {

					header('Location: /');
				
				} else {

					header('Location: ' . $_SESSION['last_page']);
				}

				break;

			case "realtor.php":

				if($realtor->checkRealtorId()) {

					$main->logProfileView();
				
					$data['realtor_profile'] = $realtor->loadRealtorProfile();
					$data['realtor_reviews'] = $realtor->loadRealtorReviews();

					$data['page_title_meta'] = $data['realtor_profile']['name'] . " Realtor Reviews – FindARealtor.net | Find the best realtors in your area. Read first-hand reviews or write one of your own!";

				} else {

					header('Location: /');
				}

				break;

			case "write_review.php":

				$data['realtor'] = $realtor->getName();

				if($_GET['action'] == 'post_review') {

					$user->postReview();
				}

				break;

			case "search.php":

				if(isset($_GET['city'])) {

					//save last search location to session
					$_SESSION['search_location'] = $_GET['city'];
					$_SESSION['search_location_latlng'] = $_GET['latlng'];

				} else {
					
					//check if saved search location exists in session
					if(isset($_SESSION['search_location'])) {

						header('Location: search.php?city=' . $_SESSION['search_location'] . '&latlng=' . $_SESSION['search_location_latlng']);

					}
				}

				$data['search_results'] = $main->search();

				$data['page_title_meta'] = "Realtors in " . $_GET['city'] . " – FindARealtor.net | Find the best realtors in your area. Read first-hand reviews or write one of your own!";

				break;

			case "login.php":

				if($_GET['action'] == 'user_login') {

					$user->login();
				}

				// echo 'Welcome back, ' . $_SESSION['first_name'] . ', ' . $_SESSION['last_name'] . '<br>';
				// echo 'ID: ' . $_SESSION['user_id'] . '<br>';
				// echo 'Email: ' . $_SESSION['email'] . '<br>';
				// echo 'Profile Picture:<br>' . '<img src="' . $_SESSION['profile_picture'] . '" /><br>';
				// echo '<a href="logout.php">Logout</a>';

				break;

			case "registration.php":

				if($_GET['action'] == 'user_registration') {

					$user->register();
				}

				break;

			case "realtor_login.php":

				if($_GET['action'] == 'realtor_login') {

					$realtor->login();

				}

				// echo 'Welcome back, ' . $_SESSION['first_name'] . ', ' . $_SESSION['last_name'] . '<br>';
				// echo 'ID: ' . $_SESSION['realtor_id'] . '<br>';
				// echo 'Email: ' . $_SESSION['email'] . '<br>';
				// echo 'Profile Picture:<br>' . '<img src="' . $_SESSION['profile_picture'] . '" /><br>';
				// echo '<a href="logout.php">Logout</a>';

				break;

			case "realtor_registration.php":

				if($_GET['action'] == 'realtor_registration') {

					$realtor->register();

				}

				break;


			case "realtor_account_settings.php":

				if(isset($_SESSION['realtor_id'])) {

					if($_GET['action'] == 'update_realtor_profile') {

						$realtor->updateRealtorProfile();
					}

					$data['realtor_account_overview'] = $realtor->loadRealtorAccountOverview();

				} else {

					header('Location: realtor_login.php?ref=realtor_account_settings');
				}

				break;

			case "request_a_review.php":

				if($_GET['action'] == 'request_review') {

					$realtor->requestReview();

					header('Location: request_a_review.php');
				}

				$data['request_history'] = $realtor->reviewRequestHistory();

				break;

			case "add_a_realtor.php":

				if($_GET['action'] == 'add_a_realtor') {

					$realtor->addARealtor();

				}

				break;

			case "signup_confirmation.php":

				if($_GET['ref'] != 'confirm_email_address' && $_GET['ref'] != 'registration' && $_GET['ref'] != 'realtor_registration') {

					$main->signUpConfirmation();

				}

				break;

			case "realtor_password_reset.php":

				if($_POST['email']) {

					$result = $realtor->resetPassword();
				}

				break;

			case "realtor_password_change.php":

				if($_GET['token']) {

					//let this return the email address, can be used further down
					$email = $realtor->checkPasswordResetToken();

					if($email == false) {
						//Faulty token, email address, or expired... notify somehow?!?
						//header('Location: login');

					} else if($email != "") {  

						if($_GET['action'] == "process") { 

							$result = $realtor->resetPasswordChange($email);

						}

					}

				} else {

					header('Location: realtor_login.php');
				}

				break;

			case "password_reset.php":

				if($_POST['email']) {

					$result = $user->resetPassword();

					if($result == true) {

						header('Location: password_reset.php?r=request_sent');
					
					} else {

						header('Location: password_reset.php?e=account_not_found');
					}
				}

				break;

			case "password_change.php":

				if($_GET['token'] && $_GET['email']) {

					//let this return the email address, can be used further down
					$email = $user->checkPasswordResetToken();

					if($email == false) {
						//Faulty token, email address, or expired... notify somehow?!?
						//header('Location: login');

					} else if($email != "") {  

						if($_GET['action'] == "process") { 

							$result = $user->resetPasswordChange($email);

						}

					}

				} else {

					header('Location: login.php');
				}

				break;

			case "write_a_review.php":

				if($_GET['action'] == 'post_review') {

					$user->postReview();

				} else {

					if($realtor->checkRealtorId()) {

						$data['realtor'] = $realtor->loadRealtorBasicInfo();

					} else {

						header('Location: choose_realtor.php');
						exit;
					}

				}

				break;

			case "claim_realtor_profile.php":

				if($_GET['action'] == 'claim_realtor_profile') {

					$realtor->claimRealtorProfile();
				}

				if(!isset($_GET['id'])) {

					header('Location: /');
				}

				$result = $realtor->checkRealtorId();

				if($realtor->checkRealtorId()) {

					$data['realtor'] = $realtor->loadRealtorBasicInfo();

				} else {

					header('Location: /');
					exit;
				}

				break;

			case "my_reviews.php":

				if($_GET['action'] == 'update_review') {

					$user->updateReview();
				
				} else if($_GET['action'] == 'delete_review') {

					$user->deleteReview();
				}

				$data['my_reviews'] = $user->loadMyReviews();

				break;


			case "account_settings.php":

				if(isset($_SESSION['user_id'])) {

					if($_GET['action'] == 'update_profile') {

						$user->updateUserProfile();
					}

					$data['user_account_overview'] = $user->loadUserAccountOverview();

				} else {

					header('Location: login.php?ref=account_settings');
				}

				break;

			case "contact.php":

				if($_GET['action'] == 'send_message') {

					$main->sendMessage();
				}

				break;

			case "realtor_support.php":

				if($_GET['action'] == 'send_message') {

					$realtor->sendMessage();
				}

				break;

			case "process_payment.php": 

				if($_GET['ref'] == 'complete' && isset($_GET['tx'])) {

					require_once('classes/Billing.class.php');
					$billing = new Billing();

					$billing->processPayment();
				}

				break;
				
		}

		$data['page_title'] = $pages_array[$page];

		return $data;
	}
?>