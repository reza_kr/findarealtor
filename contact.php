<?php

    require_once('functions.php');

    $html = load_html();
    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>
    </head>

    <body class="do-contact-us-page">

        <?php require_once('inc/header.php'); ?>

        <section class="content">
            <div class="container">

            	<div class="row">
                	<div class="col-xs-12 col-md-6 col-md-offset-3">
                		
                        <h2>Your feedback is important to us. Let us know what we can do for you!</h2>

                        <br>

                        <h4 class="note">Have you read through our <a href="faq.php">Homeowner FAQ</a> or <a href="realtor_faq.php">Realtor FAQ</a> pages? Many of the frequently asked questions are listed there!</h4>

                        <hr><br>

                        <form class="form" method="POST" action="?action=send_message">

                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="user"><input type="radio" id="user" name="user_type" /> I am a future/present homeowner</label>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="user"><input type="radio" id="user" name="user_type" /> I am a Realtor</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name" class="sr-only">Name</label>
                                <input type="text" id="name" name="name" class="form-control" placeholder="Name" />
                            </div>

                            <div class="form-group">
                                <label for="email" class="sr-only">Email</label>
                                <input type="text" id="email" name="email" class="form-control" placeholder="Email" />
                            </div>

                            <div class="form-group">
                                <label for="message" class="sr-only">Message</label>
                                <textarea id="message" name="message" class="form-control" placeholder="Message"></textarea>
                            </div>

                            <div class="form-group">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary btn-block" value="Send Message" />
                            </div>
                        </form>

					</div>
				</div>
            </div>
        </section>

        <?php require_once('inc/footer.php'); ?>
                                    
    </body>
</html>
         