<?php

    require_once('functions.php');

    $html = load_html();
    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>
    </head>

    <body class="do-contact-us-page">

        <?php require_once('inc/header.php'); ?>

        <section class="content">
            <div class="container">

            	<div class="row">
                	<div class="col-xs-12">
                		<h2 class="text-center">Get a head start on North America's fastest growing Realtor site!</h2>
                		<h2 class="text-center">We have a plan to suit your needs!</h2>
		                <table id="pricing_table" class="blackbold table table-striped table-hover">
		                	<tr>
		                		<td class="blank"></td>
		                		<td class="blank"></td>
		                		<td class="blank"></td>
		                		<td class="best_value">BEST VALUE</td>
		                		<td class="blank"></td>
		                	</tr>
							<tr>
								<td class="blank"></td>
								<th>FREE<br><a href="realtor_registration.php" class="btn btn-primary">SIGN UP</a></th>
								<th>STANDARD<br><a href="realtor_registration.php?ref=standard_plan" class="btn btn-primary">SIGN UP</a></th>
								<th>PRO<br><a href="realtor_registration.php?ref=pro_plan" class="btn btn-primary">SIGN UP</a></th>
								<th>PLATINUM<br><a href="realtor_registration.php?ref=platinum_plan" class="btn btn-primary">SIGN UP</a></th>
							</tr>
							<tr>
								<td class="rowTitle">Search Engine Optimized Profile</td>
								<td><i class="fa fa-check"></i></td>
								<td><i class="fa fa-check"></i></td>
								<td><i class="fa fa-check"></i></td>
								<td><i class="fa fa-check"></i></td>
							</tr>
							<tr>
								<td class="rowTitle">Contact Info</td>
								<td><i class="fa fa-check"></i></td>
								<td><i class="fa fa-check"></i></td>
								<td><i class="fa fa-check"></i></td>
								<td><i class="fa fa-check"></i></td>
							</tr>
							<tr>
								<td class="rowTitle">Profile Photo</td>
								<td><i class="fa fa-close"></i></td>
								<td><i class="fa fa-check"></i></td>
								<td><i class="fa fa-check"></i></td>
								<td><i class="fa fa-check"></i></td>
							</tr>
							<tr>
								<td class="rowTitle">Link to Website</td>
								<td><i class="fa fa-close"></i></td>
								<td><i class="fa fa-check"></i></td>
								<td><i class="fa fa-check"></i></td>
								<td><i class="fa fa-check"></i></td>
							</tr>
							<tr>
								<td class="rowTitle">Service Area(s)</td>
								<td><strong>1</strong> (Standard)</td>
								<td><strong>1</strong> (Extended)</td>
								<td><strong>3</strong> (Extended)</td>
								<td><strong>6</strong> (Extended)</td>
							</tr>
							<tr>
								<td class="rowTitle">Sales Pitch Paragraph</td>
								<td><i class="fa fa-close"></i></td>
								<td><i class="fa fa-check"></i></td>
								<td><i class="fa fa-check"></i></td>
								<td><i class="fa fa-check"></i></td>
							</tr>
							<tr>
								<td class="rowTitle">Verified Realtor Badge</td>
								<td><i class="fa fa-close"></i></td>
								<td><i class="fa fa-check"></i></td>
								<td><i class="fa fa-check"></i></td>
								<td><i class="fa fa-check"></i></td>
							</tr>
							<tr>
								<td class="rowTitle">Respond to Reviews</td>
								<td><i class="fa fa-close"></i></td>
								<td><i class="fa fa-close"></i></td>
								<td><i class="fa fa-check"></i></td>
								<td><i class="fa fa-check"></i></td>
							</tr>
							<tr>
								<td class="blank"></td>
								<td class="price_td"><span class="price">FREE</span><br><a href="realtor_registration.php" class="btn btn-primary">SIGN UP</a></td>
								<td class="price_td"><span class="price">$39</span>/Month<br><a href="realtor_registration.php?ref=standard_plan" class="btn btn-primary">SIGN UP</a></td>
								<td class="price_td"><span class="price">$79</span>/Month<br><a href="realtor_registration.php?ref=pro_plan" class="btn btn-primary">SIGN UP</a></td>
								<td class="price_td"><span class="price">$139</span>/Month<br><a href="realtor_registration.php?ref=platinum_plan" class="btn btn-primary">SIGN UP</a></td>
							</tr>
						</table>
					</div>
				</div>
            </div>
        </section>

        <?php require_once('inc/footer.php'); ?>
                                    
    </body>
</html>
         