<?php

    require_once('functions.php');

    $html = load_html();

    if($_GET['ref'] == 'review_updated') {

        $message = '<div class="alert alert-success" role="alert">Your review was updated and is now pending approval. Once approved it will be published on the Realtor\'s Profile.</div>';
    
    } else if($_GET['ref'] == 'review_deleted') {

        $message = '<div class="alert alert-success" role="alert">Your review was successfully removed. Feel free to write another one!</div>';
    }    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>
    </head>

    <body class="do-contact-us-page">

        <div class="content_wrapper">

            <?php require_once('inc/header.php'); ?>

            <section class="content">
                <div class="container">
                    
                    <div class="row">
                        <div class="col-xs-12">

                            <?php echo $message; ?>

                            <table class="table table-striped table-bordered my_reviews">
                                <tr>
                                    <th>Realtor</th>
                                    <th>Review</th>
                                    <th>Rating</th>
                                    <th>Anonymous</th>
                                    <th>Date Posted</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                <?php echo $html['my_reviews']; ?>
                            </table>

                        </div>
                    </div>

                </div>
            </section>

            <div class="modal fade" id="edit_review_modal">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <form action="?action=update_review" method="POST">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                            <h4 class="modal-title"><i class="fa fa-pencil"></i> Edit Review</h4>
                        </div>
                        <div class="modal-body">
                            
                            <div class="form-group">
                                <label>How would you rate your satisfaction with this Realtor?</label>
                            
                                <div class="star_rating">
                                    <input id="s_10" name="satisfaction" type="radio" value="10" /><label for="s_10">10</label>
                                    <input id="s_9" name="satisfaction" type="radio" value="9" /><label for="s_9">9</label>
                                    <input id="s_8" name="satisfaction" type="radio" value="8" /><label for="s_8">8</label>
                                    <input id="s_7" name="satisfaction" type="radio" value="7" /><label for="s_7">7</label>
                                    <input id="s_6" name="satisfaction" type="radio" value="6" /><label for="s_6">6</label>
                                    <input id="s_5" name="satisfaction" type="radio" value="5" /><label for="s_5">5</label>
                                    <input id="s_4" name="satisfaction" type="radio" value="4" /><label for="s_4">4</label>
                                    <input id="s_3" name="satisfaction" type="radio" value="3" /><label for="s_3">3</label>
                                    <input id="s_2" name="satisfaction" type="radio" value="2" /><label for="s_2">2</label>
                                    <input id="s_1" name="satisfaction" type="radio" value="1" /><label for="s_1">1</label>
                                </div>

                                <span id="satisfaction_rating"></span>
                            </div>

                            <div class="form-group">
                                <label for="details">Write about your experience with this Realtor</label>
                                <textarea id="details" name="details" class="form-control"></textarea>
                            </div>

                            <div class="form-group">
                                <input name="anonymous" id="anonymous" type="checkbox" class="form-control">
                                <label for="anonymous">Post review Anonymously</label>
                            </div>

                            <input type="hidden" id="review_id" name="review_id" />

                            <div class="alert alert-warning" role="alert">Making changes to an already approved review will make it subject to another approval process.</div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <input type="submit" name="submit" class="btn btn-primary save_edit_review" value="Save Changes" />
                        </div>

                        </form>

                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div class="modal fade" id="delete_review_modal">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <form action="?action=delete_review" method="POST">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                            <h4 class="modal-title"><i class="fa fa-trash-o"></i> Delete Review</h4>
                        </div>
                        <div class="modal-body">

                            <p>Are you sure you want to delete this review? This action is permanent and cannot be undone!</p>
                            
                            <input type="hidden" name="review_id" id="review_id" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <input type="submit" name="submit" class="btn btn-danger" value="Delete" />
                        </div>

                        </form>

                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div class="push"></div>
        </div>

        <?php require_once('inc/footer.php'); ?>
                                    
    </body>
</html>
