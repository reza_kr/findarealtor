<?php

    require_once('functions.php');

    $html = load_html();

    if($_GET['e'] == 'missing_fields') {

    	$message = '<div class="alert alert-danger" role="alert">Please complete all the required fields.</div>';

    } else if($_GET['e'] == 'invalid_email') {

    	$message = '<div class="alert alert-danger" role="alert">Please provide a valid contact email or leave the field blank.</div>';

    } else if($_GET['e'] == 'invalid_phone') {

    	$message = '<div class="alert alert-danger" role="alert">Please provide a valid phone number.</div>';
    
    }
    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>
    </head>

    <body class="do-contact-us-page">

        <?php require_once('inc/header.php'); ?>

        <section class="content">
        	<div class="container">
        		
        		<div class="row">
					<div class="col-xs-12 col-md-6 col-md-offset-3">

						<?php echo $message; ?>

						<h4 class="note">If you are the Realtor, please <a href="realtor_registration.php">Register as a Realter here</a>.</h4>

						<form class="form" method="POST" action="?action=add_a_realtor" id="add_a_realtor">
							<div class="form-group">
								 <label for="first_name">First Name</label>
								 <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
							</div>
							<div class="form-group">
								 <label for="last_name">Last Name</label>
								 <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
							</div>
							<div class="form-group">
								 <label for="phone">Phone #</label>
								 <input type="phone" class="form-control" id="phone" name="phone" placeholder="Phone Number">
							</div>
							<div class="form-group">
								 <label for="email">Email Address (optional)</label>
								 <input type="email" class="form-control" id="email" name="email" placeholder="Contact Email">
							</div>
							<!-- <hr>
							<div class="form-group">
								 <label for="country">Country</label>
								 <select class="form-control" id="country" name="country">
								 	<option value="us" selected="selected">Unites States</option>
								 	<option value="ca">Canada</option>
								 </select>
							</div> -->
							<div class="form-group">
								 <label for="service_area">City</label>
								 <input type="text" class="form-control" id="service_area" name="service_area" placeholder="Service Area" autocomplete="false">
								 <span class="city_error" style="display: none;"></span>
							</div>
							<hr>
							<div class="form-group">
								 <button id="submit_form" class="btn btn-primary btn-block">Add Realtor</button>
							</div>
						</form>

					</div>
				</div>

        	</div>
        </section>

        <?php require_once('inc/footer.php'); ?>
                  					
    </body>
</html>
