<?php

    require_once('functions.php');

    $html = load_html();
    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>
    </head>

    <body class="do-contact-us-page">

        <?php require_once('inc/header.php'); ?>

        <section class="content">
            <div class="container">
                
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="text-center">Got a question you need answers to?<br>Our Frequently Asked Questions might have your answers!</h2>
                        <h3 class="text-center">You can also get in touch with us by sending us a message.</h3>
                    </div>
                </div>

                <br><hr><br>

                <div class="row">
                    <div class="col-xs-12 faq">
                        <div class="faq_question">
                            <h4>What is FindARealtor?</h4>
                        </div>
                        <div class="faq_answer">
                            <p>We verify each onesssssss brooooo<br>
                            yeah man... true..</p>
                        </div>
                    </div>

                    <div class="col-xs-12 faq">
                        <div class="faq_question">
                            <h4>How is FindARealtor different from having a personal Realtor website?</h4>
                        </div>
                        <div class="faq_answer">
                            <p>We verify each onesssssss brooooo<br>
                            yeah man... true..</p>
                        </div>
                    </div>

                    <div class="col-xs-12 faq">
                        <div class="faq_question">
                            <h4>How Does FindARealtor work?</h4>
                        </div>
                        <div class="faq_answer">
                            <p>We verify each onesssssss brooooo<br>
                            yeah man... true..</p>
                        </div>
                    </div>

                    <div class="col-xs-12 faq">
                        <div class="faq_question">
                            <h4>How do I improve my visibility?</h4>
                        </div>
                        <div class="faq_answer">
                            <p>We verify each onesssssss brooooo<br>
                            yeah man... true..</p>
                        </div>
                    </div>

                    <div class="col-xs-12 faq">
                        <div class="faq_question">
                            <h4>How do I get more reviews/ratings?</h4>
                        </div>
                        <div class="faq_answer">
                            <p>We verify each onesssssss brooooo<br>
                            yeah man... true..</p>
                        </div>
                    </div>

                    <div class="col-xs-12 faq">
                        <div class="faq_question">
                            <h4>How do I respond to a bad review?</h4>
                        </div>
                        <div class="faq_answer">
                            <p>We verify each onesssssss brooooo<br>
                            yeah man... true..</p>
                        </div>
                    </div>

                    <div class="col-xs-12 faq">
                        <div class="faq_question">
                            <h4>How do you safeguard against fake reviews?</h4>
                        </div>
                        <div class="faq_answer">
                            <p>We verify each onesssssss brooooo<br>
                            yeah man... true..</p>
                        </div>
                    </div>

                    <div class="col-xs-12 faq">
                        <div class="faq_question">
                            <h4>What is the purpose of setting service areas?</h4>
                        </div>
                        <div class="faq_answer">
                            <p>We verify each onesssssss brooooo<br>
                            yeah man... true..</p>
                        </div>
                    </div>

                    <div class="col-xs-12 faq">
                        <div class="faq_question">
                            <h4>How will multiple service areas help me?</h4>
                        </div>
                        <div class="faq_answer">
                            <p>We verify each onesssssss brooooo<br>
                            yeah man... true..</p>
                        </div>
                    </div>

                    <div class="col-xs-12 faq">
                        <div class="faq_question">
                            <h4>Can I cancel my plan at any time?</h4>
                        </div>
                        <div class="faq_answer">
                            <p>We verify each onesssssss brooooo<br>
                            yeah man... true..</p>
                        </div>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="text-center">Still have questions?&nbsp;&nbsp;&nbsp;<a href="contact.php" class="btn btn-primary m-t-10 m-b-10">Send us a message!</a></h3>
                    </div>
                </div>

            </div>
        </section>

        <?php require_once('inc/footer.php'); ?>
                                    
    </body>
</html>
