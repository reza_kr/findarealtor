<?php

    require_once('functions.php');

    $html = load_html();
    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>
    </head>

    <body class="do-contact-us-page">

        <?php require_once('inc/header.php'); ?>

        <section class="content">
            <div class="container">

            	<div class="row">
                	<div class="col-xs-12 col-md-6 col-md-offset-3">
                		
                        <h2>Need assistance getting set up? Our support team is here to help you!</h2>

                        <br>

                        <h3>Send us a message with your questions and we will get back to you shortly.</h3>

                        <hr><br>

                        <form class="form" method="POST" action="?action=send_message">

                            <div class="form-group">
                                <label for="name" class="sr-only">Name</label>
                                <input type="text" id="name" name="name" class="form-control" placeholder="Name" value="<?php echo $_SESSION['first_name'] . ' ' . $_SESSION['last_name']; ?>" />
                            </div>

                            <div class="form-group">
                                <label for="email" class="sr-only">Email</label>
                                <input type="text" id="email" name="email" class="form-control" placeholder="Email" value="<?php echo $_SESSION['email'] ?>" />
                            </div>

                            <div class="form-group">
                                <label for="message" class="sr-only">Message</label>
                                <textarea id="message" name="message" class="form-control" placeholder="Message"></textarea>
                            </div>

                            <div class="form-group">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary btn-block" value="Send Message" />
                            </div>
                        </form>

					</div>
				</div>
            </div>
        </section>

        <?php require_once('inc/footer.php'); ?>
                                    
    </body>
</html>
         