<?php

    require_once('functions.php');

    $html = load_html();

    if($_GET['e'] == 'rating_needed') {

    	$message = '<div class="alert alert-danger" role="alert">Please provide a rating for your review.</div>';

    } else if($_GET['e'] == 'review_too_short') {

    	$message = '<div class="alert alert-danger" role="alert">Your review is too short. Please write at least 30 words.</div>';

    }
    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>
    </head>

    <body class="do-contact-us-page">

        <?php require_once('inc/header.php'); ?>

        <section class="content">
            <div class="container">
            	
            	<div class="row">
					<div class="col-xs-12 col-md-6 col-md-offset-3 write_a_review_info">
						<?php echo $message; ?>

						<?php echo $html['realtor']['profile_picture']; ?>
						<h2><?php echo $html['realtor']['name']; ?></h2>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 col-md-6 col-md-offset-3">
						<hr>
					</div>
				</div>

                <div class="row">
					<div class="col-xs-12 col-md-6 col-md-offset-3">

		                <form method="POST" action="?action=post_review" id="write_a_review">
		                	<div class="form-group">
								<label>How would you rate your satisfaction with this Realtor?</label>
							
								<div class="star_rating">
									<input id="s_10" name="satisfaction" type="radio" value="10" /><label for="s_10">10</label>
									<input id="s_9" name="satisfaction" type="radio" value="9" /><label for="s_9">9</label>
									<input id="s_8" name="satisfaction" type="radio" value="8" /><label for="s_8">8</label>
									<input id="s_7" name="satisfaction" type="radio" value="7" /><label for="s_7">7</label>
									<input id="s_6" name="satisfaction" type="radio" value="6" /><label for="s_6">6</label>
									<input id="s_5" name="satisfaction" type="radio" value="5" /><label for="s_5">5</label>
									<input id="s_4" name="satisfaction" type="radio" value="4" /><label for="s_4">4</label>
									<input id="s_3" name="satisfaction" type="radio" value="3" /><label for="s_3">3</label>
									<input id="s_2" name="satisfaction" type="radio" value="2" /><label for="s_2">2</label>
									<input id="s_1" name="satisfaction" type="radio" value="1" /><label for="s_1">1</label>
								</div>

								<span id="satisfaction_rating"></span>
							</div>
							<br><br>
							<div class="form-group">
								<label for="details">Write about your experience with this Realtor</label>
								<textarea id="details" name="details" class="form-control"></textarea>
							</div>

							<div class="form-group">
								<input name="anonymous" id="anonymous" type="checkbox" class="form-control">
								<label for="anonymous">Post review Anonymously</label>
							</div>

							<input type="hidden" name="rid" value="<?php echo htmlspecialchars($_GET['id']); ?>" />
							<input type="submit" name="submit" class="btn btn-primary btn-block" value="Confirm Review" />
						</form>
					</div>
				</div>
            </div>
        </section>

        <?php require_once('inc/footer.php'); ?>
                                    
    </body>
</html>
