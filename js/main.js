jQuery(document).ready(function(){
	"use strict";


/* ------------------------------------------------------------------
                PRELOADER                     
------------------------------------------------------------------ */
// makes sure the whole site is loaded
jQuery(window).load(function() {
        // will first fade out the loading animation
    jQuery(".status").fadeOut();
        // will fade out the whole DIV that covers the website.
    jQuery(".preloader").delay(1000).fadeOut("slow");
});


/* ------------------------------------------------------------------
                FOR SCROLL UP BUTTON                     
------------------------------------------------------------------ */
	jQuery.scrollUp({
		scrollName: 'scrollUp', // Element ID
		scrollDistance: 300, // Distance from top/bottom before showing element (px)
		scrollFrom: 'top', // 'top' or 'bottom'
		scrollSpeed: 300, // Speed back to top (ms)
		easingType: 'linear', // Scroll to top easing (see http://easings.net/)
		animation: 'fade', // Fade, slide, none
		animationInSpeed: 200, // Animation in speed (ms)
		animationOutSpeed: 200, // Animation out speed (ms)
		scrollText: 'Scroll to top', // Text for element, can contain HTML
		scrollTitle: false, // Set a custom <a> title if required. Defaults to scrollText
		scrollImg: true, // Set true to use image
		activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
		zIndex: 2147483647 // Z-Index for the overlay
	});

/* ==================================================================
				Change the menu height on scroll
================================================================== */
	$(window).on('scroll', function() {
	    if ($(window).scrollTop() > 1) {
	        $('.do-main-menu').addClass('minified');
	    } else {
	        $('.do-main-menu').removeClass('minified');
	    }
	});

/* ------------------------------------------------------------------
                USED FOR CLICK TO HIDE MENU                     
------------------------------------------------------------------ */
// jQuery(".nav a").on("click", function () {
//     jQuery("#nav-menu").removeClass("in").addClass("collapse")
// });

$(".navbar-collapse").css({ maxHeight: $(window).height() - $(".navbar-header").height() + "px" });

/* ------------------------------------------------------------------
                SMOOTH SCROll
------------------------------------------------------------------ */
smoothScroll.init({
    speed: 1500, // Integer. How fast to complete the scroll in milliseconds
    easing: 'easeInOutCubic', // Easing pattern to use
    updateURL: true, // Boolean. Whether or not to update the URL with the anchor hash on scroll
    offset: 0, // Integer. How far to offset the scrolling anchor location in pixels
    callback: function ( toggle, anchor ) {} // Function to run after scrolling
});


});
