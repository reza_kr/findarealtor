<?php

	session_start();
	
	Class Realtor { 

		public function __construct () {

			require("inc/connect_dbo.php");
			$this->dbo = $dbo;

			require_once('PseudoCrypt.class.php');
			$this->pseudocrypt = new PseudoCrypt();

		}


		public function register() {

			$first_name = htmlspecialchars($_POST['first_name']);
			$last_name = htmlspecialchars($_POST['last_name']);
			$email = htmlspecialchars($_POST['email']);
			$password = htmlspecialchars($_POST['password']);

			//$country = htmlspecialchars($_POST['country']);
			$city = htmlspecialchars($_POST['service_area']);

			if($email == '' || $password == '' || $first_name == '' || $last_name == '' || $city == '') {

				header('Location: realtor_registration.php?e=missing_fields');
				exit;
			
			} else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {

				header('Location: realtor_registration.php?e=invalid_email');
				exit;

			} else if(strlen($password) < 8) {

				header('Location: realtor_registration.php?e=password_too_short');
				exit;
			
			}

			$password_hashed = hash('SHA1', $password);

			$token = hash('SHA256', time());

			$datetime = date('Y-m-d H:i:s');

			$query = $this->dbo->prepare("SELECT COUNT(*) FROM realtor_accounts WHERE email = ?");
			$query->execute(array($email));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$num_rows = $row[0];
			}

			if($num_rows == 0) {

				$query = $this->dbo->prepare("INSERT INTO realtor_accounts SET first_name = ?, last_name = ?, email = ?, password = ?, date_joined = ?, account_type = ?, token = ?, country = ?, last_login = ?");
				$query->execute(array($first_name, $last_name, $email, $password_hashed, $datetime, 'free', $token, 'US', $datetime));

				$realtor_id = $this->dbo->lastInsertId();

				$query = $this->dbo->prepare("INSERT INTO realtor_service_area SET realtor_id = ?, city = ?");
				$query->execute(array($realtor_id, $city));

				require_once('Email.class.php');
				$mail = new Email();

				$mail->signUpConfirmation($first_name, $last_name, $email, 'realtor', $token);

				header('Location: signup_confirmation.php?ref=confirm_email_address');

			} else {

				//Email in use...
				header('Location: realtor_registration.php?e=email_in_use');
				exit;

			}

		}


		public function login() {

			$email = htmlspecialchars($_POST['email']);
			$password = htmlspecialchars($_POST['password']);

			if($email == '' || $password == '') {

				header('Location: realtor_login.php?e=missing_fields');
				exit;
			
			} else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {

				header('Location: realtor_login.php?e=invalid_email');
				exit;

			} else if(strlen($password) < 8) {

				header('Location: realtor_login.php?e=password_too_short');
				exit;
			}

			$password_hashed = hash('SHA1', $password);

			$query = $this->dbo->prepare("SELECT id, first_name, last_name, account_type, profile_picture, country FROM realtor_accounts WHERE email = ? AND password = ?");
			$query->execute(array($email, $password_hashed));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$datetime = date('Y-m-d H:i:s');

				$query2 = $this->dbo->prepare("UPDATE realtor_accounts SET last_login = ? WHERE id = ?");
				$query2->execute(array($datetime, $row[0]));

				$_SESSION['realtor_id'] = $row[0];
				$_SESSION['realtor_id_hashed'] = $this->pseudocrypt->hash($row[0], 6);
				$_SESSION['first_name'] = $row[1];
				$_SESSION['last_name'] = $row[2];
				$_SESSION['account_type'] = $row[3];
				$_SESSION['profile_picture'] = $row[4];
				$_SESSION['email'] = $email;
				$_SESSION['logged_in'] = true;
				$_SESSION['user_type'] = 'realtor';
				$_SESSION['realtor_country'] = $row[5];

				if(isset($_SESSION['last_page'])) {

					if(isset($_GET['id'])) {
						header('Location: ' . $_SESSION['last_page'] . '?ref=' . $_GET['ref']);
						exit;
					} else {
						header('Location: ' . $_SESSION['last_page']);
						exit;
					}

				} else {

					header('Location: /');
					exit;
				}
			}

			header('Location: realtor_login.php?e=incorrect_login');
			exit;
		}


		public function resetPassword() {
	    	
	    	$email = htmlspecialchars($_POST['email']);

	    	if(filter_var($email, FILTER_VALIDATE_EMAIL)) {

				$query = $this->dbo->prepare("SELECT id, first_name, last_name FROM realtor_accounts WHERE email = ?");
				$query->execute(array($email));

				$result = $query->fetchAll();

				foreach($result as $row) {
				    
				    $realtor_id = $row[0];
				    $first_name = $row[1];
				    $last_name = $row[2];

				    $token = hash('SHA256', time());

				    $datetime = date('Y-m-d H:i:s', time() + 86400); //NOW PLUS 1 DAY for EXPIRY

				    $query = $this->dbo->prepare("INSERT INTO password_reset_token SET realtor_id = ?, email = ?, token = ?, expiry = ?");
					$query->execute(array($realtor_id, $email, $token, $datetime));

					require_once("classes/Email.class.php");
					$mail = new Email();

					$mail->resetPassword($first_name, $last_name, $email, 'realtor', $token);

					header('Location: realtor_password_reset.php?ref=request_sent');
					exit;

				}

				header('Location: realtor_password_reset.php?e=account_not_found');
				exit;

			} else {

				header('Location: realtor_password_reset.php?e=invalid_email');
				exit;
			}

	    }


	    public function checkPasswordResetToken() {

			$token = htmlspecialchars($_GET['token']);
			$email = html_entity_decode(htmlspecialchars($_GET['email']));

			$datetime = date('Y-m-d H:i:s');

			$query = $this->dbo->prepare("SELECT email FROM password_reset_token WHERE email = ? AND token = ? AND expiry >= ?");
			$query->execute(array($email, $token, $datetime));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$found_email = $row[0];

			}

			if($found_email != "") {

				return $found_email;

			} else {

				header('Location: realtor_password_reset.php?e=invalid_reset_token');
			}

		}



		public function resetPasswordChange($email) {

			$new_password = htmlspecialchars($_POST['new_password']);
			$new_password2 = htmlspecialchars($_POST['new_password2']);

			$token = htmlspecialchars($_GET['token']);


			if($new_password == $new_password2) {

				//if (preg_match('/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/', $new_password)) {
				if(strlen($new_password) >= 8) {

					$new_password = hash('SHA1', $new_password);

					$query = $this->dbo->prepare("UPDATE realtor_accounts SET password = ? WHERE email = ?");
					$query->execute(array($new_password, $email));

			    	$query = $this->dbo->prepare("DELETE FROM password_reset_token WHERE email = ?");
					$query->execute(array($email));


			    	header('Location: realtor_login.php?ref=password_change_successful');

				} else {

					header('Location: realtor_password_change.php?token=' . $token . '&email=' . $email . '&e=password_too_short');
				}

			} else {

				header('Location: realtor_password_change.php?token=' . $token . '&email=' . $email . '&e=password_mismatch');
			}
		}


		public function checkRealtorId() {

			$realtor_id = $this->pseudocrypt->unhash(htmlspecialchars($_GET['id']));

			$query = $this->dbo->prepare("SELECT * FROM realtor_accounts WHERE id = ?");
			$query->execute(array($realtor_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				//ID found
				return true;
			}

			//ID not found
			return false;
		}


		public function loadRealtorBasicInfo() {

			$realtor_id = $this->pseudocrypt->unhash(htmlspecialchars($_GET['id']));

			$query = $this->dbo->prepare("SELECT first_name, last_name, profile_picture FROM realtor_accounts WHERE id = ?");
			$query->execute(array($realtor_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$first_name = $row[0];
				$last_name = $row[1];
				$profile_picture = $row[2];

				$data['name'] = $first_name . ' ' . $last_name;

				if($profile_picture != '') {
					$data['profile_picture'] = '<img src="http://cdn.findarealtor.net/rpp/' . $profile_picture . '" alt="' . $first_name . ' ' . $last_name . '" />';
				} else {
					$data['profile_picture'] = '<img src="img/anonymous.png" alt="Anonymous" />';
				}
			}

			return $data;
		}


		public function realtorAvgRating() {

			$realtor_id = $this->pseudocrypt->unhash(htmlspecialchars($_GET['id']));

			$count = 0;
			$total = 0;

			$query = $this->dbo->prepare("SELECT rating FROM reviews WHERE realtor_id = ? AND status = ?");
			$query->execute(array($realtor_id, 'accepted'));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$total += $row[0];
				$count++;
			}

			return round($total / $count, 1);
		}


		public function loadRealtorProfile() {

			$realtor_id = $this->pseudocrypt->unhash(htmlspecialchars($_GET['id']));

			$data = array();
			$service_area_array = array();

			$query = $this->dbo->prepare("SELECT first_name, last_name, bio, website, phone, contact_email, account_type, profile_picture, unclaimed, verified_realtor FROM realtor_accounts WHERE id = ?");
			$query->execute(array($realtor_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$first_name = $row[0];
				$last_name = $row[1];
				$bio = nl2br($row[2]);
				$website = $row[3];
				$phone = $row[4];
				$email = $row[5];
				$account_type = $row[6];
				$profile_picture = $row[7];
				$unclaimed = $row[8];
				$verified_realtor = $row[9];
			}

			$count = 0;
			$total = 0;
			$ratings = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

			$query = $this->dbo->prepare("SELECT rating FROM reviews WHERE realtor_id = ? AND status = ?");
			$query->execute(array($realtor_id, 'accepted'));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$ratings[$row[0]]++;

				$total += $row[0];
				$count++;
			}

			$data['num_reviews'] = $count;

			if($data['num_reviews'] > 0) {

				$data['ratings_breakdown'] = '<ul class="chart">';

				for ($i = 10; $i > 0; $i--) {
					
					$data['ratings_breakdown'] .= '<li title="' . $i . ' &#9733;"><span class="bar" data-number="' . $ratings[$i] . '"></span><span class="number">' . $ratings[$i] . '</span></li>';
				}

				$data['ratings_breakdown'] .= '</ul>';

				$data['avg_rating'] = '<span class="avg_rating_star">' . (round($total / $count, 1)) . '</span>';

			} else {

				$data['ratings_breakdown'] .= '<p>No ratings yet</p>';

				$data['avg_rating'] = '<p>No ratings yet</p><br>';
			}


			


			$data['name'] = $first_name . ' ' . $last_name;

			if($phone != '') {

				$data['phone'] = '<span class="info_item phone" data-val="' . $phone . '"><i class="fa fa-phone"></i> Phone</a></span>';
			}

			if($email != '') {

				$data['email'] = '<span class="info_item email" data-val="' . $email . '"><i class="fa fa-envelope"></i> Email</a></span>';
			}

			//$data['agency'] = '<img src="http://josiestern.com/images/2012/04/Sutton-Group-Logoi.png" alt="Sutton Group Status Realty Inc. Brokerage" class="agency_logo" />
			//					<span class="agency_name">Sutton Group Status Realty Inc. Brokerage</span>';

			if($verified_realtor == 1) {

				$data['verified_realtor'] = '<span class="verified_realtor" title="This Realtor is a verified user"><img src="img/logo_thumb_sm.png" /> VERIFIED REALTOR</span>';
			}

			if($account_type == 'free') {

				if($profile_picture != '') {
					$data['profile_picture'] = '<img class="profile_picture" src="http://cdn.findarealtor.net/rpp/' . $profile_picture . '" alt="' . $first_name . ' ' . $last_name . '" />';
				} else {
					$data['profile_picture'] = '<img class="profile_picture" src="img/anonymous.png" alt="Anonymous" />';
				}

				$service_area = $this->loadRealtorServiceArea($realtor_id, 'free');

				foreach($service_area as $area) {

					$service_area_string .= '<span class="service_area">' . $area . '</span>';

					array_push($service_area_array, $area);
				}

				$data['service_area'] = $service_area_string;

			} else if($account_type == 'standard') {

				if($profile_picture != '') {
					$data['profile_picture'] = '<img class="profile_picture" src="http://cdn.findarealtor.net/rpp/' . $profile_picture . '" alt="' . $first_name . ' ' . $last_name . '" />';
				} else {
					$data['profile_picture'] = '<img class="profile_picture" src="img/anonymous.png" alt="Anonymous" />';
				}

				if($website != '') {

					$data['website'] = '<span class="info_item website"><a href="' . $website . '" target="_blank"><i class="fa fa-link"></i> Website</a></span>';
				}

				$service_area = $this->loadRealtorServiceArea($realtor_id, 'standard');

				foreach($service_area as $area) {

					$service_area_string .= '<span class="service_area">' . $area . '</span>';

					array_push($service_area_array, $area);
				}

				$data['service_area'] = $service_area_string;

				if($bio != '') {

					$data['bio'] = '<h3>More About ' . $data['name'] . '</h3>
	                        <div class="more_about_realtor">
	                        <i class="fa fa-quote-left"></i>
	                        ' . $bio . '
	                        <i class="fa fa-quote-right"></i>
	                        </div>';
				}
			
			} else if($account_type == 'pro') {

				if($profile_picture != '') {
					$data['profile_picture'] = '<img class="profile_picture" src="http://cdn.findarealtor.net/rpp/' . $profile_picture . '" alt="' . $first_name . ' ' . $last_name . '" />';
				} else {
					$data['profile_picture'] = '<img class="profile_picture" src="img/anonymous.png" alt="Anonymous" />';
				}

				if($website != '') {

					$data['website'] = '<span class="info_item website"><a href="' . $website . '" target="_blank"><i class="fa fa-link"></i> Website</a></span>';
				}

				$service_area = $this->loadRealtorServiceArea($realtor_id, 'pro');

				foreach($service_area as $area) {

					$service_area_string .= '<span class="service_area">' . $area . '</span>';

					array_push($service_area_array, $area);
				}

				$data['service_area'] = $service_area_string;

				if($bio != '') {
					
					$data['bio'] = '<h3>More About ' . $data['name'] . '</h3>
	                        <div class="more_about_realtor">
	                        <i class="fa fa-quote-left"></i>
	                        ' . $bio . '
	                        <i class="fa fa-quote-right"></i>
	                        </div>';
				}
			
			} else if($account_type == 'platinum') {

				if($profile_picture != '') {
					$data['profile_picture'] = '<img class="profile_picture" src="http://cdn.findarealtor.net/rpp/' . $profile_picture . '" alt="' . $first_name . ' ' . $last_name . '" />';
				} else {
					$data['profile_picture'] = '<img class="profile_picture" src="img/anonymous.png" alt="Anonymous" />';
				}

				if($website != '') {

					$data['website'] = '<span class="info_item website"><a href="' . $website . '" target="_blank"><i class="fa fa-link"></i> Website</a></span>';
				}

				$service_area = $this->loadRealtorServiceArea($realtor_id, 'platinum');

				foreach($service_area as $area) {

					$service_area_string .= '<span class="service_area">' . $area . '</span>';

					array_push($service_area_array, $area);
				}

				$data['service_area'] = $service_area_string;

				if($bio != '') {
					
					$data['bio'] = '<h3>More About ' . $data['name'] . '</h3>
	                        <div class="more_about_realtor">
	                        <i class="fa fa-quote-left"></i>
	                        ' . $bio . '
	                        <i class="fa fa-quote-right"></i>
	                        </div>';
				}
			
			}

			$data['service_area_json'] = json_encode($service_area_array);

			if($realtor_id == $_SESSION['realtor_id']) {

				$data['embed_code'] = '<br><hr><br>
				<div class="sidebar_widget embed_code">
							<h4>Share your rating!</h4>
							<p>Share your rating with your website visitors using the FindARealtor widget!</p>
							<div id="findarealtor_rating" height="35" style="display:none;">
								<a href="http://findarealtor.net/realtor.php?id=' . $_GET['id'] . '"><img src="http://findarealtor.net/img/logo.png" alt="Reza Karami" id="far_logo" /></a>
								<span id="pipe"></span>
								<span id="far_rating"></span>
								<img src="http://findarealtor.net/img/star.png" id="far_star" />
							</div>
							<script type="text/javascript" src="http://localhost/realtor/js/embed.js"></script>
							<script type="text/javascript" src="http://localhost/realtor/ajax.php?action=embed_rating&id=' . $_GET['id'] . '&callback=mycallback"></script>
							<p class="m-b-0 m-t-10"><a href="#" id="show_embed_code">Show HTML Code</a></p>
							<textarea class="form-control" style="display:none; margin-top:20px;">
<div id="findarealtor_rating" height="35" style="display:none;">
<a href="http://findarealtor.net/realtor.php?id=' . $_GET['id'] . '"><img src="http://findarealtor.net/img/logo.png" alt="Reza Karami" id="far_logo" /></a>
<span id="pipe"></span>
<span id="far_rating"></span>
<img src="http://findarealtor.net/img/star.png" id="far_star" />
</div>
<script type="text/javascript" src="http://localhost/realtor/js/embed.js"></script>
<script type="text/javascript" src="http://localhost/realtor/ajax.php?action=embed_rating&id=' . $_GET['id'] . '&callback=mycallback"></script>
							</textarea>';

			} else if(!isset($_SESSION['realtor_id'])) {

				$data['write_a_review'] = '<br><hr><br>
						<div class="sidebar_widget write_a_review">
							<h4>Have you worked with this Realtor?</h4>
							<p>Share your experience and help others decide if this Realtor is right for them!</p>
							<a href="write_a_review.php" class="btn btn-primary">Write a Review</a>
						</div>';
			}


			if($unclaimed && $_SESSION['logged_in'] != true) {

				$data['is_this_you'] = '<br><hr><br>

						<div class="sidebar_widget write_a_review">
							<h4>Is this you?</h4>
							<p>Take control of your Realtor profile. Respond to reviews and reach more potential clients!</p>
							<a href="claim_realtor_profile.php?id=' . htmlspecialchars($_GET['id']) . '" class="btn btn-primary">Claim Profile</a>
						</div>';
			}

			return $data;

		}


		public function calculateAverageRating($realtor_id) {

			$count = 0;
			$total = 0;
			$avg = 0;

			$query = $this->dbo->prepare("SELECT rating FROM reviews WHERE realtor_id = ?");
			$query->execute(array($realtor_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$total += $row[0];

				$count++;
			}

			$avg = $total / $count;

			return $avg;

		}


		public function loadRealtorReviews() {

			$realtor_id = $this->pseudocrypt->unhash($_REQUEST['id']);
			$offset = $_REQUEST['offset'];
			$sort_by = $_REQUEST['sort_by'];

			if($sort_by == 'highest_rated') {

				$order_by = 'rating DESC, datetime DESC';
			
			} else if($sort_by == 'lowest_rated') {

				$order_by = 'rating ASC, datetime DESC';
			
			} else {

				$order_by = 'datetime DESC';
			}

			if($offset != '') {

				$offset++;

			} else {

				$offset = 0;
			}

			$limit = 10;

			$data = '';

			$query = $this->dbo->prepare("SELECT id, user_id, review_text, datetime, rating, anonymous, response FROM reviews WHERE realtor_id = ? AND status = ? ORDER BY " . $order_by . " LIMIT $offset, $limit");
			$query->execute(array($realtor_id, 'accepted'));

			$result = $query->fetchAll();

			$count = $offset;

			foreach($result as $row) {

				$write_response = '';
				$response = '';

				$review_id = $row[0];
				$user_id = $row[1];
				$text = nl2br(htmlspecialchars($row[2]));
				$datetime = date('Y/m/d', strtotime($row[3]));
				$rating = $row[4];
				$anonymous = $row[5];
				$response_text = nl2br($row[6]);

				$num_lines = substr_count($text, "\n");

				if($response_text != '') {

					if($realtor_id == $_SESSION['realtor_id'] && ($_SESSION['account_type'] == 'pro' || $_SESSION['account_type'] == 'platinum')) {

						$response = '<div class="review_response_wrapper">
									<p class="response_text">' . $response_text . '</p>
									<a href="#" class="delete_response"><i class="fa fa-trash-o"></i> Delete</a>
									<a href="#" class="edit_response"><i class="fa fa-pencil"></i> Edit</a>
									<div class="clearfix"></div>
								</div>';

					} else {

						$response = '<div class="review_response_wrapper">
									<p class="response_text"><strong>Realtor\'s Response</strong><br>' . $response_text . '</p>
								</div>';

					}
				
				} else {

					if($realtor_id == $_SESSION['realtor_id'] && ($_SESSION['account_type'] == 'pro' || $_SESSION['account_type'] == 'platinum')) {

						$write_response = '<a href="#" class="write_response"><i class="fa fa-reply"></i> Write Response</a>';

					}

				}

				


				if($anonymous == 1) {

					$query2 = $this->dbo->prepare("SELECT date_joined FROM user_accounts WHERE id = ?");
					$query2->execute(array($user_id));

					$result2 = $query2->fetchAll();

					foreach($result2 as $row2) {

						$date_joined = date('Y/m/d', strtotime($row2[0]));
					}

					$display_name = 'Anonymous';

					$profile_picture = '<img src="img/anonymous.png" alt="Anonymous" />';

					$data .= '<div class="review_item" id="' . $count . '">
							<table>
								<tr>
									<td>
										<span class="rating_star visible-xs m-b-20">' . $rating . '</span>
										<hr class="visible-xs">
										' . $profile_picture . '<br>
										<span class="name">' . $display_name . '</span>
										<span class="date_joined">Joined ' . $date_joined . '</span>
									</td>
									<td>
										<span class="datetime">Posted on: ' . $datetime . '</span>
										<span class="review_text' . (($num_lines >= 5) ? ' see_more_text' : '') . '">' . $text . '</span>' . (($num_lines >= 5) ? '<span class="see_more_link_wrap">... <a href="#" class="see_more">See More</a></span>' : '') . '
										<input type="hidden" id="review_id" value="' . $review_id . '" />
										' . $write_response . '
										' . $response . '
									</td>
									<td class="hidden-xs">
										<span class="rating_star">' . $rating . '</span>
									</td>
								</tr>
							</table>
						</div>';

				} else {

					$query2 = $this->dbo->prepare("SELECT first_name, last_name, profile_picture, date_joined FROM user_accounts WHERE id = ?");
					$query2->execute(array($user_id));

					$result2 = $query2->fetchAll();

					foreach($result2 as $row2) {

						$first_name = $row2[0];
						$last_name = $row2[1];
						$profile_picture = $row2[2];
						$date_joined = date('Y/m/d', strtotime($row2[3]));

						$display_name = $first_name . ' ' . substr($last_name, 0, 1) . '.';

						if($profile_picture != '') {

							$profile_picture = '<img src="http://cdn.findarealtor.net/upp/' . $profile_picture . '" alt="' . $first_name . ' ' . substr($last_name, 0, 1) . '" />';
						
						} else {

							$profile_picture = '<img src="img/anonymous.png" />';
						}
					}

					$query2 = $this->dbo->prepare("SELECT COUNT(*) FROM reviews WHERE user_id = ? AND status = ?");
					$query2->execute(array($user_id, 'accepted'));

					$result2 = $query2->fetchAll();

					foreach($result2 as $row2) {

						$num_reviews = $row2[0];
					}

					

					$data .= '<div class="review_item" id="' . $count . '">
							<table>
								<tr>
									<td>
										<span class="rating_star visible-xs m-b-20">' . $rating . '</span>
										<hr class="visible-xs">
										' . $profile_picture . '<br>
										<span class="name">' . $display_name . '</span>
										<span class="num_reviews">' . $num_reviews . (($num_reviews == 1) ? ' Review' : ' Reviews') . '</span>
										<span class="date_joined">Joined ' . $date_joined . '</span>
									</td>
									<td>
										<span class="datetime">Posted on: ' . $datetime . '</span>
										<span class="review_text' . (($num_lines >= 5) ? ' see_more_text' : '') . '">' . $text . '</span>' . (($num_lines >= 5) ? '<span class="see_more_link_wrap">... <a href="#" class="see_more">See More</a></span>' : '') . '
										<input type="hidden" id="review_id" value="' . $review_id . '" />
										' . $write_response . '
										' . $response . '
									</td>
									<td class="hidden-xs">
										<span class="rating_star">' . $rating . '</span>
									</td>
								</tr>
							</table>
						</div>';
				}

				

				$count++;
			}

			if($data != '') {

				$data = '<div class="reviews_wrapper">' . $data;

				$data .= '</div>
                		<a href="#" id="load_more_reviews" class="btn btn-primary btn-block">Load More</a>';

			}

			return $data;
		}


		public function publishResponse() {

			$realtor_id = $_SESSION['realtor_id'];

			$review_id = $_POST['review_id'];
			$response_text = trim($_POST['response_text']);

			$query = $this->dbo->prepare("UPDATE reviews SET response = ? WHERE id = ? AND realtor_id = ?");
			$query->execute(array($response_text, $review_id, $realtor_id));

		}


		public function deleteResponse() {

			$realtor_id = $_SESSION['realtor_id'];

			$review_id = $_POST['review_id'];

			$query = $this->dbo->prepare("UPDATE reviews SET response = ? WHERE id = ? AND realtor_id = ?");
			$query->execute(array('', $review_id, $realtor_id));

		}



		public function loadRealtorServiceArea($realtor_id, $account_type) {

			if($account_type == 'standard' || $account_type == 'free') {

				$limit = 1;
			
			} else if($account_type == 'pro') {

				$limit = 3;
			
			} else if($account_type == 'platinum') {

				$limit = 6;
			
			} else if($account_type == 'RETURN ALL') {

				$limit = 6;

			} else {

				return false;
			} 

			$service_areas = array();

			$query = $this->dbo->prepare("SELECT city FROM realtor_service_area WHERE realtor_id = ? LIMIT $limit");
			$query->execute(array($realtor_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$city = $row[0];

				array_push($service_areas, $city);

			}
			
			return $service_areas;

		}


		public function loadRealtorAccountOverview() {

			$realtor_id = $_SESSION['realtor_id'];

			$data = array();
			$service_area_array = array();

			$query = $this->dbo->prepare("SELECT first_name, last_name, email, bio, website, phone, contact_email, account_type, profile_picture, country, expiry_date FROM realtor_accounts WHERE id = ?");
			$query->execute(array($realtor_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$data['first_name'] = $row[0];
				$data['last_name'] = $row[1];
				$data['email'] = $row[2];
				$data['bio'] = $row[3];
				$data['website'] = $row[4];
				$data['phone'] = $row[5];
				$data['contact_email'] = $row[6];
				$account_type = $row[7];
				$data['profile_picture'] = $row[8];
				$data['country'] = strtolower($row[9]);
				$expiry_date = $row[10];
			}

			if($data['profile_picture'] != '') {
				$data['profile_picture'] = '<img src="http://cdn.findarealtor.net/rpp/' . $data['profile_picture'] . '" alt="' . $data['first_name'] . ' ' . $data['last_name'] . '" /><a href="#" id="remove_picture" title="Remove Profile Picture"><i class="fa fa-close"></i></a>';
			} else {
				$data['profile_picture'] = '<img src="img/anonymous.png" alt="Anonymous" />';
			}

			$data['avg_rating'] = $this->calculateAverageRating($realtor_id);

			$now = time();
		    $expiry_date_time = strtotime($expiry_date);
		    $days_to_expiry = floor(($expiry_date_time - $now)/(60*60*24));

		    if($days_to_expiry >= 0) {

		    	$expires_in = '<p>Expires in <strong>' . $days_to_expiry . '</strong> days</p>';
		    	$call_to_action = '&nbsp;&nbsp;&nbsp;&nbsp;<a href="upgrade.php" class="btn btn-primary">Change Plan</a>';
		    
		    } else {

		    	$expires_in = '<p class="alert alert-danger">Expired on ' . date('Y/m/d', strtotime($expiry_date)) . '</p>';
		    	$call_to_action = '&nbsp;&nbsp;&nbsp;&nbsp;<a href="process_payment.php?ref=' . $account_type . '_plan" class="btn btn-primary">Renew</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="upgrade.php" class="btn btn-default">Change Plan</a>';
		    }


			if($account_type == 'free') {

				$data['account_type'] = '<h4>Free&nbsp;&nbsp;&nbsp;&nbsp;<a href="upgrade.php" class="btn btn-primary">Upgrade</a></h4>';
			
			} else if($account_type == 'standard') {

				$data['account_type'] = '<h4>Standard' . $call_to_action . '</h4>' . $expires_in;
			
			} else if($account_type == 'pro') {

				$data['account_type'] = '<h4>Pro' . $call_to_action . '</h4>' . $expires_in;
			
			} else if($account_type == 'platium') {

				$data['account_type'] = '<h4>Platinum' . $call_to_action . '</h4>' . $expires_in;
			}


			$service_area = $this->loadRealtorServiceArea($realtor_id, 'RETURN ALL');

			if($account_type == 'free' || $account_type == 'standard') {

				$num_service_area = 1;
			
			} else if($account_type == 'pro') {

				$num_service_area = 3;
			
			} else if($account_type == 'platinum') {

				$num_service_area = 6;
			} 

			$count = 0;

			foreach($service_area as $city) {

				$service_area_string .= '<div class="col-xs-12 col-md-6"><input type="text" id="service_area_' . ($count + 1) . '" class="form-control service_area_city" name="service_area[]" placeholder="Service Area" value="' . $city . '" ' . (($count < $num_service_area) ? '' : 'disabled="disabled"') . '/></div>';

				array_push($service_area_array, $city);

				$count++;
			}

			while(count($service_area_array) < 6) {

				$service_area_string .= '<div class="col-xs-12 col-md-6"><input type="text" id="service_area_' . ($count + 1) . '" class="form-control service_area_city" name="service_area[]" placeholder="Service Area" value="" ' . (($count < $num_service_area) ? '' : 'disabled="disabled"') . ' /></div>';

				array_push($service_area_array, '');

				$count++;
			}

			$data['service_area'] = $service_area_string;
			$data['service_area_json'] = json_encode($service_area_array);


			return $data;
		}


		public function loadRealtorAccountActivity() {

			$realtor_id = $_SESSION['realtor_id'];

			$data = array();


			//profile view count - today
			$query = $this->dbo->prepare("SELECT COUNT(*) FROM profile_views WHERE realtor_id = ? AND DATE(datetime) = DATE(NOW())");
			$query->execute(array($realtor_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$data['profile_views_today'] = $row[0];
			}

			//profile view count - last 7 days
			$query = $this->dbo->prepare("SELECT COUNT(*) FROM profile_views WHERE realtor_id = ? AND DATE(datetime) > (DATE(NOW()) - INTERVAL 7 DAY)");
			$query->execute(array($realtor_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$data['profile_views_7_days'] = $row[0];
			}

			//profile view count - all time
			$query = $this->dbo->prepare("SELECT COUNT(*) FROM profile_views WHERE realtor_id = ?");
			$query->execute(array($realtor_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$data['profile_views_all_time'] = $row[0];
			}

			//reviews count - last 7 days
			$query = $this->dbo->prepare("SELECT COUNT(*) FROM reviews WHERE realtor_id = ? AND DATE(datetime) > (DATE(NOW()) - INTERVAL 7 DAY)");
			$query->execute(array($realtor_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$data['reviews_7_days'] = $row[0];
			}

			//reviews count - last 30 days
			$query = $this->dbo->prepare("SELECT COUNT(*) FROM reviews WHERE realtor_id = ? AND DATE(datetime) > (DATE(NOW()) - INTERVAL 30 DAY)");
			$query->execute(array($realtor_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$data['reviews_30_days'] = $row[0];
			}

			//reviews count - all time
			$query = $this->dbo->prepare("SELECT COUNT(*) FROM reviews WHERE realtor_id = ?");
			$query->execute(array($realtor_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$data['reviews_all_time'] = $row[0];
			}

			return $data;
		}


		// public function loadRealtorLatestReviews() { //only limited to latest 5 reviews... viewed by realtor him/herself!

		// 	$realtor_id = $_SESSION['realtor_id'];

		// 	$data = '';

		// 	$query = $this->dbo->prepare("SELECT user_id, review_text, datetime, rating, anonymous FROM reviews WHERE realtor_id = ? LIMIT 5");
		// 	$query->execute(array($realtor_id));

		// 	$result = $query->fetchAll();

		// 	foreach($result as $row) {

		// 		$user_id = $row[0];
		// 		$text = htmlspecialchars($row[1]);
		// 		$datetime = date('Y/m/d', strtotime($row[2]));
		// 		$rating = $row[3];
		// 		$anonymous = $row[4];

		// 		if($anonymous == 1) {

		// 			$display_name = 'Anonymous';

		// 		} else {

		// 			$query2 = $this->dbo->prepare("SELECT first_name, last_name, profile_picture FROM user_accounts WHERE id = ?");
		// 			$query2->execute(array($user_id));

		// 			$result2 = $query2->fetchAll();

		// 			foreach($result2 as $row2) {

		// 				$first_name = $row2[0];
		// 				$last_name = $row2[1];
		// 				$profile_picture = $row2[2];

		// 				$display_name = '<a href="client.php?id=' . $user_id . '">' . $first_name . ' ' . $last_name . '</a>';

		// 				if($profile_picture != '') {

		// 					$profile_picture = 'insert pic';
						
		// 				} else {

		// 					$profile_picture = 'N/A';
		// 				}
		// 			}
		// 		}

		// 		$data .= '<div class="review_item">
		// 					By: ' . $display_name . '<br>
		// 					Profile Picutre: ' . $profile_picture . '<br>
		// 					Review: ' . $text . '<br>
		// 					Rating: ' . $rating . '<br>
		// 				</div>';
		// 	}

		// 	return $data;
		// }



		public function updateRealtorProfile() {

			$get_args = '';

			$realtor_id = $_SESSION['realtor_id'];
			$country = $_SESSION['realtor_country'];

			$first_name = htmlspecialchars($_POST['first_name']);
			$last_name = htmlspecialchars($_POST['last_name']);
			$email = htmlspecialchars($_POST['email']);
			$bio = trim(htmlspecialchars($_POST['bio']));
			$phone = htmlspecialchars($_POST['phone']);
			$contact_email = htmlspecialchars($_POST['contact_email']);
			$website = htmlspecialchars($_POST['website']);

			if($first_name == '' || $last_name == '' || $email == '') {

				$get_args .= 'e=missing_fields&';
			
			} else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {

				$get_args .= 'e=invalid_email&';
			
			}

			$query = $this->dbo->prepare("UPDATE realtor_accounts SET first_name = ?, last_name = ?, email = ?, bio = ?, phone = ?, contact_email = ?, website = ? WHERE id = ?");
			$query->execute(array($first_name, $last_name, $email, $bio, $phone, $contact_email, $website, $realtor_id));

			//PASSWORD CHANGE
			$new_password = htmlspecialchars($_POST['new_password']);
			$confirm_password = htmlspecialchars($_POST['confirm_password']);

			if($new_password != '') {

				if($new_password == $confirm_password) {

					//if (preg_match('/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/', $new_password)) {
					if(strlen($new_password) >= 8) {

						$new_password = hash('SHA1', $new_password);

						$query = $this->dbo->prepare("UPDATE realtor_accounts SET password = ? WHERE email = ?");
						$query->execute(array($new_password, $email));

				    	$query = $this->dbo->prepare("DELETE FROM password_reset_token WHERE email = ?");
						$query->execute(array($email));


				    	$get_args .= 'ref_pass=password_change_successful&';

					} else {

						$get_args .= 'e=password_too_short&';
					}

				} else {

					$get_args .= 'e=password_mismatch&';
				}
			}


			//SERVICE AREAS
			$service_areas = $_POST['service_area'];

			$query = $this->dbo->prepare("DELETE FROM realtor_service_area WHERE realtor_id = ?");
			$query->execute(array($realtor_id));

			foreach ($service_areas as $service_area) {				
				
				if($service_area != '') {

					$query = $this->dbo->prepare("INSERT INTO realtor_service_area SET realtor_id = ?, city = ?");
					$query->execute(array($realtor_id, $service_area));
				}
			}


			$get_args .= 'ref_profile=profile_updated';

			header('Location: realtor_account_settings.php?' . $get_args);

		}



		public function requestReview() {

			$realtor_id = $_SESSION['realtor_id'];
			$realtor_id_hashed = $_SESSION['realtor_id_hashed'];
			$first_name = $_SESSION['first_name'];
			$last_name = $_SESSION['last_name'];

			if($_POST['contact_list_csv'] != '') {
				//Bulk clients list

				require_once('Email.class.php');
				$mail = new Email();

				$message = htmlspecialchars($_POST['message']);
				$clients_array = array();
				
				$clients = preg_split('/[\r\n]+/', $_POST['contact_list_csv'], -1, PREG_SPLIT_NO_EMPTY);

				foreach ($clients as $client) {
					
					$client_array = explode(',', $client);

					array_push($clients_array, $client_array);
				}


				foreach ($clients_array as $client) { 

					$client_first_name = ucwords(trim(htmlspecialchars($client[0])));
					$client_last_name = ucwords(trim(htmlspecialchars($client[1])));
					$client_email = trim(htmlspecialchars($client[2]));
					

					if($client_first_name == '' || $client_email == '') {

						header('Location: request_a_review.php?e=csv_error_missing_fields');
						exit;
					
					} else if($client_email == '' || filter_var($client_email, FILTER_VALIDATE_EMAIL) === false) {

						header('Location: request_a_review.php?e=csv_error_invalid_email');
						exit;
					}

					$this_message = str_replace('{client_first_name}', $client_first_name, $message);
					$this_message = str_replace('{client_last_name}', $client_last_name, $this_message);

					$datetime = date('Y-m-d H:i:s');

					$token = hash('SHA256', $client_email . time());

					$message = str_replace('http://FindARealtor.net/write_review.php?id=' . $realtor_id_hashed, 'http://FindARealtor.net/write_review.php?token=' . $token, $message);

					$query = $this->dbo->prepare("INSERT INTO review_requests SET realtor_id = ?, client_email = ?, client_first_name = ?, client_last_name = ?, message = ?, datetime = ?, token = ?");
					$query->execute(array($realtor_id, $client_email, $client_first_name, $client_last_name, $this_message, $datetime, $token));

					$mail->reviewRequest($first_name, $last_name, $client_first_name, $client_last_name, $client_email, $this_message, $token);

				}

				header('Location: request_a_review.php?ref=requests_sent');
				exit;


			} else {
				//Single client

				$client_first_name = ucwords(trim(htmlspecialchars($_POST['client_first_name'])));
				$client_last_name = ucwords(trim(htmlspecialchars($_POST['client_last_name'])));
				$client_email = trim(htmlspecialchars($_POST['client_email']));
				$message = htmlspecialchars($_POST['message']);

				if($client_first_name == '' || $client_email == '') {

					header('Location: request_a_review.php?e=missing_fields');
					exit;
				
				} else if($client_email == '' || filter_var($client_email, FILTER_VALIDATE_EMAIL) === false) {

					header('Location: request_a_review.php?e=invalid_email');
					exit;
				}

				$message = str_replace('{client_first_name}', $client_first_name, $message);
				$message = str_replace('{client_last_name}', $client_last_name, $message);

				$datetime = date('Y-m-d H:i:s');

				$token = hash('SHA256', $client_email . time());

				$message = str_replace('http://FindARealtor.net/write_review.php?id=' . $realtor_id_hashed, 'http://FindARealtor.net/write_review.php?token=' . $token, $message);

				$query = $this->dbo->prepare("INSERT INTO review_requests SET realtor_id = ?, client_email = ?, client_first_name = ?, client_last_name = ?, message = ?, datetime = ?, token = ?");
				$query->execute(array($realtor_id, $client_email, $client_first_name, $client_last_name, $message, $datetime, $token));

				require_once('Email.class.php');
				$mail = new Email();

				$mail->reviewRequest($first_name, $last_name, $client_first_name, $client_last_name, $client_email, $message, $token);

				header('Location: request_a_review.php?ref=request_sent');
				exit;

			}
		}


		public function resendReviewRequest() {

			$realtor_id = $_SESSION['realtor_id'];
			$first_name = $_SESSION['first_name'];
			$last_name = $_SESSION['last_name'];

			$request_id = htmlspecialchars($_POST['request_id']);

			$query = $this->dbo->prepare("SELECT client_first_name, client_last_name, client_email, message, token FROM review_requests WHERE id = ?");
			$query->execute(array($request_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$client_first_name = $row[0];
				$client_last_name = $row[1];
				$client_email = $row[2];
				$message = $row[3];
				$token = $row[4];
			}

			require_once('Email.class.php');
			$mail = new Email();

			$mail->reviewRequest($first_name, $last_name, $client_first_name, $client_last_name, $client_email, $message, $token);

			$datetime = date('Y-m-d H:i:s');
			$datetime_string = date('M j, Y g:iA');

			$query = $this->dbo->prepare("UPDATE review_requests SET datetime = ? WHERE id = ?");
			$query->execute(array($datetime, $request_id));

			return $datetime_string;

		}


		public function deleteReviewRequest() {

			$request_id = htmlspecialchars($_POST['request_id']);

			$query = $this->dbo->prepare("DELETE FROM review_requests WHERE id = ?");
			$query->execute(array($request_id));

			return $this->reviewRequestHistory(); //reload table and return
		}


		public function reviewRequestHistory() {

			$realtor_id = $_SESSION['realtor_id'];

			$data = '';

			$query = $this->dbo->prepare("SELECT id, client_email, client_first_name, client_last_name, message, datetime FROM review_requests WHERE realtor_id = ?");
			$query->execute(array($realtor_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$id = $row[0];
				$client_email = $row[1];
				$client_first_name = $row[2];
				$client_last_name = $row[3];
				$message = $row[4];
				$date_sent = date('M j, Y g:iA', strtotime($row[5]));

				$data .= '<tr id="' . $id . '">
						<td>' . $client_first_name . ' ' . $client_last_name . '</td>
						<td>' . $client_email . '</td>
						<td>' . $date_sent . '</td>
						<td>
							<div class="btn-group">
								<a href="#" class="resend_review_request btn btn-default btn-sm">Resend</a>
								<a href="#" class="delete_review_request btn btn-default btn-sm">Delete</a>
							</div>
						</td>
						</tr>';
			}

			if($data == '') {
				$data = '<tr><td colspan="4" style="text-align: center;">No previous requests to show.</td></tr>';
			}

			return $data;

		}


		public function addARealtor() {

			$first_name = htmlspecialchars($_POST['first_name']);
			$last_name = htmlspecialchars($_POST['last_name']);
			$phone = htmlspecialchars($_POST['phone']);
			$contact_email = htmlspecialchars($_POST['email']);

			$country = htmlspecialchars($_POST['country']);
			$city = htmlspecialchars($_POST['service_area']);

			if($phone == '' || $first_name == '' || $last_name == '' || $country == '' || $city == '') {

				header('Location: add_a_realtor.php?e=missing_fields');
				exit;
			
			} else if($email != '' && !filter_var($email, FILTER_VALIDATE_EMAIL)) {

				header('Location: add_a_realtor.php?e=invalid_email');
				exit;

			} else if(!is_numeric($phone)) {

				header('Location: add_a_realtor.php?e=invalid_phone');
				exit;
			
			}

			$datetime = date('Y-m-d H:i:s');


			$query = $this->dbo->prepare("INSERT INTO realtor_accounts SET first_name = ?, last_name = ?, phone = ?, contact_email = ?, date_joined = ?, account_type = ?, unclaimed = ?");
			$query->execute(array($first_name, $last_name, $phone, $contact_email, $datetime, 'free', 1)); //unclaimed is set to TRUE

			$realtor_id = $this->dbo->lastInsertId();

			$query = $this->dbo->prepare("INSERT INTO realtor_service_area SET realtor_id = ?, city = ?");
			$query->execute(array($realtor_id, $city));

			if($contact_email != '') {

				require_once('Email.class.php');
				$mail = new Email();

				$mail->realtorAdded($first_name, $last_name, $contact_email);

			}

			$realtor_id = $this->pseudocrypt->hash($realtor_id, 6);

			header('Location: realtor.php?id=' . $realtor_id);
			exit;

		}


		public function claimRealtorProfile() {

			$realtor_id = $this->pseudocrypt->unhash(htmlspecialchars($_POST['rid']));

			$first_name = htmlspecialchars($_POST['first_name']);
			$last_name = htmlspecialchars($_POST['last_name']);
			$email = htmlspecialchars($_POST['email']);
			$password = htmlspecialchars($_POST['password']);

			if($email == '' || $password == '' || $first_name == '' || $last_name == '') {

				header('Location: claim_realtor_profile.php?e=missing_fields&id=' . htmlspecialchars($_POST['rid']));
				exit;
			
			} else if($email != '' && !filter_var($email, FILTER_VALIDATE_EMAIL)) {

				header('Location: claim_realtor_profile.php?e=invalid_email&id=' . htmlspecialchars($_POST['rid']));
				exit;

			} else if(strlen($password) < 8) {

				header('Location: claim_realtor_profile.php?e=password_too_short&id=' . htmlspecialchars($_POST['rid']));
				exit;
			
			}

			$query = $this->dbo->prepare("SELECT COUNT(*) FROM realtor_accounts WHERE email = ?");
			$query->execute(array($email));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$num_rows = $row[0];
			}

			if($num_rows == 0) {

				$password_hashed = hash('SHA1', $password);

				$datetime = date('Y-m-d H:i:s');

				$query = $this->dbo->prepare("INSERT INTO claim_requests SET realtor_id = ?, claimee_first_name = ?, claimee_last_name = ?, claimee_email = ?, claimee_password = ?, datetime = ?, status = ?");
				$query->execute(array($realtor_id, $first_name, $last_name, $email, $password_hashed, $datetime, 'pending'));

				header('Location: claim_realtor_profile.php?ref=claim_request_received&id=' . htmlspecialchars($_POST['rid']));
				exit;

			} else {

				//Email in use..
				header('Location: claim_realtor_profile.php?e=email_in_use&id=' . htmlspecialchars($_POST['rid']));
				exit;
			}
		}



		public function getName() {

			$realtor_id = $this->pseudocrypt->unhash(htmlspecialchars($_GET['id']));

			$query = $this->dbo->prepare("SELECT first_name, last_name FROM realtor_accounts WHERE id = ?");
			$query->execute(array($realtor_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				return $row[0] . ' ' . $row[1];
			}
		}



		public function completeProfilePictureUpload($filename, $realtor_id) {

			$realtor_id = $this->pseudocrypt->unhash($realtor_id);

			$query = $this->dbo->prepare("UPDATE realtor_accounts SET profile_picture = ? WHERE id = ?");
			$query->execute(array($filename, $realtor_id));

			$_SESSION['profile_picture'] = $filename;
			
		}


		public function getProfilePicture() {

			$realtor_id = $this->pseudocrypt->unhash($_POST['realtor_id']);

			$query = $this->dbo->prepare("SELECT profile_picture FROM realtor_accounts WHERE id = ?");
			$query->execute(array($realtor_id));

			$result = $query->fetchAll();
			
			foreach($result as $row) {

				return $row[0];
			}
		}


		public function removeProfilePicture() {

			$realtor_id = $_SESSION['realtor_id'];

			$query = $this->dbo->prepare("SELECT profile_picture FROM realtor_accounts WHERE id = ?");
			$query->execute(array($realtor_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$profile_picture = $row[0];
			}

			$query = $this->dbo->prepare("UPDATE realtor_accounts SET profile_picture = ? WHERE id = ?");
			$query->execute(array('', $realtor_id));

			$_SESSION['profile_picture'] = '';

			require_once('classes/aws.phar');
	            
	        $s3client = Aws\S3\S3Client::factory(array(
	            'key'    => 'AKIAJKBMGCMLYBHN77PQ',
	            'secret' => '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' //reza_karami@outlook.com account
	        ));


	        $key = 'rpp/' . $profile_picture; //upp = user profile pictures

	        $result = $s3client->deleteObject(array(
			    'Bucket' => 'far-resources',
			    'Key'    => $key
			)); 
		}


		public function sendMessage() {

			$realtor_id = $this->pseudocrypt->unhash($_POST['realtor_id']);
			$name = $_POST['name'];
			$email = $_POST['email'];
			$message = $_POST['message'];

			require_once('mailwizz-setup.php');

			$endpoint = new MailWizzApi_Endpoint_TransactionalEmails();

			$response = $endpoint->create(array(
			    'to_name'           => 'FindARealtor Support', // required
			    'to_email'          => 'support@findarealtor.net', // required
			    'from_name'         => $name, // required
			    'from_email'        => $email, // optional
			    'reply_to_name'     => $name, // optional
    			'reply_to_email'    => $email, // optional
			    'subject'           => 'FindARealtor – Realtor Support', // required
			    'body'              => 'From:<br>' . $name . '<br>' . $email . '<br>' . $realtor_id . '<br><br>Message:<br>' . $message, // required
			    'send_at'           => date('Y-m-d H:i:s'),  // required, UTC date time in same format!
			));

			echo '<hr /><pre>';
			print_r($response->body);
			echo '</pre>';

			// $response = $endpoint->create(array(
			//     'to_name'           => $name, // required
			//     'to_email'          => $email, // required
			//     'from_name'         => 'FindARealtor', // required
			//     'from_email'        => 'no-reply@findarealtor.net', // optional
			//     'subject'           => 'FindARealtor – Realtor Support', // required
			//     'body'              => 'From:<br>' . $name . '<br>' . $email . '<br>' . $realtor_id . '<br><br>Message:<br>' . $message, // required
			//     'send_at'           => date('Y-m-d H:i:s'),  // required, UTC date time in same format!
			// ));

			// require_once('class.phpmailer.php');
			
			// $mail = new PHPMailer;
			// $mail->CharSet = 'UTF-8';
			// $mail->isHTML(true);
			// $mail->From = $email;
			// $mail->FromName = $name;

			// $mail->addAddress('support@findarealtor.net');
			// $mail->Subject = 'FindARealtor – Realtor Support';

			// $mail->Body = 'From:<br>' . $name . '<br>' . $email . '<br>' . $realtor_id . '<br><br>Message:<br>' . $message;

			// if(!$mail->send()) {

			//    return false;
			// }

			return true;
		}

	}
?>