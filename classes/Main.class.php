<?php

	session_start();
	
	Class Main { 

		public function __construct () {

			require("inc/connect_dbo.php");
			$this->dbo = $dbo;

			require_once('PseudoCrypt.class.php');
			$this->pseudocrypt = new PseudoCrypt();
		}


		public function logProfileView() {

			$realtor_id = $this->pseudocrypt->unhash(htmlspecialchars($_GET['id']));

			if($_SESSION['realtor_id'] != $realtor_id) { //do not log own views

				$visitor_ip = $_SERVER['REMOTE_ADDR'];

				$datetime = date('Y-m-d H:i:s');

				$query = $this->dbo->prepare("INSERT INTO profile_views SET realtor_id = ?, ip_address = ?, datetime = ?");
				$query->execute(array($realtor_id, $visitor_ip, $datetime));
			}
		}


		public function locationAutocomplete() {

			$keyword = htmlspecialchars($_POST['keyword']) . '%';
			$country = $_SESSION['country'];

			$data = '';

			if($country == 'US') {
				$query = $this->dbo->prepare("SELECT id, city, state FROM us_cities WHERE city LIKE ? ORDER BY state");
			} else if($country == 'CA') {
				$query = $this->dbo->prepare("SELECT id, city, province FROM ca_cities WHERE city LIKE ? ORDER BY province");
			}

			$query->execute(array($keyword));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$data .= '<li id="' . $row[0] . '">' . $row[1] . ', ' . $row[2] . '</li>';
			}

			return $data;
		}


		public function loadServiceAreaList() {

			if(htmlspecialchars(isset($_POST['country']))) {
				
				$country = htmlspecialchars($_POST['country']);

			} else {

				$country = $_SESSION['country'];
			}

			$cities = array();

			if($country == 'US') {
				$query = $this->dbo->prepare("SELECT id, city, state FROM us_cities ORDER BY state");
			} else if($country == 'CA') {
				$query = $this->dbo->prepare("SELECT id, city, province FROM ca_cities ORDER BY province");
			}

			$query->execute(array());

			$result = $query->fetchAll();

			foreach($result as $row) {

				$city = array();
				
				$city['label'] = $row[1] . ', ' . $row[2];
				$city['value'] = $row[0];

				array_push($cities, $city);
			}

			return json_encode($cities);

		}


		public function topRatedRealtors() {

			$realtor_count = 0;
			$local_realtor_ids = array();

			if($_SESSION['search_location'] != '') {

				//return local only
				$query = $this->dbo->prepare("SELECT ra.id, ra.first_name, ra.last_name, ra.profile_picture, AVG(r.rating) avg_rating, COUNT(r.realtor_id) AS num_reviews
								FROM realtor_accounts ra
								JOIN realtor_service_area sa
								ON ra.id = sa.realtor_id AND sa.city = ?
								LEFT JOIN reviews r 
								ON ra.id = r.realtor_id AND ra.verified = ?
								GROUP BY ra.id
								HAVING num_reviews > 0
								ORDER BY avg_rating DESC, num_reviews DESC
								LIMIT 10");
				$query->execute(array($_SESSION['search_location'], 1));


				$result = $query->fetchAll();

				foreach($result as $row) {

					$realtor_id = $this->pseudocrypt->hash($row[0], 6);
					$first_name = $row[1];
					$last_name = $row[2];
					$profile_picture = $row[3];
					$avg_rating = round($row[4], 1);
					$num_reviews = $row[5];

					array_push($local_realtor_ids, $realtor_id);

					if($profile_picture != '') {
						$profile_picture = '<img src="http://cdn.findarealtor.net/rpp/' . $profile_picture . '" alt="' . $first_name . ' ' . $last_name . '" />';
					} else {
						$profile_picture = '<img src="img/anonymous.png" alt="Anonymous" />';
					}

					$data .= '<div class="col-xs-12 col-md-6">
			    				<div class="realtor_item">
									<table>
										<tr>
											<td>
												<a href="realtor.php?id=' . $realtor_id . '">' . $profile_picture . '</a>
											</td>
											<td>
												<h4 class="realtor_name"><a href="realtor.php?id=' . $realtor_id . '">' . $first_name . ' ' . $last_name . '</a></h4>
												<span class="agency_name">Sutton Group Status Realty Inc. Brokerage</span>
												<span class="service_area">Toronto, ON</span>
												<div class="clearfix"></div>
											</td>
											<td>
												<span class="avg_rating_star">' . $avg_rating . '</span>
												<span class="num_reviews">' . $num_reviews . (($num_reviews == 1) ? ' Review' : ' Reviews') . '</span>
											</td>
										</tr>
									</table>
								</div>
			    			</div>';

			    	$realtor_count++;
				}


				if($data != '') {

					$data = '<div class="col-xs-12"><h2>Top Realtors in ' . $_SESSION['search_location'] . '</h2></div>' . $data;
				}

				//if not enough realtors in visitor's area... load rest with nation wide relators.
				if($realtor_count < 10) {

					$needed = 10 - $realtor_count;

					//$country = $_SESSION['country'];
					
					//return from accross country
					$query = $this->dbo->prepare("SELECT ra.id, ra.first_name, ra.last_name, ra.profile_picture, AVG(r.rating) avg_rating, COUNT(r.realtor_id) AS num_reviews
								FROM realtor_accounts ra
								LEFT JOIN reviews r 
								ON ra.id = r.realtor_id AND ra.country = ? AND ra.verified = ? AND ra.profile_picture <> ?
								GROUP BY ra.id
								HAVING num_reviews > 0
								ORDER BY avg_rating DESC, num_reviews DESC
								LIMIT " . $needed);

					$query->execute(array('US', 1, ''));

					$result = $query->fetchAll();

					foreach($result as $row) {
						
						$realtor_id = $this->pseudocrypt->hash($row[0], 6);
						$first_name = $row[1];
						$last_name = $row[2];
						$profile_picture = $row[3];
						$avg_rating = round($row[4], 1);
						$num_reviews = $row[5];

						if(!in_array($realtor_id, $local_realtor_ids)) { //if realtor already listed in top local realtors, don't display again

							if($profile_picture != '') {
								$profile_picture = '<img src="http://cdn.findarealtor.net/rpp/' . $profile_picture . '" alt="' . $first_name . ' ' . $last_name . '" />';
							} else {
								$profile_picture = '<img src="img/anonymous.png" alt="Anonymous" />';
							}

							$data2 .= '<div class="col-xs-12 col-md-6">
					    				<div class="realtor_item">
											<table>
												<tr>
													<td>
														<a href="realtor.php?id=' . $realtor_id . '">' . $profile_picture . '</a>
													</td>
													<td>
														<h4 class="realtor_name"><a href="realtor.php?id=' . $realtor_id . '">' . $first_name . ' ' . $last_name . '</a></h4>
														<span class="agency_name">Sutton Group Status Realty Inc. Brokerage</span>
														<span class="service_area">Toronto, ON</span>
														<div class="clearfix"></div>
													</td>
													<td>
														<span class="avg_rating_star">' . $avg_rating . '</span>
														<span class="num_reviews">' . $num_reviews . (($num_reviews == 1) ? ' Review' : ' Reviews') . '</span>
													</td>
												</tr>
											</table>
										</div>
					    			</div>';

					    }
					}

					if($data2 != '') {

						if($_SESSION['country'] == 'CA') {
							$country = 'Canada';
						} else if($_SESSION['country'] == 'US') {
							$country = 'US';
						}

						$data2 = '</div><div class="row"><div class="col-xs-12"><h2>Top Realtors across ' . $country . '</h2></div>' . $data2;
					}
				}

				$data .= $data2;

			} else {

				//if visitor city not determined... return nation wide results
				// $country = $_SESSION['country'];

				// if($country == 'CA') {
				// 	$country = 'Canada';
				// } else if($country == 'US') {
				// 	$country = 'USA';
				// }

				//return from accross country
				$query = $this->dbo->prepare("SELECT ra.id, ra.first_name, ra.last_name, ra.profile_picture, AVG(r.rating) avg_rating, COUNT(r.realtor_id) AS num_reviews
								FROM realtor_accounts ra
								LEFT JOIN reviews r 
								ON ra.id = r.realtor_id AND ra.country = ? AND ra.verified = ? AND ra.profile_picture <> ?
								GROUP BY ra.id
								HAVING num_reviews > 0
								ORDER BY avg_rating DESC, num_reviews DESC
								LIMIT 10");
				$query->execute(array('US', 1, ''));


				$result = $query->fetchAll();

				foreach($result as $row) {

					$realtor_id = $this->pseudocrypt->hash($row[0], 6);
					$first_name = $row[1];
					$last_name = $row[2];
					$profile_picture = $row[3];
					$avg_rating = round($row[4], 1);
					$num_reviews = $row[5];

					if($profile_picture != '') {
						$profile_picture = '<img src="http://cdn.findarealtor.net/rpp/' . $profile_picture . '" alt="' . $first_name . ' ' . $last_name . '" />';
					} else {
						$profile_picture = '<img src="img/anonymous.png" alt="Anonymous" />';
					}

					$data .= '<div class="col-xs-12 col-md-6">
			    				<div class="realtor_item">
									<table>
										<tr>
											<td>
												<a href="realtor.php?id=' . $realtor_id . '">' . $profile_picture . '</a>
											</td>
											<td>
												<h4 class="realtor_name"><a href="realtor.php?id=' . $realtor_id . '">' . $first_name . ' ' . $last_name . '</a></h4>
												<span class="agency_name">Sutton Group Status Realty Inc. Brokerage</span>
												<span class="service_area">Toronto, ON</span>
												<div class="clearfix"></div>
											</td>
											<td>
												<span class="avg_rating_star">' . $avg_rating . '</span>
												<span class="num_reviews">' . $num_reviews . (($num_reviews == 1) ? ' Review' : ' Reviews') . '</span>
											</td>
										</tr>
									</table>
								</div>
			    			</div>';
				}
			}

			return $data;
		}


		public function search() {

			$realtor_name = $_GET['name'];
			$city = $_GET['city'];
			$latlng = $_GET['latlng'];
			//$city_id = $_SESSION['search_location_id'];

			//$country = $_SESSION['country'];

			$offset = $_REQUEST['offset'];

			$realtor_ids = array();
			$query_array = array($city, 1);


			$query_string = 'SELECT ra.id, ra.first_name, ra.last_name, ra.account_type, ra.phone, ra.contact_email, ra.website, ra.profile_picture, AVG(r.rating) avg_rating, COUNT(r.realtor_id) AS num_reviews
							FROM realtor_accounts ra
							JOIN realtor_service_area sa 
							ON ra.id = sa.realtor_id AND sa.city = ? AND ra.verified = ?';
							
			if(trim($realtor_name) != '') {

				$query_string .= "AND (lower(concat_ws(' ', ra.first_name, ra.last_name)) LIKE ? 
						OR lower(concat_ws(' ', ra.last_name, ra.first_name)) LIKE ?) ";
				
				array_push($query_array, '%' . strtolower($realtor_name) . '%');
				array_push($query_array, '%' . strtolower($realtor_name) . '%');
			}


			// if($city_id != '') {

			// 	$query_string .= "AND sa.city_id = " . $city_id . " AND sa.country = '" . $country . "'
			// 					";
			
			// } else {

			// 	$query_string .= "AND sa.country = '" . $country . "'
			// 					";
			// }


			if(htmlspecialchars($_GET['picture']) == 'true' || htmlspecialchars($_GET['picture']) == 'on') {

				$query_string .= "AND ra.profile_picture <> ''
				";
			}


			$query_string .= 'LEFT JOIN reviews r 
							ON ra.id = r.realtor_id
							GROUP BY ra.id
							';

			if(htmlspecialchars($_GET['sort']) == 'lowest_rated') {

				$query_string .= 'ORDER BY avg_rating ASC';
			
			} else {

				$query_string .= 'ORDER BY avg_rating DESC';
			}

			if($offset != '') {

				$offset++;

			} else {

				$offset = 0;
			}

			$limit = 10;

			$query_string .= " LIMIT $offset, $limit";

			$count = $offset;
			$count_2 = 0;

			//search for all realtors within given city...
			$query = $this->dbo->prepare($query_string);
			$query->execute($query_array);

			$result = $query->fetchAll();

			foreach($result as $row) {

				$realtor_id = $row[0];
				$first_name = $row[1];
				$last_name = $row[2];
				$account_type = $row[3];
				$phone = $row[4];
				$email = $row[5];
				$website = $row[6];
				$profile_picture = $row[7];
				$avg_rating = round($row[8], 1);
				$num_reviews = $row[9];

				$realtor_id = $this->pseudocrypt->hash($realtor_id, 6);

				if($phone != '') {

					$phone = '<span class="info_item phone" data-val="' . $phone . '"><i class="fa fa-phone"></i> Phone</span>';
				}

				if($email != '') {

					$email = '<span class="info_item email" data-val="' . $email . '"><i class="fa fa-envelope"></i> Email</span>';
				}

				if($website != '') {

					$website = '<span class="info_item website"><a href="' . $website . '" target="_blank"><i class="fa fa-link"></i> Website</a></span>';
				}

				$data .= '<div class="realtor_item" id="' . $count . '">
							<table>
								<tr>
									<td>
										<a href="realtor.php?id=' . $realtor_id . '"><img src="https://scontent-lga3-1.xx.fbcdn.net/hphotos-xtp1/v/t1.0-9/11889462_479883402192845_2434390578328709503_n.jpg?oh=5a69c4de17346cd77f41037fd3da6635&oe=56977697" alt="' . $first_name . ' ' . $last_name . '" /></a>
									</td>
									<td>
										<h4 class="realtor_name"><a href="realtor.php?id=' . $realtor_id . '">' . $first_name . ' ' . $last_name . '</a></h4>
										<span class="agency_name">Sutton Group Status Realty Inc. Brokerage</span>
										<div class="clearfix"></div>
										' . $phone . '
										' . $email . '
										' . $website . '
									</td>
									<td>
										<span class="avg_rating_star">' . $avg_rating . '</span>
										<span class="num_reviews">' . $num_reviews . (($num_reviews == 1) ? ' Review' : ' Reviews') . '</span>
									</td>
								</tr>
							</table>
						</div>';

				$count++;
				$count_2++;

			}


			if($data != '') {

				if($_GET['action'] == 'load_more_realtors') { //ajax call, only return results...

				} else {

					$data = '<h4>We found these Realtors matching your search criteria</h4><div class="search_results_wrapper">' . $data;

					$data .= '</div>';

					if($count_2 < $limit) { //no more profiles to load...

						$data .= '<h2 class="note">No more Realtors to show.</h2>';

					} else {

						$data .= '<a href="#" id="load_more_realtors" class="btn btn-primary btn-block">Load More</a>';
					}
				}

			} else {

				if($_GET['action'] == 'load_more_realtors') { //ajax call, only return results...

					$data = '<h2 class="note">No more Realtors to show.</h2>';
				} else {

					$data = '<h2 class="note m-t-0">Search returned no results. Please try a different region or name.</h2>';
				}

				$data .= '<h4>You may also be interested in the following Realtors</h4>';

				$query_string = "SELECT ra.id, ra.first_name, ra.last_name, ra.account_type, ra.phone, ra.contact_email, ra.website, ra.profile_picture, AVG(r.rating) avg_rating, COUNT(r.realtor_id) AS num_reviews
							FROM realtor_accounts ra
							LEFT JOIN reviews r 
							ON ra.id = r.realtor_id AND ra.country = ? AND ra.profile_picture <> ? AND ra.verified = ?
							GROUP BY ra.id
							HAVING COUNT(r.realtor_id) > 0
							ORDER BY avg_rating DESC
							LIMIT 5";
				$query = $this->dbo->prepare($query_string);
				$query->execute(array('US', '', 1));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$realtor_id = $row[0];
					$first_name = $row[1];
					$last_name = $row[2];
					$account_type = $row[3];
					$phone = $row[4];
					$email = $row[5];
					$website = $row[6];
					$profile_picture = $row[7];
					$avg_rating = round($row[8], 1);
					$num_reviews = $row[9];

					$realtor_id = $this->pseudocrypt->hash($realtor_id, 6);

					if($phone != '') {

						$phone = '<span class="info_item phone" data-val="' . $phone . '"><i class="fa fa-phone"></i> Phone</span>';
					}

					if($email != '') {

						$email = '<span class="info_item email" data-val="' . $email . '"><i class="fa fa-envelope"></i> Email</span>';
					}

					if($website != '') {

						$website = '<span class="info_item website"><a href="' . $website . '" target="_blank"><i class="fa fa-link"></i> Website</a></span>';
					}

					$data .= '<div class="realtor_item" id="' . $count . '">
								<table>
									<tr>
										<td>
											<a href="realtor.php?id=' . $realtor_id . '"><img src="https://scontent-lga3-1.xx.fbcdn.net/hphotos-xtp1/v/t1.0-9/11889462_479883402192845_2434390578328709503_n.jpg?oh=5a69c4de17346cd77f41037fd3da6635&oe=56977697" alt="' . $first_name . ' ' . $last_name . '" /></a>
										</td>
										<td>
											<h4 class="realtor_name"><a href="realtor.php?id=' . $realtor_id . '">' . $first_name . ' ' . $last_name . '</a></h4>
											<span class="agency_name">Sutton Group Status Realty Inc. Brokerage</span>
											<div class="clearfix"></div>
											' . $phone . '
											' . $email . '
											' . $website . '
										</td>
										<td>
											<span class="avg_rating_star">' . $avg_rating . '</span>
											<span class="num_reviews">' . $num_reviews . (($num_reviews == 1) ? ' Review' : ' Reviews') . '</span>
										</td>
									</tr>
								</table>
							</div>';

				}

				$data .= '<!--END-->'; //For ajax, to indicate that there are no results to show.
			}

			return $data;

		}



		public function writeAReviewSearch() {

			$realtor_name = htmlspecialchars($_POST['realtor_name']);
			//$realtor_name_array = explode(' ', $realtor_name);

			$city = htmlspecialchars($_POST['city']);

			// foreach ($realtor_name_array as $name_part) {
				
			// 	$name_parts_select .= $name_part . ' ';
			// 	$name_parts_where .= '+' . $name_part . ' ';
			// }

			$country = $_SESSION['country'];

			$realtor_ids = array();

			if(trim($realtor_name) != '') {

				//$query = $this->dbo->prepare("SELECT id, first_name, last_name, profile_picture, MATCH(first_name, last_name) AGAINST ('" . $name_parts_select . "') AS score FROM realtor_accounts WHERE MATCH(first_name, last_name) AGAINST ('" . $name_parts_where . "' IN BOOLEAN MODE) ORDER BY score DESC LIMIT 6");

				if($city != '') {

					$city_query = 'AND sa.city = ?';
					$query_array = array('%' . strtolower($realtor_name) . '%', '%' . strtolower($realtor_name) . '%', $city);
				
				} else {

					$city_query = '';
					$query_array = array('%' . strtolower($realtor_name) . '%', '%' . strtolower($realtor_name) . '%');
				}


				$query = $this->dbo->prepare("SELECT ra.id, ra.first_name, ra.last_name, ra.profile_picture, sa.city 
					FROM realtor_accounts ra
					JOIN realtor_service_area sa
					ON ra.id = sa.realtor_id
						AND (lower(concat_ws(' ', ra.first_name, ra.last_name)) LIKE ? 
							OR lower(concat_ws(' ', ra.last_name, ra.first_name)) LIKE ?) 
						" . $city_query . "
					GROUP BY ra.id
					LIMIT 20");
				$query->execute($query_array);

				$result = $query->fetchAll();

				foreach($result as $row) {

					$realtor_id = $this->pseudocrypt->hash($row[0], 6);
					$first_name = $row[1];
					$last_name = $row[2];
					$profile_picture = $row[3];
					$city = $row[4];

					if($profile_picture != '') {
						$profile_picture = '<img src="http://cdn.findarealtor.net/rpp/' . $profile_picture . '" alt="' . $first_name . ' ' . $last_name . '" />';
					} else {
						$profile_picture = '<img src="img/anonymous.png" alt="Anonymous" />';
					}


					// $query2 = $this->dbo->prepare("SELECT city FROM realtor_service_area WHERE realtor_id = ? ORDER BY id DESC LIMIT 1");
					// $query2->execute(array($realtor_id));

					// $result2 = $query2->fetchAll();

					// foreach($result2 as $row2) {

					// 	$city = $row2[0];

					// }

					$data .= '<li id="' . $realtor_id . '" class="realtor_search_result_item">
								' . $profile_picture . '
								<h4 class="realtor_name">' . $first_name . ' ' . $last_name . '</h4>
								<span class="service_area">' . $city . '</span>
							</li>';

				}

				if($data == '') {

					$data = '<li><span class="note">Search returned no results. Please try a different name or add a new realtor!</span></li>';
				}

				return $data;
			}

		}


		public function signUpConfirmation() {

			$user_type = htmlspecialchars($_GET['type']);
			$token = htmlspecialchars($_GET['token']);
			$email = htmlspecialchars($_GET['email']);

			if($user_type == 'user') {

				$query = $this->dbo->prepare("SELECT verified FROM user_accounts WHERE email = ? AND token = ?");
				$query->execute(array($email, $token));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$verified = $row[0];

					if($verified) {

						//already verified
						header('Location: login.php');
						exit;

					} else {

						$query = $this->dbo->prepare("UPDATE user_accounts SET verified = ? WHERE email = ? AND token = ?");
						$query->execute(array(1, $email, $token));
						

						$query = $this->dbo->prepare("SELECT id, first_name, last_name, profile_picture FROM user_accounts WHERE email = ? AND token = ?");
						$query->execute(array($email, $token));

						$result = $query->fetchAll();

						foreach($result as $row) {

							$_SESSION['user_id'] = $row[0];
							$_SESSION['user_id_hashed'] = $this->pseudocrypt->hash($row[0], 6);
							$_SESSION['first_name'] = $row[1];
							$_SESSION['last_name'] = $row[2];
							$_SESSION['profile_picture'] = $row[3];
							$_SESSION['email'] = $email;
							$_SESSION['logged_in'] = true;
							$_SESSION['user_type'] = 'user';
						}

						header('Location: signup_confirmation.php?ref=registration');
						exit;
					}
				}

				//if no matching row found, verfication is invalid.
				header('Location: signup_confirmation.php?e=invalid_confirmation');
				exit;

			} else if($user_type == 'realtor') {

				$query = $this->dbo->prepare("SELECT verified FROM realtor_accounts WHERE email = ? AND token = ?");
				$query->execute(array($email, $token));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$verified = $row[0];

					if($verified) {

						//already verified
						header('Location: realtor_login.php');
						exit;

					} else {

						$query = $this->dbo->prepare("UPDATE realtor_accounts SET verified = ? WHERE email = ? AND token = ?");
						$query->execute(array(1, $email, $token));

						
						$query = $this->dbo->prepare("SELECT id, first_name, last_name, account_type, profile_picture, country FROM realtor_accounts WHERE email = ? AND token = ?");
						$query->execute(array($email, $token));

						$result = $query->fetchAll();

						foreach($result as $row) {

							$_SESSION['realtor_id'] = $row[0];
							$_SESSION['realtor_id_hashed'] = $this->pseudocrypt->hash($row[0], 6);
							$_SESSION['first_name'] = $row[1];
							$_SESSION['last_name'] = $row[2];
							$_SESSION['account_type'] = $row[3];
							$_SESSION['profile_picture'] = $row[4];
							$_SESSION['email'] = $email;
							$_SESSION['logged_in'] = true;
							$_SESSION['user_type'] = 'realtor';
							$_SESSION['realtor_country'] = $row[5];

						}

						header('Location: signup_confirmation.php?ref=realtor_registration');
						exit;

					}
				}

				//if no matching row found, verfication is invalid.
				header('Location: signup_confirmation.php?e=invalid_confirmation');
				exit;
			
			} else {

				if(htmlspecialchars(!isset($_GET['e']))) {
					header('Location: signup_confirmation.php?e=invalid_confirmation');
					exit;
				}
				
			}

		}


		public function newsletterSignup() {

			$email = htmlspecialchars($_POST['email']);

			$query = $this->dbo->prepare("INSERT IGNORE INTO mailing_list SET email = ?");
			$query->execute(array($email));
		}



		public function sendMessage() {

			$user_type = $_POST['user_type'];
			$name = $_POST['name'];
			$email = $_POST['email'];
			$message = $_POST['message'];

			require_once('class.phpmailer.php');
			
			$mail = new PHPMailer;
			$mail->CharSet = 'UTF-8';
			$mail->isHTML(true);
			$mail->From = $email;
			$mail->FromName = $name;

			$mail->addAddress('support@findarealtor.net');
			$mail->Subject = 'FindARealtor – Contact Form';

			$mail->Body = 'From a ' . $user_type . ':<br>' . $message;

			if(!$mail->send()) {

			   return false;
			}

			return true;
		}


		public function logFeedback() {

			$name = $_POST['name'];
			$message = $_POST['message'];

			$query = $this->dbo->prepare("INSERT INTO feedback SET name = ?, message = ?");
			$query->execute(array($name, $message));
		}

	}

?>