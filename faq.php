<?php

    require_once('functions.php');

    $html = load_html();
    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>
    </head>

    <body class="do-contact-us-page">

        <?php require_once('inc/header.php'); ?>

        <section class="content">
            <div class="container">
                
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="text-center">Got a question you need answers to?<br>Our Frequently Asked Questions might have your answers!</h2>
                        <h3 class="text-center">You can also get in touch with us by sending us a message.</h3>
                    </div>
                </div>

                <br><hr><br>

                <div class="row">
                    <div class="col-xs-12 faq">
                        <div class="faq_question">
                            <h4>How do I know the agent reviews are real?</h4>
                        </div>
                        <div class="faq_answer">
                            <p>We verify each onesssssss brooooo<br>
                            yeah man... true..</p>
                        </div>
                    </div>

                    <div class="col-xs-12 faq">
                        <div class="faq_question">
                            <h4>Why was my review rejected?</h4>
                        </div>
                        <div class="faq_answer">
                            <p>We verify each onesssssss brooooo<br>
                            yeah man... true..</p>
                        </div>
                    </div>

                    <div class="col-xs-12 faq">
                        <div class="faq_question">
                            <h4>How do I edit my review?</h4>
                        </div>
                        <div class="faq_answer">
                            <p>We verify each onesssssss brooooo<br>
                            yeah man... true..</p>
                        </div>
                    </div>

                    <div class="col-xs-12 faq">
                        <div class="faq_question">
                            <h4>How do I delete a review I wrote?</h4>
                        </div>
                        <div class="faq_answer">
                            <p>We verify each onesssssss brooooo<br>
                            yeah man... true..</p>
                        </div>
                    </div>

                    <div class="col-xs-12 faq">
                        <div class="faq_question">
                            <h4>How do I reset my password?</h4>
                        </div>
                        <div class="faq_answer">
                            <p>We verify each onesssssss brooooo<br>
                            yeah man... true..</p>
                        </div>
                    </div>

                    <div class="col-xs-12 faq">
                        <div class="faq_question">
                            <h4>How can I message a Realtor?</h4>
                        </div>
                        <div class="faq_answer">
                            <p>We verify each onesssssss brooooo<br>
                            yeah man... true..</p>
                        </div>
                    </div>

                    <div class="col-xs-12 faq">
                        <div class="faq_question">
                            <h4>How do I add a new Realtor whom I want to review?</h4>
                        </div>
                        <div class="faq_answer">
                            <p>We verify each onesssssss brooooo<br>
                            yeah man... true..</p>
                        </div>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="text-center">Still have questions?&nbsp;&nbsp;&nbsp;<a href="contact.php" class="btn btn-primary m-t-10 m-b-10">Send us a message!</a></h3>
                    </div>
                </div>

            </div>
        </section>

        <?php require_once('inc/footer.php'); ?>
                                    
    </body>
</html>
