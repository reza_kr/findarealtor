<?php

    require_once('functions.php');

    $html = load_html();
    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>
    </head>

    <body class="do-contact-us-page">

        <?php require_once('inc/header.php'); ?>

        <section class="content">
            <div class="container">
            	<div class="row">
					<div class="col-xs-12 col-md-6 col-md-offset-3">

						<div class="form-group" id="search_form">
							<input type="text" id="realtor_name" class="form-control" placeholder="Search Realtors">
							<input type="text" id="search_location" class="form-control" placeholder="City">
							<span class="city_error" style="display: none;"></span>
						</div>
						<div class="form-group">
							<h4 class="note">Can't find a Realtor? <a href="add_a_realtor.php">Add &amp; Review a new Realtor</a></h4>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 col-md-6 col-md-offset-3">
						<hr>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<div class="realtor_search_results">
							<h4>Please choose a Realtor</h4>

							<div class="float_center">
								<ul class="realtor_search_results_list">
									
								</ul>
							</div>

							<div class="loading_backdrop"></div>
							<div class="loading"><img src="img/loading.gif" /></div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 col-md-6 col-md-offset-3">
						<hr>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 col-md-6 col-md-offset-3">
						<input type="hidden" name="rid" value="<?php echo htmlspecialchars($_GET['id']); ?>" />
						<a href="#" class="btn btn-primary btn-block disabled proceed">Please Choose A Realtor</a>
					</div>
				</div>
            </div>
        </section>

        <?php require_once('inc/footer.php'); ?>
                                    
    </body>
</html>
