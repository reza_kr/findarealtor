<!-- PAGE OVERLAY WHEN MENU ACTIVE -->
    <div class="do-side-menu-overlay"></div>
    <!-- PAGE OVERLAY WHEN MENU ACTIVE END -->

    <div class="do-side-menu-wrap">
        <!-- OVERLAY -->
        <div class="do-dark-overlay"></div>
        <!-- OVERLAY END -->

        <nav class="do-side-menu">
            <div class="do-side-menu-widget-wrap">
                <!-- LOGO -->
                <div class="do-side-menu-logo-wrap">
                    <a href="index-2.html">
                        <img src="images/side-menu-logo.png" alt="DO">
                    </a>
                </div>
                <!-- LOGO -->

                <!-- MENU -->
                <div class="do-side-menu-menu-wrap">
                    <ul>
                        <li>
                            <a href="#">About</a>
                        </li>
                        <li>
                            <a href="#">extra menu</a>
                        </li>
                        <li>
                            <a href="#">sample page</a>
                            <ul class="do-side-menu-menu-dropdown">
                                <li>
                                    <a href="#">Sample one</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">blog style</a>
                        </li>
                        <li>
                            <a href="#">layout</a>
                        </li>
                        <li>
                            <a href="#">design</a>
                        </li>
                    </ul>
                </div>
                <!-- MENU END -->

                <!-- SOCIAL ICONS -->
                <div class="do-side-menu-social-icon">
                    <ul>
                        <li>
                            <a href="#">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-behance"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-dribbble"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="ti-vimeo-alt"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- SOCIAL ICONS END -->
            </div>
        </nav>

        <button class="do-side-menu-close-button" id="do-side-menu-close-button">Close Menu</button>
    </div>
    <!-- SIDE MENU END -->