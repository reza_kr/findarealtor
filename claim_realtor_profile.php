<?php

    require_once('functions.php');

    $html = load_html();

    if($_GET['e'] == 'missing_fields') {

    	$message = '<div class="alert alert-danger" role="alert">Please complete all the required fields.</div>';

    } else if($_GET['e'] == 'invalid_email') {

    	$message = '<div class="alert alert-danger" role="alert">Please provide a valid email.</div>';

    } else if($_GET['e'] == 'password_too_short') {

    	$message = '<div class="alert alert-danger" role="alert">Password must be at least 8 characters long.</div>';
    
    } else if($_GET['e'] == 'email_in_use') {

    	$message = '<div class="alert alert-danger" role="alert">The email address you provided is already in use. If you already have an account, please <a href="realtor_login.php">log in here</a>. Otherwise, try a differnt email address.</div>';
    
    } else if($_GET['ref'] == 'claim_request_received') {

    	$message = '<div class="alert alert-success" role="alert">Thank you. We have received your profile claim request and will get back to you shortly to confirm this action.</div>';
    }
    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>
    </head>

    <body class="do-contact-us-page">

        <?php require_once('inc/header.php'); ?>

        <section class="content">
            <div class="container">

            	<div class="row">
					<div class="col-xs-12 col-md-6 col-md-offset-3 write_a_review_info">
						<?php echo $html['realtor']['profile_picture']; ?>
						<h2><?php echo $html['realtor']['name']; ?></h2>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 col-md-6 col-md-offset-3">
						<hr>
					</div>
				</div>
                
                <div class="row">
					<div class="col-xs-12 col-md-6 col-md-offset-3">

						<?php echo $message; ?>

						<?php

						if($_GET['ref'] == 'claim_request_received') {

							echo '<a href="realtor.php?id=' . $_GET['id'] . '" class="btn btn-primary btn-block">Return to Profile</a>';
						
						} else {

							echo '<form class="form" method="POST" action="?action=claim_realtor_profile">
									<div class="form-group">
										 <label for="first_name">First Name</label>
										 <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
									</div>
									<div class="form-group">
										 <label for="last_name">Last Name</label>
										 <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
									</div>
									<div class="form-group">
										 <label for="email">Email Address</label>
										 <input type="email" class="form-control" id="email" name="email" placeholder="Email Address">
									</div>
									<div class="form-group">
										 <label for="password">Password</label>
										 <input type="password" class="form-control" id="password" name="password" placeholder="Password">
									</div>
									<hr>
									<div class="form-group">
										 <input type="hidden" name="rid" value="' . $_GET['id'] . '" />
										 <button type="submit" class="btn btn-primary btn-block">Claim Profile</button>
									</div>
								</form>';

						}

						?>

						<br><hr><br>
						<h4 class="note">Already have a Realtor account? <a href="realtor_login.php"><b>Log in</b></a></h4>
					</div>
				</div>

            </div>
        </section>

        <?php require_once('inc/footer.php'); ?>
                                    
    </body>
</html>
