<?php

    require_once('functions.php');

    $html = load_html();

    if($_GET['e'] == 'missing_fields') {

    	$message = '<div class="alert alert-danger" role="alert">Please complete all the required fields.</div>';

    } else if($_GET['e'] == 'invalid_email') {

    	$message = '<div class="alert alert-danger" role="alert">Please provide a valid email.</div>';

    } else if($_GET['e'] == 'password_too_short') {

    	$message = '<div class="alert alert-danger" role="alert">Password must be at least 8 characters long.</div>';
    
    } else if($_GET['e'] == 'incorrect_login') {

    	$message = '<div class="alert alert-danger" role="alert">Email or password is incorrect.</div>';
    
    } else if($_GET['ref'] == 'verified') {

    	$message = '<div class="alert alert-success" role="alert">Your account has been verified! Please proceed to log in.</div>';
    
    } else if($_GET['ref'] == 'password_change_successful') {

    	$message = '<div class="alert alert-success" role="alert">Your password has been updated! Please proceed to log in.</div>';
    }
    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>
    </head>

    <body class="do-contact-us-page">

        <?php require_once('inc/header.php'); ?>

        <section class="content">
            <div class="container">
                
                <div class="row">
					<div class="col-xs-12 col-md-4 col-md-offset-4">

						<?php echo $message; ?>

						<form class="form" method="POST" action="?action=realtor_login">
							<div class="form-group">
								 <label class="sr-only" for="email">Email Address</label>
								 <input type="email" class="form-control" id="email" name="email" placeholder="Email Address">
							</div>
							<div class="form-group">
								 <label class="sr-only" for="password">Password</label>
								 <input type="password" class="form-control" id="password" name="password" placeholder="Password">
		                         <div class="help-block text-right"><a href="realtor_password_reset.php">Forgot your password?</a></div>
							</div>
							<div class="form-group">
								 <button type="submit" class="btn btn-primary btn-block">Log in</button>
							</div>
							<!-- <div class="form-group">
								<input type="checkbox" name="keep_logged_in" id="keep_logged_in"> 
								<label for="keep_logged_in">Keep me logged in</label>
							</div> -->
						</form>

						<br>
						New here? <a href="realtor_registration.php"><b>Create your personal Realtor profile!</b></a>
					</div>
				</div>

            </div>
        </section>

        <?php require_once('inc/footer.php'); ?>
                                    
    </body>
</html>
