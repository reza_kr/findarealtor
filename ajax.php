<?php
	
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

	session_start();

	require_once('classes/Realtor.class.php');
	$realtor = new Realtor();

	require_once('classes/User.class.php');
	$user = new User();

	require_once('classes/Main.class.php');
	$main = new Main();


	if($_POST['action'] == 'load_more_reviews') {

		$result = $realtor->loadRealtorReviews();

		echo $result;
	
	} else if($_GET['action'] == 'load_more_realtors') { //GET , not POST

		$result = $main->search();

		echo $result;

	} else if($_POST['action'] == 'location_autocomplete') {

		$result = $main->locationAutocomplete();

		echo $result;
	
	} else if($_POST['action'] == 'load_service_area_list') {

		$result = $main->loadServiceAreaList();

		echo $result;
	
	} else if($_POST['action'] == 'get_realtor_profile_picture') {

		$result = $realtor->getProfilePicture();

		echo $result;
	
	} else if($_POST['action'] == 'write_a_review_search') {

		$result = $main->writeAReviewSearch();

		echo $result;
	
	} else if($_POST['action'] == 'newsletter_signup') {

		$result = $main->newsletterSignup();

		echo $result;
	
	} else if($_POST['action'] == 'resend_review_request') {

		$result = $realtor->resendReviewRequest();

		echo $result;

	} else if($_POST['action'] == 'delete_review_request') {

		$result = $realtor->deleteReviewRequest();

		echo $result;
	
	} else if($_POST['action'] == 'get_realtor_profile_picture') {

		$result = $realtor->getProfilePicture();

		echo $result;

	} else if($_POST['action'] == 'remove_realtor_profile_picture') {

		$result = $realtor->removeProfilePicture();

		echo $result;
	
	} else if($_POST['action'] == 'publish_response') {

		$result = $realtor->publishResponse();

		echo $result;
	
	} else if($_POST['action'] == 'delete_response') {

		$result = $realtor->deleteResponse();

		echo $result;
	
	}


	/* Users */
	if($_POST['action'] == 'get_user_profile_picture') {

		$result = $user->getProfilePicture();

		echo $result;
	
	} else if($_POST['action'] == 'remove_user_profile_picture') {

		$result = $user->removeProfilePicture();

		echo $result;
	}


	if($_GET['action'] == 'embed_rating') {

		header('Access-Control-Allow-Origin: *');

		$callback = isset($_GET['callback']) ? preg_replace('/[^a-z0-9$_]/si', '', $_GET['callback']) : false;
		header('Content-Type: ' . ($callback ? 'application/javascript' : 'application/json') . ';charset=UTF-8');

		$rating = $realtor->realtorAvgRating();

		$data = array(rating => $rating);

		echo ($callback ? $callback . '(' : '') . json_encode($data) . ($callback ? ')' : '');
	}


	if($_POST['action'] == 'update_search_location') {

		$_SESSION['search_location'] = $_POST['address'];
	}

	if($_REQUEST['action'] == 'feedback') {

		$main->logFeedback();
	}


?>