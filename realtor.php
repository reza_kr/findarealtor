<?php

    require_once('functions.php');

    $html = load_html();
    
    if($_GET['ref'] == 'review_received') {

        $message = '<br><div class="alert alert-success" role="alert">Thank you! Your review for this Realtor has been received and is pending approval. Once approved, it will appear below!</div>';
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>
    </head>

    <body class="do-contact-us-page">

        <?php require_once('inc/header.php'); ?>

        <section class="content m-t-90">

        	<section class="profile_header">
        		<div class="container">
	        		<div class="row" style="height: 263px;">
	        			<div class="col-xs-12 col-md-3">
	        				<?php echo $html['realtor_profile']['profile_picture']; ?>
	        			</div>
	        			<div class="col-xs-12 col-md-4" style="height: 100%;">
	        				<h2 class="name"><?php echo $html['realtor_profile']['name']; ?></h2>
                            <?php echo $html['realtor_profile']['verified_realtor']; ?>

	        				<?php echo $html['realtor_profile']['phone']; ?>
	        				<?php echo $html['realtor_profile']['email']; ?>
	        				<?php echo $html['realtor_profile']['website']; ?>
	        			</div>

	        			<div class="col-xs-12 col-md-3">
	        				<label>Rating Breakdown</label>
	        				<?php echo $html['realtor_profile']['ratings_breakdown']; ?>
	        			</div>
	        			<div class="col-xs-12 col-md-2">
	        				<label>Overall Rating</label>
	        				<?php echo $html['realtor_profile']['avg_rating']; ?>

	        				<!-- <label>Brokerage</label>
	        				<?php //echo $html['realtor_profile']['agency']; ?> -->
	        			</div>
	        		</div>
	        	</div>
        	</section>

            <div class="container">
                
                <div class="row">
                	<div class="col-xs-12 col-md-8">
                        <?php echo $message; ?>

                        <hr class="visible-xs">

                        <?php echo $html['realtor_profile']['bio']; ?>

                        <hr>

                		<h3 class="pull-left">Reviews (<?php echo $html['realtor_profile']['num_reviews']; ?>)</h3>
                		<span class="pull-right sort_wrapper">Sort by: 
                			<select id="review_sort">
                				<option value="most_recent">Most Recent</option>
                				<option value="highest_rated">Highest Rated</option>
                				<option value="lowest_rated">Lowest Rated</option>
                			</select>
                		</span>
                		<div class="clearfix"></div>

                		<?php 

                            if($html['realtor_reviews'] != '') {

                                echo $html['realtor_reviews']; 

                            } else {

                                echo '<h4 class="note text-center">No reviews have been posted yet.</h4>';

                                if(!isset($_SESSION['realtor_id'])) {

                                    echo '<br><h2 class="text-center">Be the first to write a review!</h2><br><br>
                                        <a href="write_a_review.php?id=' . htmlspecialchars($_GET['id']) . '" class="btn btn-primary btn-block">Write a Review</a>';
                                }
                            }
                        ?>

                		<h2 class="note no_more_reviews">No more reviews to show.</h2>

                        <br class="visible-xs">
                        <hr class="visible-xs">
                	</div>

                	<div class="col-xs-12 col-md-4 sidebar">
                		<h3>Service Area(s)</h3>

                		<?php echo $html['realtor_profile']['service_area']; ?>
                		<script type="text/javascript">
						var service_area_json = <?php echo $html['realtor_profile']['service_area_json']; ?>;
						</script>

                		<div id="map" style="width: 100%; height: 300px"></div>

						<?php echo $html['realtor_profile']['write_a_review']; ?>

                        <?php echo $html['realtor_profile']['embed_code']; ?>

                        <?php echo $html['realtor_profile']['is_this_you']; ?>
                	</div>
                
                </div>
				
				<input type="hidden" id="rid" value="<?php echo htmlspecialchars($_GET['id']); ?>" />

            </div>
        </section>

        <?php require_once('inc/footer.php'); ?>
                                    
    </body>
</html>
