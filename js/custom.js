$(document).ready(function() {

	//var page = window.location.pathname.substring(1);
	var page = window.location.pathname.substring(1).split('/')[1]; ///// CHANGE WHEN UPLOADED TO SERVER /////
	var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character

	var $_GET = {}; //Hakish way of getting the GET parameters from the URL...

	document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
	    function decode(s) {
	        return decodeURIComponent(s.split("+").join(" "));
	    }

	    $_GET[decode(arguments[1])] = decode(arguments[2]);
	});


	$(window).scroll(function() {	

		if($('body:not(.home) nav').hasClass('minified')) {

			$('header').css('margin-bottom', '60px');
		
		} else {

			$('header').css('margin-bottom', '0');
		}
	});

	//feedback form
	fm_options = {
        bootstrap: true,
        name_placeholder: "Name",
        name_required: true,
        message_placeholder: "Your feedback message",
        feedback_url: "ajax.php",
        custom_params: {
            action: "feedback"
        },
        delayed_options: {
            delay_success_milliseconds: 3000,
            send_success : "Thank you for your feedback! We strive to make improvements with the help of feedback from our users."
        }
    };
 
    //init feedback_me plugin
    fm.init(fm_options);


	if($('#asl').val() == true) { //autocorrect_search_location

		(function() {

	   		var geocoder = new google.maps.Geocoder();

	   		//validate & autocorrect address
	   		var address = $('#ssl').val();

		    geocoder.geocode({'address': address }, function(results, status) {

		      	switch(status) {
			        case google.maps.GeocoderStatus.OK:

			          	if(results[0].types[0] == 'locality') {

			          		//address is a city, ok.
			          		var formatted_address = results[0].formatted_address;

			          		$('#ssl').val(formatted_address).trigger('change');

			          		$.ajax({
						        url: "ajax.php",
						        type: "POST",
						        data: {
						        	action: 'update_search_location',
						        	address: formatted_address
						        }
						    }).done(function(data) {


						    });
			          	}

			          	break;

		      	}

		    });

	   	})();
	}


	if($('#latlng').val() == '') {

		(function() {

	   		var geocoder = new google.maps.Geocoder();

	   		//validate & autocorrect address
	   		var address = $('#ssl').val();

		    geocoder.geocode({'address': address }, function(results, status) {

		      	switch(status) {
			        case google.maps.GeocoderStatus.OK:

		          		//address is a city, ok.
		          		var latlng = results[0].geometry.location.lat() + ',' + results[0].geometry.location.lng(); 

		          		$('#ssl').val(formatted_address).trigger('change');

		          		$.ajax({
					        url: "ajax.php",
					        type: "POST",
					        data: {
					        	action: 'update_latlng',
					        	latlng: latlng
					        }
					    }).done(function(data) {


					    });

			          	break;

		      	}

		    });

	   	})();
	}


	////////////////////////////////////////////////////////////////
	// Location Search

	// (function () {

	// 	var country = $('#c').val();

	//     $.ajax({
	//         url: "ajax.php",
	//         type: "POST",
	//         dataType: "JSON",
	//         data: {
	//         	action: 'load_service_area_list',
	//         	country: country
	//         }
	//     }).done(function(data) {

	//     	var service_areas = data;
	    	
	//     	$("#search_location").autocomplete({
	// 	      	source: service_areas,
	// 	      	focus: function (event, ui) {

	// 		        $("#search_location").val(ui.item.label);
	// 		        $("#search_location_id").val(ui.item.value);

	// 		        return false;
	// 		    },
	// 	      	select: function (event, ui) {

	// 		        $("#search_location").val(ui.item.label);
	// 		        $("#search_location_id").val(ui.item.value);

	// 		        if(page == 'search.php') {

	// 		        	$('#loc').val(ui.item.label);

	// 		        	search_map_init();
	// 		        }

	// 		        return false;
	// 		    }
	// 	    });
	//     });
	// })();

    //////////////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////////////
    //Newsletter signup

    $('.newsletter_signup').click(function() {

    	var email = $('#newsletter_email').val();

    	if(email == '') {

    		$('#newsletter_email').css('background', '#FFC8C8');
    	
    	} else {

    		$('#newsletter_email').css('background', '#FFF');

    		$.ajax({
		        url: "ajax.php",
		        type: "POST",
		        data: {
		        	action: 'newsletter_signup',
		        	email: email
		        }
		    }).done(function(data) {

		    	$('.newsletter_signup_wrapper').fadeOut(function() {

		    		$(this).html('<p>Thank you for signing up! We will keep you posted on our latest updates!</p>');
		    		$(this).fadeIn();
		    	});
		    });
    	}

    	return false;
    });

    //////////////////////////////////////////////////////////////////////

	if(page == 'index.php' || page == '') {

		$('#ssl').change(function() {

			var ssl = $(this).val();

			$('form#search #search_location').val(ssl);
		});

		function init_autocomplete() {

			//var country = $('#c').val();

			var options = {
				types: ['(cities)'],
				componentRestrictions: {country: 'US'}
			};

	      	var input = document.getElementById('search_location');
	      	var autocomplete = new google.maps.places.Autocomplete(input, options);
	   	}
	   	
	   	google.maps.event.addDomListener(window, 'load', init_autocomplete);


	   	$('form#search #submit_form').click(function(e) {

    		e.preventDefault();

    		validate_search_home();

    	});

    	$('form#search #search_location').keydown(function(e) { 

	    	if (e.keyCode == 13) { 

	    		e.preventDefault();

	       		validate_search_home();
	    	}
	  	});

    	function validate_search_home() {

	   		$('form#search .error').hide();

	   		var geocoder = new google.maps.Geocoder();

	   		//validate & autocorrect address
	   		var address = $('#search_location').val();

		    geocoder.geocode({'address': address }, function(results, status) {
		    	console.log(results[0].geometry.location.lat());
		      	switch(status) {
			        case google.maps.GeocoderStatus.OK:

			          	if(results[0].types[0] == 'locality') {
			          		//address is a city, ok.
			          		for (var i = 0; i < results[0].address_components.length; i++) {
				                if (results[0].address_components[i].types[0] == "country") { //check if country is valid
			                        
				                	var latlng = results[0].geometry.location.lat() + ',' + results[0].geometry.location.lng();

			                        var country = results[0].address_components[i].short_name;

			                        if(country == 'US') {

			                        	$('#search_location_latlng').val(latlng);

			                        	$('#search_location').val(results[0].formatted_address);

			                        	$('form#search')[0].submit();
			                        
			                        } else {

			                        	//address not within allowed country, error.
			                        	$('form#search .error').text('Please specify a city within US.');
			          					$('form#search .error').show();

			                        }
			                        
			               //          if($('#c').val().toUpperCase() == country) {

			               //          	$('#search_location').val(results[0].formatted_address);

			               //          	$('form#search')[0].submit();
			                        
			               //          } else {

			               //          	var country_string = '';

			               //          	if($('#c').val() == 'ca') {

			               //          		country_string = 'Canada';
			                        	
			               //          	} else if($('#c').val() == 'us') {

			               //          		country_string = 'US';
			               //          	}

			               //          	//address not within allowed country, error.
			               //          	$('form#search .error').text('Please specify a city within ' + country_string + '.');
			          					// $('form#search .error').show();

			               //          }
			                    }
				            }
			          		
			          	
			          	} else {
			          		//address is not a city, error.
			          		$('form#search .error').text('Invalid entry. Please specify a city or a region.');
			          		$('form#search .error').show();
			          		
			          	}

			          	break;

			        case google.maps.GeocoderStatus.ZERO_RESULTS:
			        	//address not found, error.
			        	$('form#search .error').text('City or region not found. Please try again.');
			        	$('form#search .error').show();

			          	break;

			        default:
			        	//address not found, error.
			        	$('form#search .error').text('City or region not found. Please try again.');
			        	$('form#search .error').show();

		      	}

		    });

	   	}


	} else if(page == 'login.php' || page == 'realtor_login.php') {

		$('form').validate({
			rules: {
				email: {
					required: true,
					email: true
				},
				password: {
					required: true,
					minlength: 8
				}
			}
		});

	} else if(page == 'password_reset.php' || page == 'realtor_password_reset.php') {

		$('form').validate({
			rules: {
				email: {
					required: true,
					email: true
				}
			}
		});

	} else if(page == 'password_change.php' || page == 'realtor_password_change.php') {

		$('form').validate({
			rules: {
				new_password: {
					required: true,
					minlength: 8
				},
				new_password2: {
					required: true,
					equalTo: "#new_password"
				}
			}
		});

	} else if(page == 'registration.php') {

		$('form').validate({
			rules: {
				first_name: "required",
				last_name: "required",
				email: {
					required: true,
					email: true
				},
				password: {
					required: true,
					minlength: 8
				}
			}
		});

	} else if(page == "realtor.php") {

		$('body').on('click', '.see_more', function() {

			$(this).parent().parent().find('.see_more_text').removeClass('see_more_text');
			$(this).parent().remove();

			return false;
		});

		$('#show_embed_code').click(function() {

			$('.embed_code textarea').slideDown('fast').focus().select();

			return false;
		});

		$('.embed_code textarea').focus(function() {
			$(this).select();
		});

		$('.embed_code textarea').click(function() {
			$(this).select();
		});

		$('.chart').horizBarChart();

		$('#review_sort').change(function() {

			var sort_by = $('#review_sort option:selected').val();
			var rid = $('#rid').val();

			$('.reviews_wrapper').css('opacity', '0.3');

			$.ajax({
		        url: "ajax.php",
		        type: "POST",
		        data: {
		        	action: 'load_more_reviews',
		        	id: rid,
		        	sort_by: sort_by,
		        	offset: ''
		        }
		    }).done(function(data) {

		    	$('.reviews_wrapper').html(data);
		    	$('.reviews_wrapper').css('opacity', '1');

		    	$('#load_more_reviews').show();
		    	$('.no_more_reviews').hide();
		    });
		});

		$('#load_more_reviews').click(function() {

			$(this).text('Loading...');
			$(this).addClass('disabled');

			var sort_by = $('#review_sort option:selected').val();
			var offset = $('.review_item:last').attr('id');
			var rid = $('#rid').val();

			$.ajax({
		        url: "ajax.php",
		        type: "POST",
		        data: {
		        	action: 'load_more_reviews',
		        	id: rid,
		        	sort_by: sort_by,
		        	offset: offset
		        }
		    }).done(function(data) {

		    	$('#load_more_reviews').text('Load More');
				$('#load_more_reviews').removeClass('disabled');

		    	if(data != '') {
		    	
		    		$('.reviews_wrapper').append(data);

		    	} else {

		    		$('#load_more_reviews').hide();
		    		$('.no_more_reviews').show();
		    	}

		    });

		    return false;
		});

		$('.info_item.phone').click(function () {

    		$(this).html($(this).attr('data-val'));
    	});

    	$('.info_item.email').click(function () {

    		$(this).html($(this).attr('data-val'));
    	});


    	function realtor_map_init() {
			geocoder = new google.maps.Geocoder();

			var myLatlng = new google.maps.LatLng(43.647537,-79.397875);

			var bounds = new google.maps.LatLngBounds();

			var mapOptions = {
				center: myLatlng,
				zoom: 9
			};
			
			var map = new google.maps.Map(document.getElementById('map'), mapOptions);


			///////////////////////////////////////////////////////////////


			$.each(service_area_json, function(index, address) {

				if(address != '') {

					geocoder.geocode({'address': address}, function(results, status) {

					    if (status == google.maps.GeocoderStatus.OK) {
					      	map.setCenter(results[0].geometry.location);
					      	var marker = new google.maps.Marker({
					          	map: map,
					          	position: results[0].geometry.location
					      	});

							bounds.extend(marker.position);
					    }
					});
				}
			});

		}

    	google.maps.event.addDomListener(window, 'load', realtor_map_init);


    	var write_response = '<div class="write_response_wrapper">\
								<textarea id="review_response" class="form-control" placeholder="Write a response..."></textarea>\
								<a href="#" class="btn btn-primary pull-right publish_response">Publish</a>\
								<a href="#" class="pull-right cancel_response">Cancel</a>\
							</div>';

		$('body').on('click', '.write_response', function() {

			$(this).after(write_response);
			$(this).hide();

			return false;

		});


		$('body').on('click', '.edit_response', function() {

			var review_id = $(this).closest('.review_item').find('#review_id').val();

			var response_text = $(this).closest('.review_response_wrapper').find('.response_text').text();

			$(this).closest('.review_response_wrapper').after(write_response);
			$(this).closest('.review_response_wrapper').hide();

			$(this).closest('.review_item').find('.write_response_wrapper #review_response').val(br2nl(response_text));

			return false;

		});


		$('body').on('click', '.publish_response', function() {

			var thiss = $(this);

			var review_id = $(this).closest('.review_item').find('#review_id').val();
			var response_text = $(this).closest('.review_item').find('.write_response_wrapper #review_response').val();

			$.ajax({
		        url: "ajax.php",
		        type: "POST",
		        data: {
		        	action: 'publish_response',
		        	review_id: review_id,
		        	response_text: response_text
		        }
		    }).done(function(data){

		    	if($(thiss).closest('.review_item').find('.review_response_wrapper').length > 0) {

			    	$(thiss).closest('.review_item').find('.review_response_wrapper .response_text').html(nl2br(response_text));

			    	$(thiss).closest('.review_item').find('.review_response_wrapper').show();

			    } else {

			    	$(thiss).closest('.review_item').find('.write_response_wrapper').after('<div class="review_response_wrapper">\
									<p class="response_text">' + nl2br(response_text) + '</p>\
									<a href="#" class="delete_response"><i class="fa fa-trash-o"></i> Delete</a>\
									<a href="#" class="edit_response"><i class="fa fa-pencil"></i> Edit</a>\
									<div class="clearfix"></div>\
								</div>');
			    }

			    $(thiss).closest('.review_item').find('.write_response_wrapper').remove();
		    });

			return false;

		});


		$('body').on('click', '.delete_response', function() {

			var thiss = $(this);

			var review_id = $(this).closest('.review_item').find('#review_id').val();

			$.ajax({
		        url: "ajax.php",
		        type: "POST",
		        data: {
		        	action: 'delete_response',
		        	review_id: review_id
		        }
		    }).done(function(data){

		    	$(thiss).closest('.review_response_wrapper').replaceWith('<a href="#" class="write_response"><i class="fa fa-reply"></i> Write Response</a>');
		    });

			return false;

		});


		$('body').on('click', '.cancel_response', function() {

			if($(this).closest('.review_item').find('.review_response_wrapper').length > 0) {

				$(this).closest('.review_item').find('.review_response_wrapper').show();
				$(this).closest('.write_response_wrapper').remove();

			} else {

				$(this).closest('.review_item').find('.write_response').show();
				$(this).closest('.write_response_wrapper').remove();
			}
		
			return false;
		});

	
	} else if(page == "realtor_account_settings.php") {

		var last_focus;
		var invalid_cities = 0;
		var invalid_fields = false;

		$('form').validate({
			rules: {
				first_name: "required",
				last_name: "required",
				email: {
					required: true,
					email: true
				},
				new_password: {
					required: false,
                    minlength: 8
                },
                confirm_password:  {
                	required: false,
                    equalTo: "#new_password"
                }
			}
		});


		realtor_id = $('#rid').val();

		//profile picture upload
		$('#profile_picture').fileupload({
	        url: 'ajax_file_upload.php',
	        dataType: 'json',
	        autoUpload: true,
	        paramName: 'files[]',
	        acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
		    maxFileSize: 10000000, // 10 MB
		    imageMaxWidth: 3000,
		    imageMaxHeight: 3000,
	        formData: {
	        	action: 'upload_realtor_profile_picture',
	        	realtor_id: realtor_id
	        },
	        start: function(e, data) {

	        	$('.profile_picture_upload').fadeOut('fast', function() {

		        	$('#profile_picture_progress .bar').css('width', '0%');
		        	$('#profile_picture_progress').slideDown('fast');

		        });

	        },
	        progressall: function (e, data) {

	            var progress = parseInt(data.loaded / data.total * 100, 10);

	            $('#profile_picture_progress .bar').css('width', progress + '%');
	        },
	        fail: function(e, data) {
	        	console.log(e);
	        	console.log(data);

			},
	        done: function(e, d) {

	        	$('#profile_picture_progress .bar').css('width', '100%');

	        	$.ajax({
			        url: "ajax.php",
			        type: "POST",
			        data: {
			        	action: 'get_realtor_profile_picture',
			        	realtor_id: realtor_id
			        }
			    }).done(function(data){

		        	if(data) {

						$('#profile_picture_preview_wrapper img').attr('src', 'http://cdn.findarealtor.net/rpp/' + data);

						$('#profile_picture_preview_wrapper img').load( function() {

							$('#profile_picture_progress').slideUp(function() {

								$('#profile_picture_preview_wrapper').slideDown();
								$('#profile_picture_preview_wrapper img').slideDown();

								$('#profile_picture_preview_wrapper #remove_picture').remove();
								$('#profile_picture_preview_wrapper img').after('<a href="#" id="remove_picture" title="Remove Profile Picture"><i class="fa fa-close"></i></a>');

				    			setTimeout(function() {

			    					$('.profile_picture_upload').fadeIn();

			    				}, 500);
			    			});

						});

					}
				});

	        }

	    });


		$('body').on('click', '#remove_picture', function() {

			$.ajax({
		        url: "ajax.php",
		        type: "POST",
		        data: {
		        	action: 'remove_realtor_profile_picture'
		        }
		    }).done(function(data){

		    	$('#profile_picture_preview_wrapper img').attr('src', 'img/anonymous.png');
		    	$('#profile_picture_preview_wrapper #remove_picture').remove();
		    });

		    return false;
		});


		function realtor_edit_map_init() {

			//var country = $('#country option:selected').val();

			var options = {
				types: ['(cities)'],
				componentRestrictions: {country: 'US'}
			};

			var input_1 = document.getElementById('service_area_1');
	      	var autocomplete_1 = new google.maps.places.Autocomplete(input_1, options);
	      	google.maps.event.addDomListener(input_1, 'keydown', function(e) { 
		    	if (e.keyCode == 13) { 
		       		e.preventDefault(); 
		    	}
		  	});

	      	var input_2 = document.getElementById('service_area_2');
	      	var autocomplete_2 = new google.maps.places.Autocomplete(input_2, options);
	      	google.maps.event.addDomListener(input_2, 'keydown', function(e) { 
		    	if (e.keyCode == 13) { 
		       		e.preventDefault(); 
		    	}
		  	});

	      	var input_3 = document.getElementById('service_area_3');
	      	var autocomplete_3 = new google.maps.places.Autocomplete(input_3, options);
	      	google.maps.event.addDomListener(input_3, 'keydown', function(e) { 
		    	if (e.keyCode == 13) { 
		       		e.preventDefault(); 
		    	}
		  	});

		  	var input_4 = document.getElementById('service_area_4');
	      	var autocomplete_4 = new google.maps.places.Autocomplete(input_4, options);
	      	google.maps.event.addDomListener(input_4, 'keydown', function(e) { 
		    	if (e.keyCode == 13) { 
		       		e.preventDefault(); 
		    	}
		  	});

		  	var input_5 = document.getElementById('service_area_5');
	      	var autocomplete_5 = new google.maps.places.Autocomplete(input_5, options);
	      	google.maps.event.addDomListener(input_5, 'keydown', function(e) { 
		    	if (e.keyCode == 13) { 
		       		e.preventDefault(); 
		    	}
		  	});

		  	var input_6 = document.getElementById('service_area_6');
	      	var autocomplete_6 = new google.maps.places.Autocomplete(input_6, options);
	      	google.maps.event.addDomListener(input_6, 'keydown', function(e) { 
		    	if (e.keyCode == 13) { 
		       		e.preventDefault(); 
		    	}
		  	});

	      	/////////////////////////////////////////////////////////////


			geocoder = new google.maps.Geocoder();

			var myLatlng = new google.maps.LatLng(43.647537,-79.397875);

			var bounds = new google.maps.LatLngBounds();

			var mapOptions = {
				center: myLatlng,
				zoom: 9
			};
			
			var map = new google.maps.Map(document.getElementById('map'), mapOptions);


			///////////////////////////////////////////////////////////////


			$.each(service_area_json, function(index, address) {

				if(address != '') {

					geocoder.geocode({'address': address}, function(results, status) {

					    if (status == google.maps.GeocoderStatus.OK) {
					      	map.setCenter(results[0].geometry.location);
					      	var marker = new google.maps.Marker({
					          	map: map,
					          	position: results[0].geometry.location
					      	});

							bounds.extend(marker.position);
					    }
					});
				}
			});

		}

    	google.maps.event.addDomListener(window, 'load', realtor_edit_map_init);


    	$('form#update_realtor_profile #submit_form').click(function(e) {

    		e.preventDefault();

    		validate_update_profile();

    		if(invalid_cities == false && invalid_fields == false) {

    			$('form#update_realtor_profile')[0].submit();
    		}

    	});

    	$('form#update_realtor_profile .service_area_city').blur(function() {

    		last_focus = $(this);

    		var this_field = $(this);

	    	validate_update_profile();
    	});

    	$('form#update_realtor_profile .service_area_city').keydown(function(e) { 

	    	if (e.keyCode == 13) { 

	    		e.preventDefault();

	       		validate_update_profile();
	    	}
	  	});

    	function validate_update_profile() {

    		if(!$('form').valid()) {

    			invalid_fields = true;
    		
    		} else {

    			invalid_fields = false;
    		}

	   		$(last_focus).removeClass('error');

	   		if(typeof last_focus == 'undefined') {

	   			invalid_cities = false;

	   		} else {

		   		var geocoder = new google.maps.Geocoder();

		   		//validate & autocorrect address
		   		var address = $(last_focus).val();

		   		if(address == '') {

		   			invalid_cities = false;
		   		
		   		} else {

				    geocoder.geocode({'address': address }, function(results, status) {

				      	switch(status) {
					        case google.maps.GeocoderStatus.OK:

					          	if(results[0].types[0] == 'locality') {
					          		//address is a city, ok.
					          		for (var i = 0; i < results[0].address_components.length; i++) {
						                if (results[0].address_components[i].types[0] == "country") { //check if country is valid
					                        
					                        var country = results[0].address_components[i].short_name;

					                        if(country == 'US') {

					                        	$(last_focus).val(results[0].formatted_address);

					                        	invalid_cities = false;
					                        
					                        } else {

					                        	//address not within allowed country, error.
					          					$(last_focus).addClass('error');

					          					invalid_cities = true;
					                        }

					               //          if($('#country option:selected').val().toUpperCase() == country) {

					               //          	$(last_focus).val(results[0].formatted_address);

					               //          	invalid_cities = false;
					                        
					               //          } else {

					               //          	//address not within allowed country, error.
					          					// $(last_focus).addClass('error');

					          					// invalid_cities = true;
					               //          }
					                    }
						            }
					          		
					          	
					          	} else {
					          		//address is not a city, error.
					          		$(last_focus).addClass('error');

					          		invalid_cities = true;
					          	}

					          	break;

					        case google.maps.GeocoderStatus.ZERO_RESULTS:
					        	//address not found, error.
					        	$(last_focus).addClass('error');

					        	invalid_cities = true;

					          	break;

					        default:
					        	//address not found, error.
					        	$(last_focus).addClass('error');

					        	invalid_cities = true;

				      	}

				    });
				}
			}
	   	}


	} else if(page == "account_settings.php") {

		$('form').validate({
			rules: {
				first_name: "required",
				last_name: "required",
				email: {
					required: true,
					email: true
				},
				new_password: {
					required: false,
                    minlength: 8
                },
                confirm_password:  {
                	required: false,
                    equalTo: "#new_password"
                }
			}
		});


		user_id = $('#uid').val();

		//profile picture upload
		$('#profile_picture').fileupload({
	        url: 'ajax_file_upload.php',
	        dataType: 'json',
	        autoUpload: true,
	        paramName: 'files[]',
	        acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
		    maxFileSize: 10000000, // 10 MB
		    imageMaxWidth: 3000,
		    imageMaxHeight: 3000,
	        formData: {
	        	action: 'upload_user_profile_picture',
	        	user_id: user_id
	        },
	        start: function(e, data) {

	        	$('.profile_picture_upload').fadeOut('fast', function() {

		        	$('#profile_picture_progress .bar').css('width', '0%');
		        	$('#profile_picture_progress').slideDown('fast');

		        });

	        },
	        progressall: function (e, data) {

	            var progress = parseInt(data.loaded / data.total * 100, 10);

	            $('#profile_picture_progress .bar').css('width', progress + '%');
	        },
	        fail: function(e, data) {
	        	console.log(e);
	        	console.log(data);

			},
	        done: function(e, d) {

	        	$('#profile_picture_progress .bar').css('width', '100%');

	        	$.ajax({
			        url: "ajax.php",
			        type: "POST",
			        data: {
			        	action: 'get_user_profile_picture',
			        	user_id: user_id
			        }
			    }).done(function(data){

		        	if(data) {

						$('#profile_picture_preview_wrapper img').attr('src', 'http://cdn.findarealtor.net/upp/' + data);

						$('#profile_picture_preview_wrapper img').load( function() {

							$('#profile_picture_progress').slideUp(function() {

								$('#profile_picture_preview_wrapper').slideDown();
								$('#profile_picture_preview_wrapper img').slideDown();

				    			setTimeout(function() {

			    					$('.profile_picture_upload').fadeIn();

			    				}, 500);
			    			});

						});

					}
				});

	        }

	    });

		
		$('body').on('click', '#remove_picture', function() {

			$.ajax({
		        url: "ajax.php",
		        type: "POST",
		        data: {
		        	action: 'remove_user_profile_picture'
		        }
		    }).done(function(data){

		    	$('#profile_picture_preview_wrapper img').attr('src', 'img/anonymous.png');
		    	$('#profile_picture_preview_wrapper #remove_picture').remove();
		    });

		    return false;
		});
	

	} else if(page == "search.php") {

		$('#ssl').change(function() {

			var ssl = $(this).val();

			$('form#search_form #search_location').val(ssl);
		});

		function search_map_init() {

			//var country = $('#c').val();

			var options = {
				types: ['(cities)'],
				componentRestrictions: {country: 'US'}
			};

	      	var input = document.getElementById('search_location');
	      	var autocomplete = new google.maps.places.Autocomplete(input, options);

	      	///////////////////////////////////////////////////////////////

			var geocoder = new google.maps.Geocoder();

			var myLatlng = new google.maps.LatLng(43.647537,-79.397875);

			var mapOptions = {
				center: myLatlng,
				zoom: 9
			};
			
			var map = new google.maps.Map(document.getElementById('map'), mapOptions);


			///////////////////////////////////////////////////////////////


			var address = $('#loc').val();
			geocoder.geocode({'address': address}, function(results, status) {

			    if (status == google.maps.GeocoderStatus.OK) {
			      	map.setCenter(results[0].geometry.location);
			      	var marker = new google.maps.Marker({
			          	map: map,
			          	position: results[0].geometry.location
			      	});
			    }
			 });


			//////////////////////////////////////////////////////////////
		}

    	google.maps.event.addDomListener(window, 'load', search_map_init);


    	$('form#search_form #submit_form').click(function(e) {

    		e.preventDefault();

    		validate_search();

    	});

    	$('form#search_form #search_location').keydown(function(e) { 

	    	if (e.keyCode == 13) { 

	    		e.preventDefault();

	       		validate_search();
	    	}
	  	});

    	function validate_search() {

	   		$('form#search_form .error').hide();

	   		var geocoder = new google.maps.Geocoder();

	   		//validate & autocorrect address
	   		var address = $('#search_location').val();

		    geocoder.geocode({'address': address }, function(results, status) {

		      	switch(status) {
			        case google.maps.GeocoderStatus.OK:

			          	if(results[0].types[0] == 'locality') {
			          		//address is a city, ok.
			          		for (var i = 0; i < results[0].address_components.length; i++) {
				                if (results[0].address_components[i].types[0] == "country") { //check if country is valid
			                        
			                        var latlng = results[0].geometry.location.lat() + ',' + results[0].geometry.location.lng();

			                        var country = results[0].address_components[i].short_name;

			                        if(country == 'US') {

			                        	$('#search_location_latlng').val(latlng);

			                        	$('#search_location').val(results[0].formatted_address);

			                        	$('form#search_form')[0].submit();
			                        
			                        } else {

			                        	//address not within allowed country, error.
			                        	$('form#search_form .error').text('Please specify a city within US.');
			          					$('form#search_form .error').show();

			                        }
			                        
			               //          if($('#c').val().toUpperCase() == country) {

			               //          	$('#search_location').val(results[0].formatted_address);

			               //          	$('form#search_form')[0].submit();
			                        
			               //          } else {

			               //          	var country_string = '';

			               //          	if($('#c').val() == 'ca') {

			               //          		country_string = 'Canada';
			                        	
			               //          	} else if($('#c').val() == 'us') {

			               //          		country_string = 'US';
			               //          	}

			               //          	//address not within allowed country, error.
			               //          	$('form#search_form .error').text('Please specify a city within ' + country_string + '.');
			          					// $('form#search_form .error').show();

			               //          }
			                    }
				            }
			          		
			          	
			          	} else {
			          		//address is not a city, error.
			          		$('form#search_form .error').text('Invalid entry. Please specify a city or a region.');
			          		$('form#search_form .error').show();
			          		
			          	}

			          	break;

			        case google.maps.GeocoderStatus.ZERO_RESULTS:
			        	//address not found, error.
			        	$('form#search_form .error').text('City or region not found. Please try again.');
			        	$('form#search_form .error').show();

			          	break;

			        default:
			        	//address not found, error.
			        	$('form#search_form .error').text('City or region not found. Please try again.');
			        	$('form#search_form .error').show();

		      	}

		    });

	   	}




    	$('.info_item.phone').click(function () {

    		$(this).html($(this).attr('data-val'));
    	});

    	$('.info_item.email').click(function () {

    		$(this).html($(this).attr('data-val'));
    	});


    	$('#load_more_realtors').click(function() {

			$(this).text('Loading...');
			$(this).addClass('disabled');

			var offset = $('.realtor_item:last').attr('id');
			var name = $('#search_form #realtor_name').val();
			var city = $('#search_form #search_location').val();
			var sort = $('#search_form #sort option:selected').val();
			var picture_only = $('#search_form #picture_only').val();

			$.ajax({
		        url: "ajax.php",
		        type: "GET",
		        data: {
		        	action: 'load_more_realtors',
		        	offset: offset,
		        	name: name,
		        	city: city,
		        	sort: sort,
		        	picture_only: picture_only
		        }
		    }).done(function(data) {

		    	$('#load_more_realtors').text('Load More');
				$('#load_more_realtors').removeClass('disabled');

		    	if(data.indexOf("<!--END-->") >= 0) {
		    		//end reached
		    		$('#load_more_realtors').hide();
		    		//$('.no_more_realtors').show();

		    	}

		    	$('.search_results_wrapper').append(data);

		    });

		    return false;
		});

	} else if(page == 'realtor_registration.php') {

		var invalid_city = false;
		var invalid_fields = false;

		$('form').validate({
			rules: {
				first_name: "required",
				last_name: "required",
				email: {
					required: true,
					email: true
				},
				password: {
					required: true,
					minlength: 8
				},
				//country: "required"
			}
		});

		function realtor_registration_autocomplete_init() {

			//var country = $('#country option:selected').val();

			var options = {
				types: ['(cities)'],
				componentRestrictions: {country: 'US'}
			};

	      	var input = document.getElementById('service_area');

	      	google.maps.event.clearInstanceListeners(input);

	      	var autocomplete = new google.maps.places.Autocomplete(input, options);

	      	google.maps.event.addDomListener(input, 'keydown', function(e) { 
		    	if (e.keyCode == 13) { 
		       		e.preventDefault(); 
		    	}
		  	});

		}

    	google.maps.event.addDomListener(window, 'load', realtor_registration_autocomplete_init);


		// $('#country').change(function() {
		// 	$("#service_area").val('');

		// 	realtor_registration_autocomplete_init();
		// });


		$('form#realtor_registration #submit_form').click(function(e) {

    		e.preventDefault();

    		validate_realtor_register();

    		if(invalid_city == false && invalid_fields == false) {

    			$('form#realtor_registration')[0].submit();
    		}

    	});

    	$('form#realtor_registration #service_area').blur(function() {

	    	validate_realtor_register();
    	});

    	$('form#realtor_registration #service_area').keydown(function(e) { 

	    	if (e.keyCode == 13) { 

	    		e.preventDefault();

	       		validate_realtor_register();
	    	}
	  	});

    	function validate_realtor_register() {

    		if(!$('form').valid()) {

    			invalid_fields = true;
    		
    		} else {

    			invalid_fields = false;
    		}

	   		$('form#realtor_registration .city_error').hide();

	   		var geocoder = new google.maps.Geocoder();

	   		//validate & autocorrect address
	   		var address = $('#service_area').val();

		    geocoder.geocode({'address': address }, function(results, status) {

		      	switch(status) {
			        case google.maps.GeocoderStatus.OK:

			          	if(results[0].types[0] == 'locality') {
			          		//address is a city, ok.
			          		for (var i = 0; i < results[0].address_components.length; i++) {
				                if (results[0].address_components[i].types[0] == "country") { //check if country is valid
			                        
			                        var country = results[0].address_components[i].short_name;

			                        if(country == 'US') {

			                        	$('#service_area').val(results[0].formatted_address);

			                        	invalid_city = false;
			                        
			                        } else {

			                        	//address not within allowed country, error.
			                        	$('form#realtor_registration .city_error').text('Please specify a city within US.');
			          					$('form#realtor_registration .city_error').show();

			          					invalid_city = true;
			                        }

			               //          if($('#country option:selected').val().toUpperCase() == country) {

			               //          	$('#service_area').val(results[0].formatted_address);

			               //          	invalid_city = false;
			                        
			               //          } else {

			               //          	var country_string = '';

			               //          	if($('#country option:selected').val() == 'ca') {

			               //          		country_string = 'Canada';
			                        	
			               //          	} else if($('#country option:selected').val() == 'us') {

			               //          		country_string = 'US';
			               //          	}

			               //          	//address not within allowed country, error.
			               //          	$('form#realtor_registration .city_error').text('Please specify a city within ' + country_string + '.');
			          					// $('form#realtor_registration .city_error').show();

			          					// invalid_city = true;
			               //          }
			                    }
				            }
			          		
			          	
			          	} else {
			          		//address is not a city, error.
			          		$('form#realtor_registration .city_error').text('Invalid entry. Please specify a city or a region.');
			          		$('form#realtor_registration .city_error').show();
			          		
			          		invalid_city = true;
			          	}

			          	break;

			        case google.maps.GeocoderStatus.ZERO_RESULTS:
			        	//address not found, error.
			        	$('form#realtor_registration .city_error').text('City or region not found. Please try again.');
			        	$('form#realtor_registration .city_error').show();

			        	invalid_city = true;

			          	break;

			        default:
			        	//address not found, error.
			        	$('form#realtor_registration .city_error').text('City or region not found. Please try again.');
			        	$('form#realtor_registration .city_error').show();

			        	invalid_city = true;
		      	}

		    });

	   	}

	
	} else if(page == 'add_a_realtor.php') {

		$('form').validate({
			rules: {
				first_name: "required",
				last_name: "required",
				phone: {
					required: true,
					number: true
				},
				//country: "required"
			}
		});

		function add_a_realtor_autocomplete_init() {

			//var country = $('#country option:selected').val();

			var options = {
				types: ['(cities)'],
				componentRestrictions: {country: 'US'}
			};

	      	var input = document.getElementById('service_area');

	      	google.maps.event.clearInstanceListeners(input);

	      	var autocomplete = new google.maps.places.Autocomplete(input, options);

	      	google.maps.event.addDomListener(input, 'keydown', function(e) { 
		    	if (e.keyCode == 13) { 
		       		e.preventDefault(); 
		    	}
		  	});

		}

    	google.maps.event.addDomListener(window, 'load', add_a_realtor_autocomplete_init);


		// $('#country').change(function() {
		// 	$("#service_area").val('');

		// 	add_a_realtor_autocomplete_init();
		// });


		$('form#add_a_realtor #submit_form').click(function(e) {

    		e.preventDefault();

    		validate_add_realtor();

    	});

    	$('form#add_a_realtor #service_area').blur(function() {

	    	validate_add_realtor();
    	});

    	$('form#add_a_realtor #service_area').keydown(function(e) { 

	    	if (e.keyCode == 13) { 

	    		e.preventDefault();

	       		validate_add_realtor();
	    	}
	  	});

    	function validate_add_realtor() {

    		$('form').valid();

	   		$('form#add_a_realtor .city_error').hide();

	   		var geocoder = new google.maps.Geocoder();

	   		//validate & autocorrect address
	   		var address = $('#service_area').val();

		    geocoder.geocode({'address': address }, function(results, status) {

		      	switch(status) {
			        case google.maps.GeocoderStatus.OK:

			          	if(results[0].types[0] == 'locality') {
			          		//address is a city, ok.
			          		for (var i = 0; i < results[0].address_components.length; i++) {
				                if (results[0].address_components[i].types[0] == "country") { //check if country is valid
			                        
			                        var country = results[0].address_components[i].short_name;

			                        if(country == 'US') {

			                        	$('#service_area').val(results[0].formatted_address);
			                        
			                        } else {

			                        	//address not within allowed country, error.
			                        	$('form#add_a_realtor .city_error').text('Please specify a city within US.');
			          					$('form#add_a_realtor .city_error').show();

			                        }

			               //          if($('#country option:selected').val().toUpperCase() == country) {

			               //          	$('#service_area').val(results[0].formatted_address);
			                        
			               //          } else {

			               //          	var country_string = '';

			               //          	if($('#country option:selected').val() == 'ca') {

			               //          		country_string = 'Canada';
			                        	
			               //          	} else if($('#country option:selected').val() == 'us') {

			               //          		country_string = 'US';
			               //          	}

			               //          	//address not within allowed country, error.
			               //          	$('form#add_a_realtor .city_error').text('Please specify a city within ' + country_string + '.');
			          					// $('form#add_a_realtor .city_error').show();

			               //          }
			                    }
				            }
			          		
			          	
			          	} else {
			          		//address is not a city, error.
			          		$('form#add_a_realtor .city_error').text('Invalid entry. Please specify a city or a region.');
			          		$('form#add_a_realtor .city_error').show();
			          		
			          	}

			          	break;

			        case google.maps.GeocoderStatus.ZERO_RESULTS:
			        	//address not found, error.
			        	$('form#add_a_realtor .city_error').text('City or region not found. Please try again.');
			        	$('form#add_a_realtor .city_error').show();

			          	break;

			        default:
			        	//address not found, error.
			        	$('form#add_a_realtor .city_error').text('City or region not found. Please try again.');
			        	$('form#add_a_realtor .city_error').show();

		      	}

		    });

	   	}

	
	} else if(page == 'claim_realtor_profile.php') {

		$('form').validate({
			rules: {
				first_name: "required",
				last_name: "required",
				email: {
					required: true,
					email: true
				},
				password: {
					required: true,
					minlength: 8
				}
			}
		});

	} else if(page == 'choose_realtor.php') {

		var search_requests = new Array();
		var typing_delay;
		var invalid_city = false;

		$('#realtor_name').on('keyup', function() {

			if(invalid_city != true) {

				clearTimeout(typing_delay);

				typing_delay = setTimeout(function () {
					realtor_search();
				}, 200);
			}
		});


		function realtor_search() {

			var realtor_name = $('#realtor_name').val();
			realtor_name = $.trim(realtor_name);

			var city = $('#search_location').val();

	        if(realtor_name.length > 0) {

	        	$('.realtor_search_results:not(:visible)').slideDown('fast');

	        	$('.realtor_search_results_list').css('opacity', '0.3');
	        	$('.loading').fadeIn();

	        	for(var i = 0; i < search_requests.length; i++) {

	    			search_requests[i].abort();
				}

				search_requests = [];

	        	search_requests.push(

					$.ajax({
				        url: "ajax.php",
				        type: "POST",
				        data: {
				        	action: 'write_a_review_search',
				        	realtor_name: realtor_name,
				        	city: city
				        }
				    }).done(function(data) {

				    	$('.realtor_search_results_list').css('opacity', '1');
	        			$('.loading').fadeOut();

						$('.realtor_search_results_list').html(data);

						$('.proceed').text('Please choose a Realtor');
						$('.proceed').addClass('disabled');
						$('.proceed').attr('href', '#');
				    	
				    })
				);


	        } else {

	        	for(var i = 0; i < search_requests.length; i++) {
	    			search_requests[i].abort();
				}

				search_requests = [];

				$('.realtor_search_results:visible').slideUp('fast');

	        }
		}


		$('body').on('click', '.realtor_search_result_item', function() {

			var realtor_id = $(this).attr('id');

			$('.realtor_search_result_item').removeClass('active');
			$(this).addClass('active');

			$('.proceed').text('Proceed');
			$('.proceed').removeClass('disabled');
			$('.proceed').attr('href', 'write_a_review.php?id=' + realtor_id);

			return false;
		});



		function choose_realtor_autocomplete_init() {

			//var country = $('#c').val();

			var options = {
				types: ['(cities)'],
				componentRestrictions: {country: 'US'}
			};

	      	var input = document.getElementById('search_location');

	      	google.maps.event.clearInstanceListeners(input);

	      	var autocomplete = new google.maps.places.Autocomplete(input, options);

	      	google.maps.event.addDomListener(input, 'keydown', function(e) { 
		    	if (e.keyCode == 13) { 
		       		e.preventDefault(); 

		       		realtor_search();
		    	}
		  	});

		}

    	google.maps.event.addDomListener(window, 'load', choose_realtor_autocomplete_init);


    	$('#search_location').blur(function() {

	    	validate_choose_realtor();
    	});

    	$('#search_location').keydown(function(e) { 

	    	if (e.keyCode == 13) { 

	       		validate_choose_realtor();
	    	}
	  	});

    	function validate_choose_realtor() {

	   		$('.city_error').hide();

	   		var geocoder = new google.maps.Geocoder();

	   		//validate & autocorrect address
	   		var address = $('#search_location').val();

	   		if(address == '') {

	   			invalid_city = false;

	   			realtor_search();
	   		
	   		} else {

			    geocoder.geocode({'address': address }, function(results, status) {

			      	switch(status) {
				        case google.maps.GeocoderStatus.OK:

				          	if(results[0].types[0] == 'locality') {
				          		//address is a city, ok.
				          		for (var i = 0; i < results[0].address_components.length; i++) {
					                if (results[0].address_components[i].types[0] == "country") { //check if country is valid
				                        
				                        var country = results[0].address_components[i].short_name;

				                        if(country == 'US') {

				                        	$('#search_location').val(results[0].formatted_address);

				                        	invalid_city = false;

				                        	realtor_search();
				                        
				                        } else {

				                        	//address not within allowed country, error.
				                        	$('.city_error').text('Please specify a city within US.');
				          					$('.city_error').show();

				          					invalid_city = true;

				                        }

				               //          if($('#c').val().toUpperCase() == country) {

				               //          	$('#search_location').val(results[0].formatted_address);

				               //          	invalid_city = false;

				               //          	realtor_search();
				                        
				               //          } else {

				               //          	var country_string = '';

				               //          	if($('#c').val() == 'ca') {

				               //          		country_string = 'Canada';
				                        	
				               //          	} else if($('#c').val() == 'us') {

				               //          		country_string = 'US';
				               //          	}

				               //          	//address not within allowed country, error.
				               //          	$('.city_error').text('Please specify a city within ' + country_string + '.');
				          					// $('.city_error').show();

				          					// invalid_city = true;

				               //          }
				                    }
					            }
				          		
				          	
				          	} else {
				          		//address is not a city, error.
				          		$('.city_error').text('Invalid entry. Please specify a city or a region.');
				          		$('.city_error').show();

				          		invalid_city = true;
				          	}

				          	break;

				        case google.maps.GeocoderStatus.ZERO_RESULTS:
				        	//address not found, error.
				        	$('.city_error').text('City or region not found. Please try again.');
				        	$('.city_error').show();

				        	invalid_city = true;

				          	break;

				        default:
				        	//address not found, error.
				        	$('.city_error').text('City or region not found. Please try again.');
				        	$('.city_error').show();

				        	invalid_city = true;

			      	}

			    });
			}
	   	}


	} else if(page == 'write_a_review.php') {

		$('form').validate({
			ignore: [],
			rules: {
				satisfaction: {
					required: true
				},
				details: {
					wordCount: ['30']
				}
			},
			messages: {
				satisfaction: {
					required: "Please choose a star rating for this Realtor."
				}
			},
			errorPlacement: function(error, element) {
			    if(element.is(":radio")) {
	                error.appendTo(element.parents('.form-group'));
	            } else {
	                error.insertAfter(element);
	            }
			}
		});

		$('.star_rating [type*="radio"]').change(function () {
		    var rating = $(this).attr('value');
		    
		    $('#satisfaction_rating').html(rating + '/10');
		});
	
	} else if(page == 'request_a_review.php') {

		$('form').validate({
			rules: {
				client_first_name: "required",
				client_email: {
					required: true,
					email: true
				},
				password: {
					required: true,
					minlength: 8
				},
				message: {
					required: true,
					minlength: 50
				}
			}
		});


		$('#bulk_upload_contacts').change(function(e) {

			var ext = $(this).val().split(".").pop().toLowerCase();

			if(ext != 'csv') {

				alert('Please upload a valid CSV file!');
				return false;
			}

			if (e.target.files != undefined) {

				var reader = new FileReader();

				var result;

				reader.onload = function(e) {

					$('#contact_list_csv').val(e.target.result);

					$('.single_client').slideUp('fast');
					$('.bulk_clients').slideDown('fast');

					// var lines = e.target.result.split("\n");

					// for (var i = 0; i < lines.length; i++) {

					// 	var line_values = lines[i].split(",");
						
					// 	for (var j = 0; j < line_values.length; j++) {

					// 		var current_val = $('#contacts_list_csv').val();
					// 		current_val += line_values[j] + ', ';
					// 		$('#contacts_list_csv').val(current_val);
					// 	}
					// }

					// //console.log(result);
				};

				reader.readAsText(e.target.files.item(0));

			}

			return false;
		});


		$('body').on('click', '.resend_review_request', function() {

			var request_id = $(this).closest('tr').attr('id');

			$.ajax({
		        url: "ajax.php",
		        type: "POST",
		        data: {
		        	action: 'resend_review_request',
		        	request_id: request_id
		        }
		    }).done(function(data) { //return datetime

		    	var tr = $('.request_history tr#' + request_id).html();

		    	$('.request_history tr#' + request_id).remove();
		    	$('.request_history tbody').prepend('<tr id="' + request_id + '">' + tr + '</tr>');
		    	$('.request_history tbody tr:first td:eq(2)').text(data);
		    });

		    return false;
		});

		$('body').on('click', '.delete_review_request', function() {

			var request_id = $(this).closest('tr').attr('id');

			$.ajax({
		        url: "ajax.php",
		        type: "POST",
		        data: {
		        	action: 'delete_review_request',
		        	request_id: request_id
		        }
		    }).done(function(data) {

		    	$('.request_history tbody').html(data);
		    });

		    return false;
		});

	} else if(page == 'my_reviews.php') {

		$('#edit_review_modal form').validate({
			ignore: [],
			rules: {
				satisfaction: {
					required: true
				},
				details: {
					wordCount: ['30']
				}
			},
			messages: {
				satisfaction: {
					required: "Please choose a star rating for this Realtor."
				}
			},
			errorPlacement: function(error, element) {
			    if(element.is(":radio")) {
	                error.appendTo(element.parents('.form-group'));
	            } else {
	                error.insertAfter(element);
	            }
			}
		});

		$('.edit_review').click(function() {

			$('#edit_review_modal #details').val('');
			$("#edit_review_modal input[name=satisfaction]").each(function() {
				$(this).prop('checked', false);
			});
			$('#edit_review_modal #anonymous').prop('checked', false);

			var review_id = $(this).attr('id');
			var review_text = $(this).closest('tr').find('td.review_text').text();
			var rating = $(this).closest('tr').find('td.rating').text();
			var anonymous = ($(this).closest('tr').find('td.anonymous').text() == 'Yes') ? 1 : 0;

			$('#edit_review_modal').modal('show');

			$('#edit_review_modal #review_id').val(review_id);
			$('#edit_review_modal #details').val(review_text);
			$("#edit_review_modal input[name=satisfaction][value=" + rating + "]").attr('checked', 'checked');

			if(anonymous == 1) {

				$('#edit_review_modal #anonymous').attr('checked', 'checked');
			}
		});

		$('.delete_review').click(function() {

			var review_id = $(this).attr('id');

			$('#delete_review_modal').modal('show');

			$('#delete_review_modal #review_id').val(review_id);

		});
	
	} else if(page == 'contact.php') {

		$('form').validate({
			ignore: [],
			rules: {
				user_type: {
					required: true
				},
				name: {
					required: true
				},
				email: {
					required: true,
					email: true
				}
			},
			errorPlacement: function(error, element) {
			    if(element.is(":radio")) {
	                error.appendTo(element.parents('.form-group'));
	            } else {
	                error.insertAfter(element);
	            }
			}
		});
	
	} else if(page == 'process_payment.php') {

		// $('form').validate({
		// 	ignore: [],
		// 	rules: {
		// 		plan_duration: {
		// 			required: true
		// 		}
		// 	},
		// 	errorPlacement: function(error, element) {
		// 	    if(element.is(":radio")) {
	 //                error.appendTo(element.parents('.form-group'));
	 //            } else {
	 //                error.insertAfter(element);
	 //            }
		// 	},
		// 	messages: {
		// 		plan_duration: "Please select a plan duration."
		// 	}
		// });

		$('form .plan_duration input').change(function() {

			$('form .plan_duration_wrapper label').removeClass('checked');
			$(this).closest('label').addClass('checked');

			var plan = $(this).attr('id');
			var rid = $('#rid').val();

			// if(plan == 'standard_monthly') {

			// 	$('form').attr('action', 'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=G3C232L47LBSL&custom=' + rid);
			
			// } else if(plan == 'standard_yearly') {

			// 	$('form').attr('action', 'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=6WQ46535Q9P2L&custom=' + rid);
			
			// } else if(plan == 'pro_monthly') {

			// 	$('form').attr('action', 'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ZJKXA88ADWNQ2&custom=' + rid);
			
			// } else if(plan == 'pro_yearly') {

			// 	$('form').attr('action', 'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=WMB3NPW47D2CA&custom=' + rid);
			
			// } else if(plan == 'platinum_monthly') {

			// 	$('form').attr('action', 'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=K6VK5U89MQBWE&custom=' + rid);
			
			// } else if(plan == 'platinum_yearly') {

			// 	$('form').attr('action', 'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=N38LEZP3BWY98&custom=' + rid);
			
			// }
		});

		$('form .cc_type input').change(function() {

			$('form .cc_type_wrapper label').removeClass('checked');
			$(this).closest('label').addClass('checked');

		});

		
	}


	function getWordCount(wordString) {
	  var words = wordString.split(" ");
	  words = words.filter(function(words) { 
	    return words.length > 0
	  }).length;
	  return words;
	}

	//add the custom validation method
	jQuery.validator.addMethod("wordCount",
	   function(value, element, params) {
	      var count = getWordCount(value);
	      if(count >= params[0]) {
	         return true;
	      }
	   },
	   jQuery.validator.format("A minimum of {0} words is required.")
	);


	function nl2br(str, is_xhtml) {   
	    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
	    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
	}

	function br2nl(str) {
		return str.replace(/<br\s*[\/]?>/gi, "\n");
	}
});

