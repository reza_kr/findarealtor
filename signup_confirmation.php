<?php

    require_once('functions.php');

    $html = load_html();

    if($_GET['e'] == 'invalid_confirmation') {

        $message = '<div class="alert alert-danger" role="alert">This confirmation link has either expired or is otherwise invalid. Please request a new confirmation link.</div>';
    
    } else if($_GET['ref'] == 'confirm_email_address') {

        $message = '<div class="alert alert-success" role="alert">Thank you for signing up! <strong>Please confirm your email address by using the link in the Welcome email we have sent you.</strong></div>';
    
    } else if($_GET['ref'] == 'registration') {

        $message = '<div class="alert alert-success" role="alert">Your account has been verified! You are now logged in and ready to write reviews!</div>';
    
    }  else if($_GET['ref'] == 'realtor_registration') {

        $message = '<div class="alert alert-success" role="alert">Your account has been verified! You are now logged in and ready to be explored by homeowners! Don\'t forget to <a href="realtor_account_settings.php">complete your profile</a> to increase your chances of being viewed!</div>';
    }
    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>
    </head>

    <body class="do-contact-us-page">

        <?php require_once('inc/header.php'); ?>

        <section class="content">
            <div class="container">
                
                <div class="row">

                	<div class="col-xs-12">
                		<?php echo $message; ?>

                    </div>
                </div>

                <div class="row">

                    <div class="col-xs-12 col-md-6 col-md-offset-3">

                        <?php

                        if($_GET['ref'] == 'registration') {

                        ?>

                        <h2 class="text-center">Great! Now What?</h2>

                        <hr>

                        <div class="now_what">
                            <a href="search.php">
                                <table>
                                    <tr>
                                        <td><i class="fa fa-search"></i></td>
                                        <td>
                                            <h2>Search for Realtors</h2>
                                            <h4>Search through our Realtor profiles and find someone who's right for you!</h4>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </div>

                        <div class="now_what">
                            <a href="write_a_review.php">   
                                <table>
                                    <tr>
                                        <td><i class="fa fa-pencil"></i></td>
                                        <td>
                                            <h2>Write a Review</h2>
                                            <h4>Write a review for a Realtor you have dealt with in the past.</h4>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </div>

                        <div class="now_what">
                            <a href="account_settings.php">
                                <table>
                                    <tr>
                                        <td><i class="fa fa-user"></i></td>
                                        <td>
                                            <h2>Upload Profile Picture</h2>
                                            <h4>Complete your profile by uploading a photo!</h4>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </div>

                        <?php

                        }

                        ?>


                        <?php

                        if($_GET['ref'] == 'realtor_registration') {

                        ?>

                        <h2 class="text-center">Great! Now What?</h2>

                        <hr>

                        <div class="now_what">
                            <a href="realtor_account_settings.php">
                                <table>
                                    <tr>
                                        <td><i class="fa fa-user"></i></td>
                                        <td>
                                            <h2>Complete Your Profile</h2>
                                            <h4>Upload a profile picture, add your contact details and set a service area!</h4>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </div>

                        <div class="now_what">
                            <a href="request_a_review.php">
                                <table>
                                    <tr>
                                        <td><i class="fa fa-paper-plane"></i></td>
                                        <td>
                                            <h2>Send out Review Requests</h2>
                                            <h4>Contact your previous clients and kindly ask them to write a review for you!</h4>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </div>

                        <div class="now_what">
                            <a href="upgrade.php">   
                                <table>
                                    <tr>
                                        <td><i class="fa fa-star"></i></td>
                                        <td>
                                            <h2>Upgrade Your Profile</h2>
                                            <h4>Upgrade your account and benefit from our full set of features!</h4>
                                        </td>
                                    </tr>
                                </table>
                            </a>
                        </div>


                        <?php

                        }

                        ?>

                	</div>
                </div>

            </div>
        </section>

        <?php require_once('inc/footer.php'); ?>
                                    
    </body>
</html>
