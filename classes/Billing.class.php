<?php

	session_start();
	
	Class Billing { 

		public function __construct () {

			require("inc/connect_dbo.php");

			$this->dbo = $dbo;

			require_once('PseudoCrypt.class.php');
			$this->pseudocrypt = new PseudoCrypt();

		}


		public function processPayment() { 
			//Processed when user is sent back from completed payment... STILL need to await IPN notification to confirm payment hereafter!
			$realtor_id = $this->pseudocrypt->unhash($_GET['cm']);

			$txn_id = $_GET['tx'];
			$status = strtolower($_GET['st']);
			$amount = $_GET['amt'];
			$plan = $_GET['item_number'];

			$datetime = date('Y-m-d H:i:s');

			$query = $this->dbo->prepare("INSERT INTO transactions SET realtor_id = ?, txn_id = ?, type = ?, amount = ?, plan = ?, status = ?, datetime = ?");
			$query->execute(array($realtor_id, $txn_id, 'pay', $amount, $plan, $status, $datetime));
			
		}


		public function incomingIPN() { 

			//**************************************
			//For production, the IPN NOTIFICATION URL must be set on paypal.com -> my account -> profile -> instant payment notification preferences.
			//**************************************

			$ipn_post_data = $_POST;

			$url = 'https://www.paypal.com/cgi-bin/webscr';

			//Confirm with paypal that the IPN was received....
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
		    curl_setopt($ch, CURLOPT_POST, TRUE);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('cmd' => '_notify-validate') + $ipn_post_data));
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		    curl_setopt($ch, CURLOPT_HEADER, FALSE);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
			$res = curl_exec($ch);
			
			curl_close($ch);

			//Handle the response....
			if (strcmp (trim($res), "VERIFIED") == 0) {

			    $payment_status = strtolower($_POST['payment_status']);
			    
			    if($_POST['mc_gross'] != NULL) {
			    
			    	$payment_amount = $_POST['mc_gross'];
			    
			    } else {
			   	
			   		$payment_amount = $_POST['mc_gross1'];
			    }

			    $txn_id = $_POST['txn_id'];

			    $realtor_id = $this->pseudocrypt->unhash($_POST['custom']);

			    $datetime = date('Y-m-d H:i:s');

			    //record transaction...
			    $query = $this->dbo->prepare("UPDATE transactions SET ipn_confirmation = ? WHERE realtor_id = ? AND txn_id = ?");
				$query->execute(array($realtor_id, $txn_id));
				

			} else if (strcmp ($res, "INVALID") == 0) {
			    // log for manual investigation
			}

		}

	}
?>