<?php

	session_start();

	require_once('sendgrid-api/sendgrid-php.php');


	Class Email { 

		public function __construct () {

			$this->emailHeader = '<table width="690" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="max-width:690px;padding-top:0px;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;background-color:#ffffff;border:1px solid #dddddd;">
							        <tbody><tr>
							            <td colspan="3" width="100%">
							            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f9f9f9" style="padding-top:0px;border-collapse:collapse;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;background-color:#E6E6E6;border-bottom:1px solid #dddddd;">
							              <tbody><tr>
							                <td width="3%"></td>
							                <td width="94%" height="64"><a href="http://findarealtor.net"><img src="http://findarealtor.net/img/logo.png" alt="FindARealtor" title="FindARealtor" border="0" width="210" height="" style="vertical-align:middle;display:block;"></a></td>
							                <td width="3%"></td>
							              </tr>
							            </tbody></table></td>
							        </tr>
							        <tr> 
							          <td colspan="3" width="100%" height="30"></td>
							        </tr>';

 			$this->emailFooter = '<tr>
							          <td colspan="3" width="100%" height="25"></td>
							        </tr>
							 
							      
							        <tr>
							          <td colspan="3" width="100%" height="25" style="border-collapse:collapse;border-bottom:1px solid #f1f1f1;"></td>
							        </tr>
							        <tr bgcolor="#E6E6E6" style="background-color:#E6E6E6;">

							            	<td width="3%" style="border-top:1px solid #dddddd;"></td>
							              <td width="94%" style="padding:20px 0; border-top:1px solid #dddddd;"><p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:10px;line-height:12px;color:#989898;">Copyright &copy; ' . date('Y') . ' FindARealtor.net. 
							              </td>
							              <td width="3%" style="border-top:1px solid #dddddd;"></td>
							        </tr>


							      </tbody>

							</table>';

		}


		public function signUpConfirmation($first_name, $last_name, $email, $user_type, $token) {

			$sendgrid = new SendGrid('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', array("turn_off_ssl_verification" => true));
			$mail    = new SendGrid\Email();

			$mail->addTo($email)->
			       setFrom('noreply@findarealtor.net')->
			       setFromName('FindARealtor.net')->
			       setSubject('FindARealtor.net – Sign Up Confirmation')->
			       setText("Dear " . $first_name . " " . $last_name . ",\r\n\r\nWe are delighted to have you on board!\r\nTo get your account up and running, we just need to confirm that the email address you provided really belongs to you. Please follow the provided link below and you will be forwarded to the login page immediately.\r\n\r\n Confirmation Link: \r\n http://findarealtor.net/signup_confirmation.php?type=" . $user_type . "&token=" . $token . "&email=" . $email . "\r\n\r\nSincerely,\r\nThe FindARealtor Team\r\nfindarealtor.net")->
			       setHtml($this->emailHeader . '
							        <tr>
							        	 <td width="3%"></td>

							          	  <td width="94%">
							                  	<p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;">

							                  	<strong>Dear ' . $first_name . ' ' . $last_name . ',</strong><br><br>We are delighted to have you on board!<br>To get your account up and running, we just need to confirm that the email address you provided really belongs to you. Please follow the provided link below and you will be forwarded to the login page immediately.<br><br> Confirmation Link: <br><a style="word-break:break-all;" href="http://findarealtor.net/signup_confirmation.php?type=' . $user_type . '&token=' . $token . '&email=' . $email . '" target="_blank" >http://findarealtor.net/signup_confirmation.php?type=' . $user_type . '&token=' . $token . '&email=' . $email . '</a><br><br>Sincerely,<br><strong>The FindARealtor Team</strong><br><a href="http://findarealtor.net">findarealtor.net</a>


							                	</p>
							              </td>

							        	   <td width="3%"></td>
							        </tr>'. $this->emailFooter);

			$response = $sendgrid->send($mail);
			

		}



		public function resetPassword($first_name, $last_name, $email, $user_type, $token) {

			$sendgrid = new SendGrid('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', array("turn_off_ssl_verification" => true));
			$mail    = new SendGrid\Email();

			$mail->addTo($email)->
			       setFrom('noreply@findarealtor.net')->
			       setFromName('FindARealtor.net')->
			       setSubject('FindARealtor.net – Password Reset Request')->
			       setText("Dear " . $first_name . " " . $last_name . ",\r\n\r\nWe have received your password reset request.\r\nTo reset your password, please use the temporary link provided below. You have 24 hours to reset your password with the provided link. Thereafter you are required to request a new password reset.\r\n\r\n Temporary Reset Link: \r\nhttp://findarealtor.net/" . (($user_type == "realtor") ? "realtor_password_change" : "password_change") . ".php?token=" . $token . "&email=" . $email . "\r\n\r\nIf you did not request a password reset and/or suspect that your account has been compromised with, we advice you to get in touch with us immediately at support@findarealtor.net.\r\n\r\nThank you for using FindARealtor and have a great day!\r\n\r\nSincerely,\r\nThe FindARealtor Team\r\nfindarealtor.net")->
			       setHtml($this->emailHeader . '
							        <tr>
							        	 <td width="3%"></td>

							          	  <td width="94%">
							                  	<p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;">

							                  	<strong>Dear ' . $first_name . ' ' . $last_name . ',</strong><br><br>We have received your password reset request.<br>To reset your password, please use the temporary link provided below. You have 24 hours to reset your password with the provided link. Thereafter you are required to request a new password reset.<br><br> Temporary Reset Link: <br><a style="word-break:break-all;" href="http://findarealtor.net/' . (($user_type == 'realtor') ? 'realtor_password_change' : 'password_change') . '.php?token=' . $token . '&email=' . $email . '" target="_blank" >http://findarealtor.net/' . (($user_type == 'realtor') ? 'realtor_password_change' : 'password_change') . '.php?token=' . $token . '&email=' . $email . '</a><br><br>If you did not request a password reset and/or suspect that your account has been compromised with, we advice you to get in touch with us immediately at <a href="mailto:support@findarealtor.net">support@findarealtor.net</a>.<br><br>Thank you for using FindARealtor and have a great day!<br><br>Sincerely,<br><strong>The FindARealtor Team</strong><br><a href="http://findarealtor.net">findarealtor.net</a>


							                	</p>
							              </td>

							        	   <td width="3%"></td>
							        </tr>'. $this->emailFooter);

			$response = $sendgrid->send($mail);
			

		}



		public function reviewRequest($first_name, $last_name, $client_first_name, $client_last_name, $client_email, $message, $token) {

			$sendgrid = new SendGrid('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', array("turn_off_ssl_verification" => true));
			$mail    = new SendGrid\Email();

			$mail->addTo($client_email)->
			       setFrom('noreply@findarealtor.net')->
			       setFromName('FindARealtor.net')->
			       setSubject('FindARealtor.net – ' . $first_name . ' ' . $last_name . ' would like your help!')->
			       setText("Message from " . $first_name . " " . $last_name . ":\r\n--------\r\n" . nl2br($message) . "\r\n--------\r\nWrite a review for " . $first_name . " " . $last_name . " and help them increase their reputation on FindARealtor! Follow the link below:\r\n\r\nhttp://findarealtor.net/write_a_review.php?token=" . $token . "\r\n\r\nThank you for using FindARealtor and have a great day!\r\n\r\nSincerely,\r\nThe FindARealtor Team\r\nfindarealtor.net")->
			       setHtml($this->emailHeader . '
							        <tr>
							        	 <td width="3%"></td>

							          	  <td width="94%">
							                  	<p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;">

							                  	<strong>Message from ' . $first_name . ' ' . $last_name . ':<br><hr><br>"' . nl2br($message) . '"<br><hr><br>Write a review for ' . $first_name . ' ' . $last_name . ' and help them increase their reputation on FindARealtor! Follow the link below:<br><br><a href="http://findarealtor.net/write_a_review.php?token=' . $token . '">http://findarealtor.net/write_a_review.php?token=' . $token . '</a><br><br>Thank you for using FindARealtor and have a great day!<br><br>Sincerely,<br><strong>The FindARealtor Team</strong><br><a href="http://findarealtor.net">findarealtor.net</a>

							                	</p>
							              </td>

							        	   <td width="3%"></td>
							        </tr>'. $this->emailFooter);

			$response = $sendgrid->send($mail);
			

		}


		public function expiryNotification($first_name, $last_name, $email, $expiry_date, $days_to_expiry) {

			$this->mail->addAddress($email, $first_name . ' ' . $last_name);

			$this->mail->Subject = 'FindARealtor – Your plan is about to expire';

			$this->mail->Body   .= $this->emailHeader;	

			$this->mail->Body    .=  '<tr>
							          <td width="3%"></td>
							          <td width="94%">
							            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
							                <tbody>
							                    <tr>
							                      <td width="60%" valign="top">

							                          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
							                                <tbody>

							                                  <tr>
							                                      <td width="100%" style="font-size:28px;line-height:36px;color:#434343;">FindARealtor – Expiry Notice</td>
							                                  </tr>
							    
							                                </tbody>
							                          </table>

							                      </td>
							                    </tr>
							                    <tr>
							                    	   <td colspan="2" width="100%" height="10" style="border-collapse:collapse;border-bottom:1px solid #f1f1f1;"></td>
							                    </tr>

							                </tbody>
							            </table>

							          </td>  
							          <td width="3%"></td>
							        </tr>'; 

			$this->mail->Body   .= '<tr>
							            <td colspan="3" width="100%" height="25"></td>
							        </tr>

							        <tr>
							        	 <td width="3%"></td>

							          	  <td width="94%">
							                  	<p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;">

							                  	<strong>Dear ' . $first_name . ' ' . $last_name . ',<br><br>Your plan is set to expire in ' . $days_to_expiry . ' days (' . $expiry_date . '). Please log in and renew your membership to maintain your profile features!<br><br>Thank you for using FindARealtor and have a great day!<br><br>Sincerely,<br><strong>The FindARealtor Team</strong><br><a href="http://findarealtor.net">findarealtor.net</a>

							                	</p>
							              </td>

							        	   <td width="3%"></td>
							        </tr>';

			$this->mail->Body   .= $this->emailFooter;				        


			if(!$this->mail->send()) {

			   return false;
			}

			return true;
		}


		public function expiredNotification($first_name, $last_name, $email) {

			$this->mail->addAddress($email, $first_name . ' ' . $last_name);

			$this->mail->Subject = 'FindARealtor – Your plan is about to expire';

			$this->mail->Body   .= $this->emailHeader;	

			$this->mail->Body    .=  '<tr>
							          <td width="3%"></td>
							          <td width="94%">
							            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
							                <tbody>
							                    <tr>
							                      <td width="60%" valign="top">

							                          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
							                                <tbody>

							                                  <tr>
							                                      <td width="100%" style="font-size:28px;line-height:36px;color:#434343;">FindARealtor – Expiry Notice</td>
							                                  </tr>
							    
							                                </tbody>
							                          </table>

							                      </td>
							                    </tr>
							                    <tr>
							                    	   <td colspan="2" width="100%" height="10" style="border-collapse:collapse;border-bottom:1px solid #f1f1f1;"></td>
							                    </tr>

							                </tbody>
							            </table>

							          </td>  
							          <td width="3%"></td>
							        </tr>'; 

			$this->mail->Body   .= '<tr>
							            <td colspan="3" width="100%" height="25"></td>
							        </tr>

							        <tr>
							        	 <td width="3%"></td>

							          	  <td width="94%">
							                  	<p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;">

							                  	<strong>Dear ' . $first_name . ' ' . $last_name . ',<br><br>Your plan has expired and your account has been downgraded to our \'Free\' plan. This means that you will no longer be able to enjoy the features that you did with your paid membership. To renew your membership now, please follow the link below:<br><br><a href="http://findarealtor.net/renew_plan.php">http://findarealtor.net/renew_plan.php</a><br><br>Thank you for using FindARealtor and have a great day!<br><br>Sincerely,<br><strong>The FindARealtor Team</strong><br><a href="http://findarealtor.net">findarealtor.net</a>

							                	</p>
							              </td>

							        	   <td width="3%"></td>
							        </tr>';

			$this->mail->Body   .= $this->emailFooter;				        


			if(!$this->mail->send()) {

			   return false;
			}

			return true;
		}


		public function realtorAdded($first_name, $last_name, $contact_email) {

			$this->mail->addAddress($email, $first_name . ' ' . $last_name);

			$this->mail->Subject = 'FindARealtor – Someone has created a profile for you!';

			$this->mail->Body   .= $this->emailHeader;	

			$this->mail->Body    .=  '<tr>
							          <td width="3%"></td>
							          <td width="94%">
							            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
							                <tbody>
							                    <tr>
							                      <td width="60%" valign="top">

							                          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
							                                <tbody>

							                                  <tr>
							                                      <td width="100%" style="font-size:28px;line-height:36px;color:#434343;">FindARealtor – Realtor Profile Created</td>
							                                  </tr>
							    
							                                </tbody>
							                          </table>

							                      </td>
							                    </tr>
							                    <tr>
							                    	   <td colspan="2" width="100%" height="10" style="border-collapse:collapse;border-bottom:1px solid #f1f1f1;"></td>
							                    </tr>

							                </tbody>
							            </table>

							          </td>  
							          <td width="3%"></td>
							        </tr>'; 

			$this->mail->Body   .= '<tr>
							            <td colspan="3" width="100%" height="25"></td>
							        </tr>

							        <tr>
							        	 <td width="3%"></td>

							          	  <td width="94%">
							                  	<p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;">

							                  	<strong>Dear ' . $first_name . ' ' . $last_name . ',<br><br>Someone has created a Realtor profile for you on FindARealtor.net! FindARealtor allows present and future homeowners to review Realtors and connect with them with ease! You can take control of your profile free of charge now by following the link below:<br><br><a href="http://findarealtor.net/claim_realtor_profile.php?id=' . $realtor_id . '">http://findarealtor.net/claim_realtor_profile.php?id=' . $realtor_id . '</a><br><br>If you need further assistance with getting your account set up, please don\t hesitate to contact us.<br><br>Have a great day!<br><br>Sincerely,<br><strong>The FindARealtor Team</strong><br><a href="http://findarealtor.net">findarealtor.net</a>

							                	</p>
							              </td>

							        	   <td width="3%"></td>
							        </tr>';

			$this->mail->Body   .= $this->emailFooter;				        


			if(!$this->mail->send()) {

			   return false;
			}

			return true;

		}		

	}

?>