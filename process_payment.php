<?php

	require_once(__DIR__ . "/paypal-pro/includes/config.php");

	if(!isset($mess)) { 
		$mess = ""; 
	}

	//REQUEST VARIABLES 
	$fname = (!empty($_REQUEST["fname"])) ? strip_tags(str_replace("'", "`", $_REQUEST["fname"])) : '';
	$lname = (!empty($_REQUEST["lname"])) ? strip_tags(str_replace("'", "`", $_REQUEST["lname"])) : '';
	//$email = (!empty($_REQUEST["email"])) ? strip_tags(str_replace("'", "`", $_REQUEST["email"])) : '';
	$address = (!empty($_REQUEST["address"])) ? strip_tags(str_replace("'", "`", $_REQUEST["address"])) : '';
	$city = (!empty($_REQUEST["city"])) ? strip_tags(str_replace("'", "`", $_REQUEST["city"])) : '';
	$country = (!empty($_REQUEST["country"])) ? strip_tags(str_replace("'", "`", $_REQUEST["country"])) : 'US';
	$state = (!empty($_REQUEST["state"])) ? strip_tags(str_replace("'", "`", $_REQUEST["state"])) : '';
	$zip = (!empty($_REQUEST["zip"])) ? strip_tags(str_replace("'", "`", $_REQUEST["zip"])) : '';
	// $service = (!empty($_REQUEST['service'])) ? strip_tags(str_replace("'", "`", strip_tags($_REQUEST['service']))) : '0';
	// $item_description = (!empty($_REQUEST["item_description"])) ? strip_tags(str_replace("'", "`", $_REQUEST["item_description"])) : '';
	// $amount = (!empty($_REQUEST["amount"])) ? strip_tags(str_replace("'", "`", $_REQUEST["amount"])) : '';
	$realtor_id = $_SESSION['realtor_id_hashed'];
	
	//FORM SUBMISSION PROCESSING 
	if(!empty($_POST["process"]) && $_POST["process"] == "yes") {

		require(__DIR__ . "/paypal-pro/includes/form.processing.php");
	} 
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>

        <style type="text/css">
			.wrapper {
				min-height: 100%;
				height: auto !important; /* This line and the next line are not necessary unless you need IE6 support */
				height: 100%;
				margin: 0 auto -82px; /* the bottom margin is the negative value of the footer's height */
			}
			footer, .push {
				height: 82px; /* .push must be the same height as .footer */
			}

			nav {
				height: 90px;
			}
        </style>
    </head>

    <body class="do-contact-us-page">

	    <div class="preloader">
	        <div class="do-loader">&nbsp;</div>
	    </div>

	    <div class="wrapper">

		    <header>
		    	<nav class="navbar do-main-menu" role="navigation">
		            <div class="container">

		                <!-- Navbar Toggle -->
		                <div class="navbar-header">
		                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		                        <span class="icon-bar"></span>
		                        <span class="icon-bar"></span>
		                        <span class="icon-bar"></span>
		                    </button>

		                    <!-- Logo -->
		                    <a class="navbar-brand" href="/"><img class="logo" src="img/logo.png" alt="FindARealtor"></a>

		                </div>
		                <!-- Navbar Toggle End -->
		            </div>
		        </nav>
		        <!-- Navigation Menu end-->
		    </header>

		    <section class="do-normal-page-title-section">
	            <div class="container">
	                <div class="row">
	                    <!-- Page Title -->
	                    <div class="do-page-title-wrapper">
	                        <div class="do-default-page-title col-xs-12">
	                            <h2><?php echo $html['page_title']; ?></h2>
	                        </div>
	                    </div>
	                    <!-- Page Title End -->
	                </div>
	            </div>
	        </section>

	        <section class="content">
	            <div class="container">
	                
	                <div class="row">
						<div class="col-xs-12 col-md-6 col-md-offset-3 boxed">

							<form action="?ref=<?php echo $_GET['ref']; ?>" method="POST" onsubmit="return checkForm();" class="pppt_form" id="ff1" name="ff1">

								<?php

								if($_GET['ref'] == 'standard_plan') {

								?>
									<h2 class="text-center m-t-0">Standard Plan</h2>

									<hr>

									<h4 class="text-center">Please choose a plan duration</h4>
									<br>

									<div class="form-group plan_duration_wrapper">
										<label class="plan_duration" for="standard_monthly">
											<input type="radio" id="standard_monthly" name="plan_duration" value="standard_monthly" />
											Monthly @ $39 USD/Month
										</label>

										<label class="plan_duration" for="standard_yearly">
											<input type="radio" id="standard_yearly" name="plan_duration" value="standard_yearly" />
											Yearly @ $419 USD/Year (Save 10%)
										</label>
									</div>

								<?php

								} else if($_GET['ref'] == 'premium_plan') {

								?>

									<h2 class="text-center m-t-0">Premium Plan</h2>

									<hr>

									<h4 class="text-center">Please select a plan duration</h4>
									<br>

									<div class="form-group">
										<label class="plan_duration" for="premium_monthly">
											<input type="radio" id="premium_monthly" name="plan_duration" />
											Monthly @ $79 USD/Month
										</label>

										<label class="plan_duration" for="premium_yearly">
											<input type="radio" id="premium_yearly" name="plan_duration" />
											Yearly @ $849 USD/Year (Save 10%)
										</label>
									</div>

								<?php

								} else if($_GET['ref'] == 'platinum_plan') {

								?>

									<h2 class="text-center m-t-0">Platinum Plan</h2>

									<hr>

									<h4 class="text-center">Please select a plan duration</h4>
									<br>

									<div class="form-group">
										<label class="plan_duration" for="platinum_monthly">
											<input type="radio" id="platinum_monthly" name="plan_duration" />
											Monthly @ $139 USD/Month
										</label>

										<label class="plan_duration" for="platinum_yearly">
											<input type="radio" id="platinum_yearly" name="plan_duration" />
											Yearly @ $1,499 USD/Year (Save 10%)
										</label>
									</div>

								<?php

								}

								?>

								<hr>

								<!-- <p class="small text-center"><img src="img/paypal_accept.png" alt="Pay with PayPal" width="160" /><br>(No PayPal account required)</p>
								<button type="submit" class="btn btn-primary btn-block"><i class="fa fa-paypal"></i> Pay With PayPal</button> -->

								<h2 class="text-center">Billing Information</h2>
								<br> 

								<div class="row">
									<div class="col-xs-12 col-md-6">
					                	<div class="form-group">
											 <label for="fname">First Name</label>
											 <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" value="<?php echo ($fname != '') ? $fname : $_SESSION['first_name'];?>" onkeyup="checkFieldBack(this);">
										</div>
					                </div>
					                <div class="col-xs-12 col-md-6">
					                    <div class="form-group">
											 <label for="lname">Last Name</label>
											 <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" value="<?php echo ($lname != '') ? $lname : $_SESSION['last_name'];?>" onkeyup="checkFieldBack(this);">
										</div>
									</div>
								</div>

								<div class="form-group">
									 <label for="address">Address</label>
									 <input type="text" class="form-control" id="address" name="address" placeholder="Address" value="<?php echo $address;?>" onkeyup="checkFieldBack(this);">
								</div>
			                    
			                    <div class="row">
									<div class="col-xs-12 col-md-6">
										<div class="form-group">
											 <label for="city">City</label>
											 <input type="text" class="form-control" id="city" name="city" placeholder="City" value="<?php echo $city;?>" onkeyup="checkFieldBack(this);">
										</div>
			                    	</div>
			                    	<div class="col-xs-12 col-md-6">
						                <div class="form-group">
											 <label for="country">Country</label>
						                    <select name="country" id="country" class="form-control" disabled="disabled">
						                    	<option value="<?php echo $_SESSION['country']; ?>"><?php echo ($_SESSION['country'] == 'CA') ? 'Canada' : $_SESSION['country']; ?></option>
						                    </select>
					                  	</div>
					                </div>
					            </div>

					            <div class="row">
									<div class="col-xs-12 col-md-6">
						                <div class="form-group">
											 <label for="state">State/Province</label>
						                    <select name="state" id="state" class="form-control" onchange="checkFieldBack(this);">
						                    	<option>Please Choose...</option>
						                    	<?php 

						                    	if($_SESSION['country'] == 'US') {

						                    	?>
							                    	
						                    	<option>Alabama</option>
												<option>Alaska</option>
												<option>Arizona</option>
												<option>Arkansas</option>
												<option>California</option>
												<option>Colorado</option>
												<option>Connecticut</option>
												<option>Delaware</option>
												<option>Florida</option>
												<option>Georgia</option>
												<option>Hawaii</option>
												<option>Idaho</option>
												<option>Illinois Indiana</option>
												<option>Iowa</option>
												<option>Kansas</option>
												<option>Kentucky</option>
												<option>Louisiana</option>
												<option>Maine</option>
												<option>Maryland</option>
												<option>Massachusetts</option>
												<option>Michigan</option>
												<option>Minnesota</option>
												<option>Mississippi</option>
												<option>Missouri</option>
												<option>Montana Nebraska</option>
												<option>Nevada</option>
												<option>New Hampshire</option>
												<option>New Jersey</option>
												<option>New Mexico</option>
												<option>New York</option>
												<option>North Carolina</option>
												<option>North Dakota</option>
												<option>Ohio</option>
												<option>Oklahoma</option>
												<option>Oregon</option>
												<option>Pennsylvania Rhode Island</option>
												<option>South Carolina</option>
												<option>South Dakota</option>
												<option>Tennessee</option>
												<option>Texas</option>
												<option>Utah</option>
												<option>Vermont</option>
												<option>Virginia</option>
												<option>Washington</option>
												<option>West Virginia</option>
												<option>Wisconsin</option>
												<option>Wyoming</option>

												<?php

												} else if($_SESSION['country'] == 'CA') {

												?>

												<option>Alberta</option>
												<option>British Columbia</option>
												<option>Manitoba</option>
												<option>New Brunswick</option>
												<option>Newfoundland and Labrador</option>
												<option>Northwest Territories</option>
												<option>Nova Scotia</option>
												<option>Nunavut</option>
												<option>Ontario</option>
												<option>Prince Edward Island</option>
												<option>Quebec</option>
												<option>Saskatchewan</option>
												<option>Yukon Territory</option>

												<?php

												}

												?>
						                    </select>
						                </div> 	
						            </div>
						        	<div class="col-xs-12 col-md-6">
						                <div class="form-group">
											 <label for="zip">ZIP/Postal Code</label>
					                    	<input name="zip" id="zip" type="text" class="form-control" placeholder="Zip/Postal Code" value="<?php echo $zip;?>" onkeyup="checkFieldBack(this);" />
					                   	</div>	 
					                </div>
					            </div>

			                   	<hr>

				                <h2 class="text-center">Method of Payment</h2>
				                <br>
				                <label>Choose Payment Method</label>
				                <div class="form-group cc_type_wrapper">
									<label class="cc_type" for="visa">
										<input name="cctype" id="visa" type="radio" value="V" />
										<img src="img/visa.png" class="cardhide V" />
									</label>

									<label class="cc_type" for="mastercard">
										<input name="cctype" id="mastercard" type="radio" value="M" />
										<img src="img/mc.png" class="cardhide M" />
									</label>

									<label class="cc_type" for="amex">
										<input name="cctype" id="amex" type="radio" value="A" />
										<img src="img/amex.png" class="cardhide A" />
									</label>

									<label class="cc_type" for="discover">
										<input name="cctype" id="discover" type="radio" value="D" />
										<img src="img/disc.png" class="cardhide D" />
									</label>

									<?php if($enable_paypal){ ?>
										<label class="cc_type" for="paypal">
											<input name="cctype" id="paypal" type="radio" value="PP" class="isPayPal" />
											<img src="img/paypal.png" class="paypal cardhide PP" />
										</label>
				                    <?php } ?>
								</div>
			                    
			                    <div class="ccinfo">
				                    <div class="form-group">
										 <label for="ccn">Card Number</label>
			                        	<input name="ccn" id="ccn" type="text" class="form-control" placeholder="Credit Card Number" onkeyup="checkNumHighlight(this.value);checkFieldBack(this);noAlpha(this);" onkeypress="checkNumHighlight(this.value);noAlpha(this);" onblur="checkNumHighlight(this.value);" onchange="checkNumHighlight(this.value);" maxlength="16" />
			                        	<span class="ccresult"></span>
			                        </div>

			                        <div class="form-group">
										 <label for="ccname">Cardholder Name</label>
										 <input name="ccname" id="ccname" type="text" class="form-control" placeholder="Cardholder Name" onkeyup="checkFieldBack(this);"  />
									</div>

									<div class="form-group">
										 
										<div class="row">
										 	<div class="col-xs-6 col-md-3">
										 		<label for="exp1">Expiry Date</label>
						                        <select name="exp1" id="exp1" class="form-control" onchange="checkFieldBack(this);">
						                            <option value="01">01</option>
						                            <option value="02">02</option>
						                            <option value="03">03</option>
						                            <option value="04">04</option>
						                            <option value="05">05</option>
						                            <option value="06">06</option>
						                            <option value="07">07</option>
						                            <option value="08">08</option>
						                            <option value="09">09</option>
						                            <option value="10">10</option>
						                            <option value="11">11</option>
						                            <option value="12">12</option>
						                        </select>
						                    </div>
						                    <div class="col-xs-6 col-md-3">
						                    	<label for="exp2">&nbsp;</label>
						                        <select name="exp2" id="exp2" class="form-control" onchange="checkFieldBack(this);">
						                            <?php echo getActualYears();   ?>
						                        </select>
						                    </div>

						                    <div class="col-xs-6 col-md-3">
						                    	<label for="cvv">CVV</label>
					                        	<input name="cvv" id="cvv" type="text" maxlength="5" class="form-control" placeholder="CVV" onkeyup="checkFieldBack(this);noAlpha(this);"  />
					                        	<a href="hint.php" rel="hint" class="noscriptCase"><img src="img/ico_question.jpg" align="absmiddle" border="0" /></a>
					                        </div>
						                </div>
			                       	</div> 
			                    </div>         

				                <button type="submit" class="btn btn-primary btn-block">Process Payment</button>
                				<input type="hidden" name="process" value="yes" />	
							</form>	    					

	    				</div>
	    			</div>

	    		</div>
	    	</section>

	    	<div class="push"></div>

	    </div>

    	<footer class="do-footer">
	        <div class="container">
	            <div class="row">
	                <!-- FOOTER BOTTOM -->
	                <div class="do-footer-bottom">
	                    &copy; <?php echo date('Y'); ?> FindARealtor.net. <a href="terms_of_service.php">Terms of Service</a> &middot; <a href="privacy_policy.php">Privacy Policy</a> 
	                </div>
	                <!-- FOOTER BOTTOM END -->
	            </div>
	        </div>
	    </footer>
	    <!-- FOOTER SECTION END-->

	    <?php require_once('inc/footer_scripts.php'); ?>


	    <script src="paypal-pro/js/ccvalidations.js"></script>
	    <script>
		    $(document).ready(function(){
		        $(".ccinfo").show();
		        //$("a[rel='hint']").colorbox();
		        $(":radio[name=cctype]").click(function(){
		            if($(this).hasClass("isPayPal")){
		                 $(".ccinfo").slideUp("fast");
		            } else {
		                 $(".ccinfo").slideDown("fast");
		            }
		            resetCCHightlight();
		        });

		        $("input[name=ccn]").bind('paste', function(e) {
		                var el = $(this);
		                setTimeout(function() {
		                    var text = $(el).val();
		                    resetCCHightlight();
		                    checkNumHighlight(text);
		                }, 100);
		        });
		    });
		</script>
	    <script type="text/JavaScript">
		<!--

		function checkForm() {

			var err=0;

		    for (var i=0; i < document.ff1.cctype.length; i++){
		       if (document.ff1.cctype[i].checked){
		          var cctype = document.ff1.cctype[i].value;
		        }
		    }


		<?php
		$reqFields=array(
			"fname",
			"lname",
			"address",
			"city",
			"state",
			"zip",
		);

		$reqFields_cc=array(
		    "ccn",
		    "ccname",
		    "exp1",
		    "exp2",
		    "cvv"
		);

		if($show_services){
			$reqField[] = "service";
		}

		foreach ($reqFields as $v) { ?>
		if (document.getElementById('<?php echo $v;?>').value==0) {
		    if (err==0) {
		        document.getElementById('<?php echo $v;?>').focus();
		    }
		    document.getElementById('<?php echo $v;?>').style.borderColor='#FF6060';
		    err=1;
		}
		<?php } ?>

		if(cctype!="PP"){
		    <?php foreach ($reqFields_cc as $v) { ?>
		            if (document.getElementById('<?php echo $v;?>').value==0) {
		                if (err==0) {
		                    document.getElementById('<?php echo $v;?>').focus();
		                }
		                document.getElementById('<?php echo $v;?>').style.borderColor='#FF6060';
		                err=1;
		            }
		    <?php } ?>
		    if(err==0){

		        //check credit card.
		        var ccn = document.getElementById("ccn").value;
		        if(!isValidCardNumber(ccn)){
		            alert("Invalid credit card number. Please check your input and try again");
		            return false;
		        }
		        if(isExpiryDate(document.getElementById("exp2").value,document.getElementById("exp1").value)==false){
		            alert("Credit Card expiry date is in the past! Please adjust your input.");
		            return false;
		        }

		        if(!isCardTypeCorrect(ccn,cctype)){
		            alert("Invalid credit card number/type combination. Please check your input and try again");
		            return false;
		        }
		    }
		}

		// var reg1 = /(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/; // not valid
		// var reg2 = /^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,5}|[0-9]{1,3})(\]?)$/; // valid
		// if (document.getElementById('email').value==0 || !reg2.test(document.getElementById('email').value)) {
		// if (err==0) {
		// 	document.getElementById('email').focus();
		// }
		// document.getElementById('email').style.backgroundColor='#ffa5a5';
		// err=1;
		// }

		if (err==0) {
				return true;
			} else {	alert();
				alert("Please complete all highlited fields to continue.");
				return false;
			}
		}

		function checkFieldBack(fieldObj) {
			if (fieldObj.value!=0) {
				fieldObj.style.borderColor='#CCC';
			}
		}
		function noAlpha(obj){
			reg = /[^0-9.,]/g;
			obj.value =  obj.value.replace(reg,"");
		 }


		//-->
		</script>
    </body>
</html>