<?php

    require_once('functions.php');

    $html = load_html();
    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>
    </head>

    <body class="do-contact-us-page">

        <?php require_once('inc/header.php'); ?>

        <section class="content">
            <div class="container">
                
                <form action="search.php" method="GET" id="search_form">
                <div class="row">
                	<div class="col-xs-12 col-md-6">

                		<div class="row">
                			<div class="col-xs-12">
		                		<div class="form-group">
									<input type="text" id="realtor_name" name="name" class="form-control" placeholder="Realtor Name (optional)" value="<?php echo $_GET['name']; ?>" />
									<input type="text" id="search_location" name="city" class="form-control" placeholder="City" value="<?php echo $_SESSION['search_location']; ?>" />
									<input type="hidden" id="search_location_latlng" name="latlng" />
									<span class="error" style="display: none;"></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-6">
								<p>Refine Results</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-6">
								<div class="form-group">
									<label for="sort">Sort by</label>
									<select name="sort" id="sort" class="form-control">
										<option value="highest_rated" <?php echo (htmlspecialchars($_GET['sort']) == 'highest_rated') ? 'selected="selected"' : '' ?>>Highest Rated</option>
										<option value="lowest_rated" <?php echo (htmlspecialchars($_GET['sort']) == 'lowest_rated') ? 'selected="selected"' : '' ?>>Lowest Rated</option>
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-md-6">
								<div class="form-group">
									<label>&nbsp;</label>
									<input type="checkbox" class="form-control" name="picture_only" id="picture_only" <?php echo (isset($_GET['picture_only']) == true) ? 'checked="checked"' : '' ?> />
									<label for="picture_only">Only show Realtors with Profile Pictures</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
							<button id="submit_form" class="btn btn-primary btn-block">Search</button>
							</div>
						</div>

					</div>
					<div class="col-xs-12 col-md-6">
						<div id="map" style="width: 100%; height: 256px"></div>
						<input type="hidden" id="loc" value="<?php echo htmlspecialchars($_GET['city']); ?>" />
					</div>
				</div>
				</form>

				<hr>

				<div class="row">
					<div class="col-xs-12 col-md-8">

						<?php echo $html['search_results']; ?>

						<hr>
						
                		<h2 class="note no_more_realtors">No more Realtors to show.</h2>
					</div>

					<div class="col-xs-12 col-md-4 sidebar">
						<div class="sidebar_widget cant_find_a_realtor">
							<h4>Can't find a Realtor?</h4>
							<p>If you're not able to find a Realtor that you want to review, you can add them yourself!</p>
							<a href="add_a_realtor.php" class="btn btn-primary">Add &amp; Review a New Realtor</a>
						</div>

						<br><hr><br>

						<div class="sidebar_widget im_not_listed">
							<h4>I'm not listed!</h4>
							<p>Are you a Realtor but are not listed here? You can create a profile now!</p>
							<a href="realtor_registration.php" class="btn btn-primary">Register As A Realtor</a>
						</div>
					</div>
				</div>
            </div>
        </section>

        <?php require_once('inc/footer.php'); ?>
                                    
    </body>
</html>
