<?php

    require_once('functions.php');

    $html = load_html();
    
    if($_GET['e'] == 'missing_fields') {

    	$message = '<div class="alert alert-danger" role="alert">Please complete all the required fields.</div>';

    } else if($_GET['e'] == 'invalid_email') {

    	$message = '<div class="alert alert-danger" role="alert">Please provide a valid email.</div>';

    } else if($_GET['e'] == 'password_too_short') {

    	$message = '<div class="alert alert-danger" role="alert">Password must be at least 8 characters long.</div>';
    
    } else if($_GET['e'] == 'email_in_use') {

    	$message = '<div class="alert alert-danger" role="alert">The email address you provided is already in use. If you already have a Realtor account, please <a href="realtor_login.php">log in here</a>. Otherwise, try a differnt email address.</div>';
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>
    </head>

    <body class="do-contact-us-page">

        <?php require_once('inc/header.php'); ?>

        <section class="content">
            <div class="container">
                
                <div class="row">
					<div class="col-xs-12 col-md-6 col-md-offset-3">

						<?php echo $message; ?>

						<form class="form" method="POST" action="?action=realtor_registration&ref=<?php echo $_GET['ref']; ?>" id="realtor_registration">
							<div class="form-group">
								 <label for="first_name">First Name</label>
								 <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
							</div>
							<div class="form-group">
								 <label for="last_name">Last Name</label>
								 <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
							</div>
							<div class="form-group">
								 <label for="email">Email Address</label>
								 <input type="email" class="form-control" id="email" name="email" placeholder="Email Address">
							</div>
							<div class="form-group">
								 <label for="password">Password</label>
								 <input type="password" class="form-control" id="password" name="password" placeholder="Password">
							</div>
							<hr>
<!-- 							<div class="form-group">
								 <label for="country">Country</label>
								 <select class="form-control" id="country" name="country">
								 	<option value="us" selected="selected">Unites States</option>
								 	<option value="ca">Canada</option>
								 </select>
							</div> -->
							<div class="form-group">
								 <label for="service_area">Primary Service Area</label>
								 <input type="text" class="form-control" id="service_area" name="service_area" placeholder="Service Area">
								 <span class="city_error" style="display: none;"></span>
							</div>
							<div class="form-group">
								 <button id="submit_form" class="btn btn-primary btn-block">Complete Registration</button>
							</div>
						</form>

						<br>
						Already have a Realtor account? <a href="realtor_login.php"><b>Log in</b></a>
					</div>
				</div>

            </div>
        </section>

        <?php require_once('inc/footer.php'); ?>
                                    
    </body>
</html>
