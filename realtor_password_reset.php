<?php

    require_once('functions.php');

    $html = load_html();

    if($_GET['e'] == 'invalid_email') {

    	$message = '<div class="alert alert-danger" role="alert">Please provide a valid email.</div>';
    
    } else if($_GET['e'] == 'account_not_found') {

    	$message = '<div class="alert alert-danger" role="alert">We could not find an account associated to that email address.</div>';
    
    } else if($_GET['e'] == 'invalid_reset_token') {

    	$message = '<div class="alert alert-danger" role="alert">Invalid or expired token. Please request a new password reset.</div>';
    
    } else if($_GET['ref'] == 'request_sent') {

    	$message = '<div class="alert alert-success" role="alert">We have sent you an email! Please follow the instructions in the email to reset your password.</div>';
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>
    </head>

    <body class="do-contact-us-page">

        <?php require_once('inc/header.php'); ?>

        <section class="content">
            <div class="container">
                
                <div class="row">
					<div class="col-xs-12 col-md-4 col-md-offset-4">

						<?php echo $message; ?>

						<p class="note">Please provide your email address and we will send you an email with instructions to set a new password.</p>

						<form class="form" method="POST" action="?action=realtor_password_reset">
							<div class="form-group">
								 <label for="email">Email Address</label>
								 <input type="email" class="form-control" id="email" name="email" placeholder="Email Address">
							</div>

							<div class="form-group">
								 <button type="submit" class="btn btn-primary btn-block">Reset Password</button>
							</div>
						</form>

						<br>
						Remember your credentials? <a href="realtor_login.php"><b>Log in</b></a>
					</div>
				</div>

            </div>
        </section>

        <?php require_once('inc/footer.php'); ?>
                                    
    </body>
</html>
