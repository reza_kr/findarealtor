<?php

    require_once('functions.php');

    $html = load_html();

    if($_GET['e'] == 'missing_fields') {

        $message .= '<div class="alert alert-danger" role="alert">Please complete all the required fields.</div>';
    
    } else if($_GET['e'] == 'invalid_email') {

        $message .= '<div class="alert alert-danger" role="alert">Please provide a valid email address.</div>';
    
    } else if($_GET['ref'] == 'password_too_short') {

        $message .= '<div class="alert alert-danger" role="alert">Password must be at least 8 characters long.</div>';
    
    } else if($_GET['ref'] == 'password_mismatch') {

        $message .= '<div class="alert alert-danger" role="alert">Passwords don\'t match.</div>';
    
    }  

    if($_GET['ref_pass'] == 'password_change_successful') {

        $message .= '<div class="alert alert-success" role="alert">Your password has been updated!</div>';
    }  

    if($_GET['ref_profile'] == 'profile_updated') {

        $message .= '<div class="alert alert-success" role="alert">Your profile has been updated!</div>';
    }
    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>
    </head>

    <body class="do-contact-us-page">

        <?php require_once('inc/header.php'); ?>

        <section class="content">
            <div class="container">

                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">

                        <?php echo $message; ?>
                
                        <form action="?action=update_profile" method="POST">

                            <div class="profile_picture_actions_wrapper">
                                <div id="profile_picture_preview_wrapper"><?php echo $html['user_account_overview']['profile_picture']; ?></div>
                                
                                <div id="profile_picture_progress" class="progress" style="display: none;">
                                    <div class="bar"></div>
                                </div>
                                <label class="profile_picture_upload btn btn-primary">
                                    <input id="profile_picture" type="file" name="profile_picture" />
                                    Choose Photo
                                </label>
                            </div>

                            <div class="form-group">
                                 <label for="first_name">First Name</label>
                                 <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="<?php echo $html['user_account_overview']['first_name']; ?>">
                            </div>
                            <div class="form-group">
                                 <label for="last_name">Last Name</label>
                                 <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="<?php echo $html['user_account_overview']['last_name']; ?>">
                            </div>
                            <div class="form-group">
                                 <label for="email">Email Address</label>
                                 <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="<?php echo $html['user_account_overview']['email']; ?>">
                            </div>

                            <hr>

                            <div class="form-group change_password">
                                <label for="new_password">Change Password</label>
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <input type="password" class="form-control" id="new_password" name="new_password" placeholder="New Password" />
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Password" />
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <input type="hidden" id="uid" value="<?php echo $_SESSION['user_id_hashed']; ?>">
                            <button type="submit" name="submit" class="btn btn-primary pull-right">Save Changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <?php require_once('inc/footer.php'); ?>
                                    
    </body>
</html>
