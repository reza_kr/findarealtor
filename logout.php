<?php

	session_start();

	if(isset($_SESSION['logged_in'])) {

		if($_SESSION['user_type'] == 'user') {

			$redirect_url = 'login.php';
		
		} else if($_SESSION['user_type'] == 'realtor') {

			$redirect_url = 'realtor_login.php';
		
		} else {

			$redirect_url = 'login.php';
		}
	
	} else {

		header('Location: /');
		exit;
	}

	unset($_SESSION['fb_token']);

	session_destroy();

	if(!isset($_SESSION['last_page'])) {

		header('Location: ' . $redirect_url);
	
	} else {

		header('Location: ' . $_SESSION['last_page']);
	}

?>