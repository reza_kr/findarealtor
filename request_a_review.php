<?php

    require_once('functions.php');

    $html = load_html();

    if($_GET['e'] == 'missing_fields') {

    	$message = '<div class="alert alert-danger" role="alert">Passwords don\'t match.</div>';
    
    } else if($_GET['e'] == 'invalid_email') {

    	$message .= '<div class="alert alert-danger" role="alert">Password must be at least 8 characters long.</div>';
    
    } else if($_GET['ref'] == 'request_sent') {

    	$message .= '<div class="alert alert-success" role="alert">Review request has been sent!</div>';
    
    } else if($_GET['e'] == 'csv_error_missing_fields') {

    	$message .= '<div class="alert alert-danger" role="alert">Your CSV file has missing fields! Please ensure all the values are filled in correctly and try again.</div>';
    
    } else if($_GET['e'] == 'csv_error_invalid_email') {

    	$message .= '<div class="alert alert-danger" role="alert">We found an invalid email address in your CSV file! Please ensure all the values valid and try again.</div>';
    
    } else if($_GET['ref'] == 'requests_sent') {

    	$message .= '<div class="alert alert-success" role="alert">Review requests have been sent!</div>';
    
    }
    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once('inc/head.php'); ?>
    </head>

    <body class="do-contact-us-page">

        <?php require_once('inc/header.php'); ?>

        <section class="content">
            <div class="container">

            	<div class="row">
	                <div class="col-xs-12">
	                	<?php echo $message; ?>
	                </div>
	            </div>

	            <form action="?action=request_review" method="POST">

	            <div class="row">

	                <div class="col-xs-12 col-md-4">

	                	<div class="single_client">
							<div class="form-group">
								 <label for="client_first_name">Client First Name</label>
								 <input type="text" class="form-control" id="client_first_name" name="client_first_name">
							</div>
							<div class="form-group">
								 <label for="client_last_name">Client Last Name</label>
								 <input type="text" class="form-control" id="client_last_name" name="client_last_name">
							</div>
							<div class="form-group">
								 <label for="client_emails">Client Email</label>
								 <input type="text" class="form-control" id="client_email" name="client_email">
							</div>

							<span class="or">OR</span>
						</div>

						<div class="form-group bulk_clients">
							 <label for="contact_list_csv">Contact List CSV</label>
							 <textarea class="form-control" id="contact_list_csv" name="contact_list_csv" style="height: 243px; overflow: auto;"></textarea>
						</div>

						<label class="btn btn-primary btn-block upload_button">
                            <input id="bulk_upload_contacts" type="file" name="contacts_list" />
                            Bulk Upload Contacts List (.CSV)
                        </label>
						<p class="note small">Must be a <a href="http://www.computerhope.com/jargon/c/csv.htm" target="_blank">CSV file</a>; One entry per line<br>Format: <strong>first name</strong>, <strong>last name</strong>, <strong>email address</strong></p>
					</div>

					<div class="col-xs-12 col-md-8">
						<div class="form-group">
							<label for="message">Message</label>
							<textarea id="message" name="message" class="form-control" style="height: 306px;">Hi {client_first_name} {client_last_name},

Thank you very much once again for your business! I very much appreciate doing business with great clients like you!

Could I ask for 5 minutes of your time to write a review of the work I did for you on FindARealtor.net? 

On FindARealtor, consumers can read and write reviews on their experiences with Realtors - helping fellow homeowners make important hiring decisions!

Please follow the link below to post a review of your experience:
http://FindARealtor.net/write_a_review.php?id=<?php echo $_SESSION['realtor_id_hashed']; ?>


Thank you for very much for your time!

Best Regards,
<?php echo $_SESSION['first_name'] . ' ' . $_SESSION['last_name']; ?>
							</textarea>
						</div>

						<span class="note display_inline">{client_first_name}, {client_last_name} will be automatically populated.</span>

						<button type="submit" name="submit" class="btn btn-primary pull-right">Send Invitation(s)</button>
					</div>
				</div>

				</form>

				<br><br>

				<div class="row">

					<div class="col-xs-12">

						<h2>Request History</h2>
						<br>
						
						<table class="table table-striped table-bordered request_history">
							<thead>
								<tr>
									<th>Client Name</th>
									<th>Client Email</th>
									<th>Date Sent</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php echo $html['request_history']; ?>
							</tbody>
						</table>

					</div>

				</div>
            </div>
        </section>

        <?php require_once('inc/footer.php'); ?>
                                    
    </body>
</html>
